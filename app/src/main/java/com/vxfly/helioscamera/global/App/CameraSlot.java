//JIRA ICOM-1878 Start:Add by b.jiang 2015-08-31
package com.vxfly.helioscamera.global.App;

public class CameraSlot {
	// public boolean isRegister;
	public boolean isOccupied;
	public boolean enableConnect;
	public String slotName;
	public String cameraName;
	public byte[] cameraPhoto;

	public CameraSlot(String slotName, boolean isOccupied, String cameraName, byte[] cameraPhoto, boolean enableConnect) {
		this.slotName = slotName;
		this.isOccupied = isOccupied;
		this.cameraName = cameraName;
		this.cameraPhoto = cameraPhoto;
		this.enableConnect = enableConnect;
	}

	// JIRA ICOM-1878 End:Add by b.jiang 2015-08-31

}
