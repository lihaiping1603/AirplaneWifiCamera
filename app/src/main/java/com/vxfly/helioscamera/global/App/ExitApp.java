package com.vxfly.helioscamera.global.App;

import java.util.LinkedList;
import java.util.List;

import android.app.Activity;
import android.app.Application;

import com.vxfly.helioscamera.SDKAPI.FileOperation;
import com.vxfly.helioscamera.SDKAPI.PreviewStream;
import com.vxfly.helioscamera.SDKAPI.VideoPlayback;
import com.vxfly.helioscamera.camera.MyCamera;
import com.vxfly.helioscamera.log.WriteLogToDevice;

public class ExitApp extends Application {

	private List<Activity> activityList = new LinkedList<Activity>();
	private static ExitApp instance;

	public static ExitApp getInstance() {
		if (instance == null) {
			instance = new ExitApp();
		}
		return instance;
	}

	public void addActivity(Activity activity) {
		activityList.add(activity);
	}

	public void removeActivity(Activity activity) {
		activityList.remove(activity);
	}

	public void exit() {
		boolean temp = false;
		PreviewStream previewStream = PreviewStream.getInstance();
		FileOperation fileOperation = FileOperation.getInstance();
		VideoPlayback videoPlayback = VideoPlayback.getInstance();

		for (MyCamera camera : GlobalInfo.getInstance().getCameraList()) {
			if (camera.getSDKsession().isSessionOK() == true) {

				WriteLogToDevice.writeLog("[Normal] -- ExitApp: ", "start start cancelDownload ");
				temp = fileOperation.cancelDownload();
				WriteLogToDevice.writeLog("[Normal] -- ExitApp: ", "end cancelDownload temp =" + temp);

				WriteLogToDevice.writeLog("[Normal] -- ExitApp: ", "start start stopMediaStream ");
				temp = false;
				temp = previewStream.stopMediaStream(camera.getpreviewStreamClient());
				WriteLogToDevice.writeLog("[Normal] -- ExitApp: ", "start stopMediaStream temp =" + temp);

				WriteLogToDevice.writeLog("[Normal] -- ExitApp: ", "start start stopPlaybackStream ");
				temp = false;
				temp = videoPlayback.stopPlaybackStream();
				WriteLogToDevice.writeLog("[Normal] -- ExitApp: ", "start stopPlaybackStream temp =" + temp);

				WriteLogToDevice.writeLog("[Normal] -- ExitApp: ", "start start destroyCamera ");
				temp = false;
				temp = camera.destroyCamera();
				WriteLogToDevice.writeLog("[Normal] -- ExitApp: ", "destroySession temp =" + temp);
			}
		}
		for (Activity activity : activityList) {
			WriteLogToDevice.writeLog("[Normal] -- ExitApp: ", "start finsh activity");
			activity.finish();
		}
		WriteLogToDevice.writeLog("[Normal] -- ExitApp: ", "start System.exit");
		System.exit(0);
	}
	// ICOM-1919 ICOM-1920 Start add by zhangyanhu C01012 2015-8-28
	public void exitWhenScreenOff() {
		boolean temp = false;
		PreviewStream previewStream = PreviewStream.getInstance();
		FileOperation fileOperation = FileOperation.getInstance();
		VideoPlayback videoPlayback = VideoPlayback.getInstance();

		for (MyCamera camera : GlobalInfo.getInstance().getCameraList()) {
			if (camera.getSDKsession().isSessionOK() == true) {

				WriteLogToDevice.writeLog("[Normal] -- ExitApp: ", "start start cancelDownload ");
				temp = fileOperation.cancelDownload();
				WriteLogToDevice.writeLog("[Normal] -- ExitApp: ", "end cancelDownload temp =" + temp);

				WriteLogToDevice.writeLog("[Normal] -- ExitApp: ", "start start stopMediaStream ");
				temp = false;
				temp = previewStream.stopMediaStream(camera.getpreviewStreamClient());
				WriteLogToDevice.writeLog("[Normal] -- ExitApp: ", "start stopMediaStream temp =" + temp);

				WriteLogToDevice.writeLog("[Normal] -- ExitApp: ", "start start stopPlaybackStream ");
				temp = false;
				temp = videoPlayback.stopPlaybackStream();
				WriteLogToDevice.writeLog("[Normal] -- ExitApp: ", "start stopPlaybackStream temp =" + temp);

				WriteLogToDevice.writeLog("[Normal] -- ExitApp: ", "start start destroyCamera ");
				temp = false;
				temp = camera.destroyCamera();
				WriteLogToDevice.writeLog("[Normal] -- ExitApp: ", "destroySession temp =" + temp);
			}
		}
		for (Activity activity : activityList) {
			WriteLogToDevice.writeLog("[Normal] -- ExitApp: ", "start finsh activity");
			activity.finish();
		}
		WriteLogToDevice.writeLog("[Normal] -- ExitApp: ", "start System.exit");
	}

	// ICOM-1919 ICOM-1920 End add by zhangyanhu C01012 2015-8-28
	public void finishAllActivity() {
		PreviewStream previewStream = PreviewStream.getInstance();
		boolean temp = false;

		for (MyCamera camera : GlobalInfo.getInstance().getCameraList()) {
			WriteLogToDevice.writeLog("[Normal] -- ExitApp: ", "start stopMediaStream ");
			temp = previewStream.stopMediaStream(camera.getpreviewStreamClient());
			WriteLogToDevice.writeLog("[Normal] -- ExitApp: ", "end stopMediaStream temp =" + temp);

			WriteLogToDevice.writeLog("[Normal] -- ExitApp: ", "start destroySession temp =" + temp);
			temp = false;
			temp = camera.destroyCamera();
			WriteLogToDevice.writeLog("[Normal] -- ExitApp: ", "end destroySession temp =" + temp);
		}

		for (Activity activity : activityList) {
			activity.finish();
		}
	}
}
