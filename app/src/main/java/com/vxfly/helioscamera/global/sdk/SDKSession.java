package com.vxfly.helioscamera.global.sdk;

import android.util.Log;

import com.vxfly.helioscamera.log.WriteLogToDevice;
import com.icatch.wificam.customer.ICatchWificamConfig;
import com.icatch.wificam.customer.ICatchWificamSession;
import com.icatch.wificam.customer.exception.IchInvalidPasswdException;
import com.icatch.wificam.customer.exception.IchInvalidSessionException;
import com.icatch.wificam.customer.exception.IchPtpInitFailedException;

public class SDKSession {
	private static int scanflag;
	private ICatchWificamSession session;
	private String ipAddress;
	private String uid;
	private String username;
	private String password;
	private boolean sessionPrepared = false;

	public SDKSession(String ipAddress, String uid, String username, String password) {
		this.ipAddress = ipAddress;
		this.username = username;
		this.password = password;
		this.uid = uid;
	}

	public SDKSession() {
	}

	public boolean prepareSession() {
		WriteLogToDevice.writeLog("[Normal] -- SDKSession: ", "start prepareSession() session = "+session);
		// TODO Auto-generated constructor stub
		ICatchWificamConfig.getInstance().enablePTPIP();
	
		sessionPrepared = true;
		session = new ICatchWificamSession();
		WriteLogToDevice.writeLog("[Normal] -- SDKSession: ", "session = "+session);
		boolean retValue = false;
		try {
			WriteLogToDevice.writeLog("[Normal] -- SDKSession: ", "begin prepareSession0");
			retValue = session.prepareSession("192.168.1.1", "anonymous", "anonymous@icatchtek.com");
			WriteLogToDevice.writeLog("[Normal] -- SDKSession: ", "begin prepareSession0 ret="+retValue);
		} catch (IchInvalidPasswdException e) {
			// TODO Auto-generated catch block
			WriteLogToDevice.writeLog("[Error] -- SDKSession: ", "prepareSession IchInvalidPasswdException");
			e.printStackTrace();
		} catch (IchPtpInitFailedException e) {
			WriteLogToDevice.writeLog("[Error] -- SDKSession: ", "prepareSession IchPtpInitFailedException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (retValue == false) {
			WriteLogToDevice.writeLog("[Error] -- SDKSession: ", "failed to prepareSession()");
			sessionPrepared = false;
			Log.v("1111", "SDKSession,prepareSession fail!");
		}
		return sessionPrepared;
	}

	public boolean prepareSession(String ip){
		// TODO Auto-generated constructor stub
		ICatchWificamConfig.getInstance().enablePTPIP();
		sessionPrepared = true;
		session = new ICatchWificamSession();
		WriteLogToDevice.writeLog("[Normal] -- SDKSession: ", "start prepareSession(String ip) :session = "+session);
		boolean retValue = false;
		try {
			WriteLogToDevice.writeLog("[Normal] -- SDKSession: ", "begin prepareSession1");
			
			retValue = session.prepareSession(ip, "anonymous", "anonymous@icatchtek.com");
			WriteLogToDevice.writeLog("[Normal] -- SDKSession: ", "end prepareSession1 ret=" + retValue);
		} catch (IchInvalidPasswdException e) {
			// TODO Auto-generated catch block
			WriteLogToDevice.writeLog("[Error] -- SDKSession: ", "prepareSession IchInvalidPasswdException");
			e.printStackTrace();
		} catch (IchPtpInitFailedException e) {
			// TODO Auto-generated catch block
			WriteLogToDevice.writeLog("[Error] -- SDKSession: ", "prepareSession IchPtpInitFailedException");
			e.printStackTrace();
		}
		if (retValue == false) {
			WriteLogToDevice.writeLog("[Error] -- SDKSession: ", "failed to prepareSession(String ip)");
			sessionPrepared = false;
			Log.v("1111", "SDKSession,prepareSession fail!");
		}
		return sessionPrepared;
	}
	
    public boolean prepareSession(String ip,boolean enablePTPIP) {
        // TODO Auto-generated constructor stub
        if(enablePTPIP){
            ICatchWificamConfig.getInstance().enablePTPIP();
        }else{
            ICatchWificamConfig.getInstance().disablePTPIP();
        }
        sessionPrepared = true;
        session = new ICatchWificamSession();
        boolean retValue = false;
        try {
            retValue = session.prepareSession(ip, "anonymous", "anonymous@icatchtek.com");
        } catch (IchInvalidPasswdException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IchPtpInitFailedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (retValue == false) {
            sessionPrepared = false;
            Log.v("1111", "SDKSession,prepareSession fail!");
        }
        WriteLogToDevice.writeLog("[Normal] -- SDKSession:", "prepareSession =" + sessionPrepared);
        return sessionPrepared;
    }

	public boolean isSessionOK() {
		return sessionPrepared;
	}

	public ICatchWificamSession getSDKSession() {
		return session;
	}

	public boolean checkWifiConnection() {
		WriteLogToDevice.writeLog("[Normal] -- SDKSession:", "Start checkWifiConnection");
		boolean retValue = false;
		try {
			retValue = session.checkConnection();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Normal] -- SDKSession:", "checkWifiConnection IchInvalidSessionException");
			

			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- SDKSession:", "End checkWifiConnection,retValue=" + retValue);
		return retValue;
	}

	public boolean destroySession() {
		WriteLogToDevice.writeLog("[Normal] -- SDKSession:", "Start destroySession");
		Boolean retValue = false;
		try {
			retValue = session.destroySession();
			WriteLogToDevice.writeLog("[Normal] -- SDKSession:", "End  destroySession,retValue=" + retValue);
		} catch (IchInvalidSessionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return retValue;
	}

	public static boolean startDeviceScan() {
		WriteLogToDevice.writeLog("[Normal] -- SDKSession: ", "Start startDeviceScan");

		boolean tempStartDeviceScanValue = ICatchWificamSession.startDeviceScan();

		WriteLogToDevice.writeLog("[Normal] -- SDKSession: ", "End startDeviceScan,tempStartDeviceScanValue=" + tempStartDeviceScanValue);
		if (tempStartDeviceScanValue) {
			scanflag = 1;
		}
		return tempStartDeviceScanValue;
	}

	public static void stopDeviceScan() {
		WriteLogToDevice.writeLog("[Normal] -- SDKSession: ", "Start stopDeviceScan");
		boolean tempStopDeviceScanValue = false;
		if (scanflag == 1) {
			tempStopDeviceScanValue = ICatchWificamSession.stopDeviceScan();
		} else {
			tempStopDeviceScanValue = true;
		}
		scanflag = 0;
		WriteLogToDevice.writeLog("[Normal] -- SDKSession: ", "End stopDeviceScan,tempStopDeviceScanValue=" + tempStopDeviceScanValue);
	}
}
