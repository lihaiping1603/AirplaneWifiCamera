/**
 * Added by zhangyanhu C01012,2014-6-27
 */
package com.vxfly.helioscamera.SDKAPI;

import com.vxfly.helioscamera.baseItems.Tristate;
import com.vxfly.helioscamera.global.App.GlobalInfo;
import com.vxfly.helioscamera.log.WriteLogToDevice;
import com.icatch.wificam.customer.ICatchWificamPreview;
import com.icatch.wificam.customer.exception.IchAudioStreamClosedException;
import com.icatch.wificam.customer.exception.IchBufferTooSmallException;
import com.icatch.wificam.customer.exception.IchCameraModeException;
import com.icatch.wificam.customer.exception.IchInvalidArgumentException;
import com.icatch.wificam.customer.exception.IchInvalidSessionException;
import com.icatch.wificam.customer.exception.IchSocketException;
import com.icatch.wificam.customer.exception.IchStreamNotRunningException;
import com.icatch.wificam.customer.exception.IchStreamNotSupportException;
import com.icatch.wificam.customer.exception.IchTryAgainException;
import com.icatch.wificam.customer.exception.IchVideoStreamClosedException;
import com.icatch.wificam.customer.type.ICatchAudioFormat;
import com.icatch.wificam.customer.type.ICatchCustomerStreamParam;
import com.icatch.wificam.customer.type.ICatchFileStreamParam;
import com.icatch.wificam.customer.type.ICatchFrameBuffer;
import com.icatch.wificam.customer.type.ICatchH264StreamParam;
import com.icatch.wificam.customer.type.ICatchMJPGStreamParam;
import com.icatch.wificam.customer.type.ICatchPreviewMode;

public class PreviewStream {
	private static PreviewStream instance;

	// private ICatchWificamPreview previewStreamControl;
	//
	public static PreviewStream getInstance() {
		if (instance == null) {
			instance = new PreviewStream();
		}
		return instance;
	}

	//
	// private PreviewStream(){
	//
	// }
	//
	// public void initPreviewStream(){
	// previewStreamControl =
	// GlobalInfo.getInstance().getCurrentCamera().getpreviewStreamClient();
	// }

	public boolean stopMediaStream(ICatchWificamPreview previewStreamControl) {
		WriteLogToDevice.writeLog("[Normal] -- PreviewStream: ", "begin stopMediaStream");
		boolean retValue = false;
		try {
			retValue = previewStreamControl.stop();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- PreviewStream", "end stopMediaStream =" + retValue);
		return retValue;
	}

	public boolean supportAudio(ICatchWificamPreview previewStreamControl) {
		WriteLogToDevice.writeLog("[Normal] -- PreviewStream: ", "begin supportAudio");
		boolean retValue = false;
		try {
			retValue = previewStreamControl.containsAudioStream();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchStreamNotRunningException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchStreamNotRunningException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// JIRA ICOM-1844 Start delete by b.jiang 2015-08-14
		// WriteLogToDevice.writeLog("[Normal] -- PreviewStream: ",
		// "end containsAudioStream retValue =" + retValue);
		// //retValue = false;
		// retValue = retValue & (!GlobalInfo.forbidAudioOutput);
		// JIRA ICOM-1844 End delete by b.jiang 2015-08-14
		WriteLogToDevice.writeLog("[Normal] -- PreviewStream: ", "end supportAudio retValue =" + retValue);
		return retValue;
	}

	/**
	 * Added by zhangyanhu C01012,2014-7-2
	 */
	public boolean getNextVideoFrame(ICatchFrameBuffer buffer, ICatchWificamPreview previewStreamControl) {
		// WriteLogToDevice.writeLog("[Normal] -- PreviewStream: ",
		// "begin getNextVideoFrame");
		boolean retValue = false;
		// Log.d("tigertiger","previewStream = "+previewStream);
		try {
			retValue = previewStreamControl.getNextVideoFrame(buffer);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchSocketException");
			// need to close preview get next video frame
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchBufferTooSmallException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchBufferTooSmallException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchTryAgainException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchTryAgainException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchStreamNotRunningException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchStreamNotRunningException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidArgumentException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchInvalidArgumentException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchVideoStreamClosedException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchVideoStreamClosedException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// WriteLogToDevice.writeLog("[Normal] -- PreviewStream: ",
		// "end getNextVideoFrame retValue =" + retValue);
		return retValue;
	}

	/**
	 * Added by zhangyanhu C01012,2014-7-2
	 */
	public boolean getNextAudioFrame(ICatchWificamPreview previewStreamControl, ICatchFrameBuffer icatchBuffer) {
		// WriteLogToDevice.writeLog("[Normal] -- PreviewStream: ",
		// "begin getNextAudioFrame");
		boolean retValue = false;
		try {
			retValue = previewStreamControl.getNextAudioFrame(icatchBuffer);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchBufferTooSmallException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchBufferTooSmallException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchTryAgainException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchTryAgainException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchStreamNotRunningException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchStreamNotRunningException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidArgumentException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchInvalidArgumentException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchAudioStreamClosedException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchAudioStreamClosedException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// WriteLogToDevice.writeLog("[Normal] -- PreviewStream: ",
		// "end getNextAudioFrame retValue =" + retValue);
		return retValue;
	}

	/**
	 * Added by zhangyanhu C01012,2014-7-2
	 */
	public Tristate startMediaStream(ICatchWificamPreview previewStreamControl, ICatchMJPGStreamParam param, ICatchPreviewMode previewMode) {
		// TODO Auto-generated method stub
		WriteLogToDevice.writeLog("[Normal] -- PreviewStream: ", "begin startMediaStream");
		boolean temp = false;
		Tristate retValue = Tristate.FALSE;
		try {
			temp = previewStreamControl.start(param, previewMode, GlobalInfo.forbidAudioOutput);
			//JIRA ICOM-1705 Start add by b.jiang 2015-08-13
			if(temp){
				retValue = Tristate.NORMAL;
			}else{
				retValue = Tristate.FALSE;
			}
			//JIRA ICOM-1705 End add by b.jiang 2015-08-13
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidArgumentException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchInvalidArgumentException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchStreamNotSupportException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchStreamNotSupportException");
			//JIRA ICOM-1705 Start add by b.jiang 2015-08-13
			retValue = Tristate.ABNORMAL;
			//JIRA ICOM-1705 End add by b.jiang 2015-08-13
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// JIRA ICOM-1705 End add by b.jiang 2015-08-06
		WriteLogToDevice.writeLog("[Normal] -- PreviewStream: ", "param = " + param);
		WriteLogToDevice.writeLog("[Normal] -- PreviewStream: ", "previewMode = " + previewMode);
		WriteLogToDevice.writeLog("[Normal] -- PreviewStream: ", "end startMediaStream retValue =" + retValue);
		return retValue;
	}

	public Tristate startMediaStream(ICatchWificamPreview previewStreamControl, ICatchCustomerStreamParam param, ICatchPreviewMode previewMode) {
		// TODO Auto-generated method stub
		WriteLogToDevice.writeLog("[Normal] -- PreviewStream: ", "begin startMediaStream");
		boolean temp = false;
		// JIRA ICOM-1705 Start add by b.jiang 2015-08-06
		Tristate retValue = Tristate.FALSE;
		try {
			temp = previewStreamControl.start(param, previewMode, GlobalInfo.forbidAudioOutput);
			//JIRA ICOM-1705 Start add by b.jiang 2015-08-13
			if(temp){
				retValue = Tristate.NORMAL;
			}else{
				retValue = Tristate.FALSE;
			}
			//JIRA ICOM-1705 End add by b.jiang 2015-08-13
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidArgumentException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchInvalidArgumentException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchStreamNotSupportException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchStreamNotSupportException");
			// TODO Auto-generated catch block
			//JIRA ICOM-1705 Start add by b.jiang 2015-08-13
			retValue = Tristate.ABNORMAL;
			//JIRA ICOM-1705 End add by b.jiang 2015-08-13
			e.printStackTrace();
		}
		// JIRA ICOM-1705 End add by b.jiang 2015-08-06
		WriteLogToDevice.writeLog("[Normal] -- PreviewStream: ", "end startMediaStream retValue =" + retValue);
		return retValue;
	}

	/**
	 * Added by zhangyanhu C01012,2014-7-2
	 */
	public Tristate startMediaStream(ICatchWificamPreview previewStreamControl, ICatchH264StreamParam param, ICatchPreviewMode previewMode) {
		// TODO Auto-generated method stub
		WriteLogToDevice.writeLog("[Normal] -- PreviewStream: ", "begin startMediaStream");
		boolean temp = false;
		// JIRA ICOM-1705 Start add by b.jiang 2015-08-06
		Tristate retValue = Tristate.FALSE;
		try {
			temp = previewStreamControl.start(param, previewMode, GlobalInfo.forbidAudioOutput);
			//JIRA ICOM-1705 Start add by b.jiang 2015-08-13
			if(temp){
				retValue = Tristate.NORMAL;
			}else{
				retValue = Tristate.FALSE;
			}
			//JIRA ICOM-1705 End add by b.jiang 2015-08-13
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidArgumentException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchInvalidArgumentException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchStreamNotSupportException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchStreamNotSupportException");
			// TODO Auto-generated catch block
			//JIRA ICOM-1705 Start add by b.jiang 2015-08-13
			retValue = Tristate.ABNORMAL;
			//JIRA ICOM-1705 End add by b.jiang 2015-08-13
			e.printStackTrace();
		}
		// JIRA ICOM-1705 End add by b.jiang 2015-08-06
		WriteLogToDevice.writeLog("[Normal] -- PreviewStream: ", "end startMediaStream retValue =" + retValue);
		return retValue;
	}

	public boolean changePreviewMode(ICatchWificamPreview previewStreamControl, ICatchPreviewMode previewMode) {
		WriteLogToDevice.writeLog("[Normal] -- PreviewStream: ", "begin changePreviewMode");
		boolean retValue = false;
		try {
			retValue = previewStreamControl.changePreviewMode(previewMode);
		} catch (IchSocketException e) {
			// TODO Auto-generated catch block
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchSocketException");
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			// TODO Auto-generated catch block
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchCameraModeException");
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchStreamNotSupportException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchStreamNotSupportException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- PreviewStream: ", "end changePreviewMode");
		return retValue;

	}

	public int getVideoWidth(ICatchWificamPreview previewStreamControl) {
		// TODO Auto-generated method stub
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getVideoWidth");
		int retValue = 0;

		try {
			retValue = previewStreamControl.getVideoFormat().getVideoW();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchStreamNotRunningException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchStreamNotRunningException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getVideoWidth retValue =" + retValue);
		return retValue;
	}

	public int getVideoHeigth(ICatchWificamPreview previewStreamControl) {
		// TODO Auto-generated method stub
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getVideoHeigth");
		int retValue = 0;

		try {
			retValue = previewStreamControl.getVideoFormat().getVideoH();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchStreamNotRunningException e) {
			// TODO Auto-generated catch block
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchStreamNotRunningException");
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getVideoHeigth retValue =" + retValue);
		return retValue;
	}

	public int getCodec(ICatchWificamPreview previewStreamControl) {
		// TODO Auto-generated method stub
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getCodec previewStreamControl =" + previewStreamControl);
		int retValue = 0;

		try {
			retValue = previewStreamControl.getVideoFormat().getCodec();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchStreamNotRunningException e) {
			// TODO Auto-generated catch block
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchStreamNotRunningException");
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getCodec retValue =" + retValue);
		return retValue;
	}

	public int getBitrate(ICatchWificamPreview previewStreamControl) {
		// TODO Auto-generated method stub
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getBitrate");
		int retValue = 0;

		try {
			retValue = previewStreamControl.getVideoFormat().getBitrate();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchStreamNotRunningException e) {
			// TODO Auto-generated catch block
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchStreamNotRunningException");
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getBitrate retValue =" + retValue);
		return retValue;
	}

	public ICatchAudioFormat getAudioFormat(ICatchWificamPreview previewStreamControl) {
		// TODO Auto-generated method stub
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getAudioFormat");
		ICatchAudioFormat retValue = null;

		try {
			retValue = previewStreamControl.getAudioFormat();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchStreamNotRunningException e) {
			// TODO Auto-generated catch block
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchStreamNotRunningException");
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getAudioFormat retValue =" + retValue);
		return retValue;
	}

	public boolean startMediaStream(ICatchWificamPreview previewStreamControl, ICatchFileStreamParam param, ICatchPreviewMode previewMode) {
		// TODO Auto-generated method stub
		WriteLogToDevice.writeLog("[Normal] -- PreviewStream: ", "begin startMediaStream previewStreamControl="+ previewStreamControl);
		boolean temp = false;
		try {
			temp = previewStreamControl.start(param, previewMode);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidArgumentException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchInvalidArgumentException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchStreamNotSupportException e) {
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchStreamNotSupportException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// JIRA ICOM-1705 End add by b.jiang 2015-08-06
		WriteLogToDevice.writeLog("[Normal] -- PreviewStream: ", "param = " + param);
		WriteLogToDevice.writeLog("[Normal] -- PreviewStream: ", "end startMediaStream temp =" + temp);
		return temp;
	}
	
	
	//start add by b.jiang 20160108
	public boolean enableAudio(ICatchWificamPreview previewStreamControl){
		WriteLogToDevice.writeLog("[Normal] -- PreviewStream: ", "start enableAudio");
		boolean value = false;
		try {
			value = previewStreamControl.enableAudio();
		} catch (IchSocketException e) {
			// TODO Auto-generated catch block
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchSocketException");
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			// TODO Auto-generated catch block
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchCameraModeException");
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			// TODO Auto-generated catch block
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchInvalidSessionException");
			e.printStackTrace();
		} catch (IchStreamNotSupportException e) {
			// TODO Auto-generated catch block
			WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchStreamNotSupportException");
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- PreviewStream: ", "end enableAudio value = " + value);
		return value;
	}
	//end by b.jiang 20160108
	
	//start add by b.jiang 20160108
		public boolean disableAudio(ICatchWificamPreview previewStreamControl){
			WriteLogToDevice.writeLog("[Normal] -- PreviewStream: ", "start disableAudio");
			boolean value = false;
			try {
				value = previewStreamControl.disableAudio();
			} catch (IchSocketException e) {
				// TODO Auto-generated catch block
				WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchSocketException");
				e.printStackTrace();
			} catch (IchCameraModeException e) {
				// TODO Auto-generated catch block
				WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchCameraModeException");
				e.printStackTrace();
			} catch (IchInvalidSessionException e) {
				// TODO Auto-generated catch block
				WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchInvalidSessionException");
				e.printStackTrace();
			} catch (IchStreamNotSupportException e) {
				// TODO Auto-generated catch block
				WriteLogToDevice.writeLog("[Error] -- PreviewStream: ", "IchStreamNotSupportException");
				e.printStackTrace();
			}
			WriteLogToDevice.writeLog("[Normal] -- PreviewStream: ", "end disableAudio value = " + value);
			return value;
		}
		//end by b.jiang 20160108
	
}
