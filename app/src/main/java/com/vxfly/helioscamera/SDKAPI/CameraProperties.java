/**
 * Added by zhangyanhu C01012,2014-6-23
 */
package com.vxfly.helioscamera.SDKAPI;

import java.util.List;

import android.util.Log;

import com.vxfly.helioscamera.data.FwToApp;
import com.vxfly.helioscamera.global.App.GlobalInfo;
import com.vxfly.helioscamera.global.App.PropertyId;
import com.vxfly.helioscamera.log.WriteLogToDevice;
import com.icatch.wificam.customer.ICatchWificamControl;
import com.icatch.wificam.customer.ICatchWificamProperty;
import com.icatch.wificam.customer.ICatchWificamUtil;
import com.icatch.wificam.customer.exception.IchCameraModeException;
import com.icatch.wificam.customer.exception.IchDevicePropException;
import com.icatch.wificam.customer.exception.IchInvalidArgumentException;
import com.icatch.wificam.customer.exception.IchInvalidSessionException;
import com.icatch.wificam.customer.exception.IchNoSDCardException;
import com.icatch.wificam.customer.exception.IchSocketException;
import com.icatch.wificam.customer.type.ICatchCodec;
import com.icatch.wificam.customer.type.ICatchLightFrequency;
import com.icatch.wificam.customer.type.ICatchMode;
import com.icatch.wificam.customer.type.ICatchVideoFormat;
import com.icatch.wificam.customer.type.ICatchVideoSize;

public class CameraProperties {
	private List<Integer> fuction;
	// private PreviewStream previewStream = new PreviewStream();
	private List<ICatchMode> modeList;
	private static CameraProperties instance;
	private ICatchWificamProperty cameraConfiguration;
	private ICatchWificamControl cameraAction;

	public static CameraProperties getInstance() {
		if (instance == null) {
			instance = new CameraProperties();
		}
		return instance;
	}

	private CameraProperties() {

	}

	public void initCameraProperties() {
		cameraConfiguration = GlobalInfo.getInstance().getCurrentCamera().getCameraPropertyClint();
		cameraAction = GlobalInfo.getInstance().getCurrentCamera().getcameraActionClient();
	}

	public List<String> getSupportedImageSizes() {
		List<String> list = null;
		try {
			list = cameraConfiguration.getSupportedImageSizes();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getSupportedImageSizes list.size =" + list.size());
		return list;
	}

	public List<String> getSupportedVideoSizes() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getSupportedVideoSizes");
		List<String> list = null;
		try {
			list = cameraConfiguration.getSupportedVideoSizes();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getSupportedVideoSizes size =" + list.size());
		return list;
	}

	public List<Integer> getSupportedWhiteBalances() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getSupportedWhiteBalances");
		List<Integer> list = null;
		try {
			list = cameraConfiguration.getSupportedWhiteBalances();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getSupportedWhiteBalances list.size() =" + list.size());
		return list;
	}

	public List<Integer> getSupportedCaptureDelays() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getSupportedCaptureDelays");
		List<Integer> list = null;
		try {
			list = cameraConfiguration.getSupportedCaptureDelays();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getSupportedCaptureDelays list.size() =" + list.size());
		return list;
	}

	public List<Integer> getSupportedLightFrequencys() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getSupportedLightFrequencys");
		List<Integer> list = null;

		try {
			list = cameraConfiguration.getSupportedLightFrequencies();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// delete LIGHT_FREQUENCY_AUTO because UI don't need this option
		for (int ii = 0; ii < list.size(); ii++) {
			if (list.get(ii) == ICatchLightFrequency.ICH_LIGHT_FREQUENCY_AUTO) {
				list.remove(ii);
			}
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getSupportedLightFrequencys list.size() =" + list.size());
		return list;
	}

	public boolean setImageSize(String value) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin setImageSize set value =" + value);
		boolean retVal = false;

		try {
			retVal = cameraConfiguration.setImageSize(value);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end setImageSize retVal=" + retVal);
		return retVal;
	}

	public boolean setVideoSize(String value) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin setVideoSize set value =" + value);
		boolean retVal = false;

		try {
			retVal = cameraConfiguration.setVideoSize(value);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end setVideoSize retVal=" + retVal);
		return retVal;
	}

	public boolean setWhiteBalance(int value) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin setWhiteBalanceset value =" + value);
		boolean retVal = false;
		if (value < 0 || value == 0xff) {
			return false;
		}
		try {
			retVal = cameraConfiguration.setWhiteBalance(value);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end setWhiteBalance retVal=" + retVal);
		return retVal;
	}

	public boolean setLightFrequency(int value) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin setLightFrequency set value =" + value);
		boolean retVal = false;
		if (value < 0 || value == 0xff) {
			return false;
		}
		try {
			retVal = cameraConfiguration.setLightFrequency(value);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end setLightFrequency retVal=" + retVal);
		return retVal;
	}

	public String getCurrentImageSize() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getCurrentImageSize");
		String value = "unknown";

		try {
			value = cameraConfiguration.getCurrentImageSize();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getCurrentImageSize value =" + value);
		return value;
	}

	public String getCurrentVideoSize() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getCurrentVideoSize");
		String value = "unknown";

		try {
			value = cameraConfiguration.getCurrentVideoSize();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getCurrentVideoSize value =" + value);
		return value;
	}

	public int getCurrentWhiteBalance() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getCurrentWhiteBalance");
		int value = 0xff;
		try {
			WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "******value=   " + value);
			value = cameraConfiguration.getCurrentWhiteBalance();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getCurrentWhiteBalance retvalue =" + value);
		return value;
	}

	public int getCurrentLightFrequency() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getCurrentLightFrequency");
		int value = 0xff;
		try {
			value = cameraConfiguration.getCurrentLightFrequency();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getCurrentLightFrequency value =" + value);
		return value;
	}

	public boolean setCaptureDelay(int value) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin setCaptureDelay set value =" + value);
		boolean retVal = false;

		try {
			WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start setCaptureDelay ");
			retVal = cameraConfiguration.setCaptureDelay(value);
			WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end setCaptureDelay ");
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end setCaptureDelay retVal =" + retVal);
		return retVal;
	}

	public int getCurrentCaptureDelay() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getCurrentCaptureDelay");
		int retVal = 0;

		try {
			retVal = cameraConfiguration.getCurrentCaptureDelay();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getCurrentCaptureDelay retVal =" + retVal);
		return retVal;
	}

	public int getCurrentDateStamp() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getCurrentDateStampType");
		int retValue = 0;
		try {
			retValue = cameraConfiguration.getCurrentDateStamp();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "getCurrentDateStampType retValue =" + retValue);
		return retValue;
	}

	/**
	 * 
	 * Added by zhangyanhu C01012,2014-4-1
	 */
	public boolean setDateStamp(int dateStamp) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin setDateStampType set value = " + dateStamp);
		Boolean retValue = false;
		try {
			retValue = cameraConfiguration.setDateStamp(dateStamp);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getCurrentVideoSize retValue =" + retValue);
		return retValue;
	}

	/**
	 * 
	 * Added by zhangyanhu C01012,2014-4-1
	 */
	public List<Integer> getDateStampList() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getDateStampList");
		List<Integer> list = null;
		try {
			list = cameraConfiguration.getSupportedDateStamps();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getDateStampList list.size ==" + list.size());
		return list;
	}

	public List<Integer> getSupportFuction() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getSupportFuction");
		List<Integer> fuction = null;
		// List<Integer> temp = null;
		try {
			fuction = cameraConfiguration.getSupportedProperties();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getSupportFuction fuction.size() =" + fuction.size());
		return fuction;
	}

	/**
	 * to prase the burst number Added by zhangyanhu C01012,2014-2-10
	 */
	public int getCurrentBurstNum() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getCurrentBurstNum");
		int number = 0xff;
		try {
			number = cameraConfiguration.getCurrentBurstNumber();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "getCurrentBurstNum num =" + number);
		return number;
	}

	public int getCurrentAppBurstNum() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getCurrentAppBurstNum");
		int number = 0xff;
		try {
			number = cameraConfiguration.getCurrentBurstNumber();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		number = FwToApp.getInstance().getAppBurstNum(number);
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "getCurrentAppBurstNum num =" + number);
		return number;
	}

	public boolean setCurrentBurst(int burstNum) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin setCurrentBurst set value = " + burstNum);
		if (burstNum < 0 || burstNum == 0xff) {
			return false;
		}
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setBurstNumber(burstNum);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end setCurrentBurst retValue =" + retValue);
		return retValue;
	}

	public int getRemainImageNum() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getRemainImageNum");
		int num = 0;
		try {
			num = cameraAction.getFreeSpaceInImages();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchNoSDCardException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchNoSDCardException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getRemainImageNum num =" + num);
		return num;
	}

	public int getRecordingRemainTime() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getRecordingRemainTimeInt");
		int recordingTime = 0;

		try {
			recordingTime = cameraAction.getRemainRecordingTime();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchNoSDCardException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getRecordingRemainTimeInt recordingTime =" + recordingTime);
		return recordingTime;
	}

	public boolean isSDCardExist() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin isSDCardExist");
		Boolean isReady = false;
		// JIRA ICOM-1577 Begin:Modify by b.jiang C01063 2015-07-17

		// try {
		// isReady = cameraAction.isSDCardExist();
		// } catch (IchSocketException e) {
		// WriteLogToDevice.writeLog("[Error] -- CameraProperties: ",
		// "IchSocketException");
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// } catch (IchCameraModeException e) {
		// WriteLogToDevice.writeLog("[Error] -- CameraProperties: ",
		// "IchCameraModeException");
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// } catch (IchInvalidSessionException e) {
		// WriteLogToDevice.writeLog("[Error] -- CameraProperties: ",
		// "IchInvalidSessionException");
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ",
		// "end isSDCardExist isReady =" + isReady);
		// return isReady;

		return GlobalInfo.isSdCardExist;
		// JIRA ICOM-1577 End:Modify by b.jiang C01063 2015-07-17
	}

	public int getBatteryElectric() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start getBatteryElectric");
		int electric = 0;
		try {
			electric = cameraAction.getCurrentBatteryLevel();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getBatteryElectric electric =" + electric);
		return electric;
	}

	public boolean supportVideoPlayback() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin hasVideoPlaybackFuction");
		boolean retValue = false;
		try {
			retValue = cameraAction.supportVideoPlayback();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchNoSDCardException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchNoSDCardException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "hasVideoPlaybackFuction retValue =" + retValue);
		return retValue;
		// return false;
	}

	public boolean cameraModeSupport(ICatchMode mode) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin cameraModeSupport  mode=" + mode);
		Boolean retValue = false;
		if (modeList == null) {
			modeList = getSupportedModes();
		}
		if (modeList.contains(mode)) {
			retValue = true;
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end cameraModeSupport retValue =" + retValue);
		return retValue;
	}

	public String getCameraMacAddress() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getCameraMacAddress macAddress macAddress ");
		String macAddress = cameraAction.getMacAddress();
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getCameraMacAddress macAddress =" + macAddress);
		return macAddress;
	}

	public List<Integer> getConvertSupportedImageSizes() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getConvertSupportedImageSizes");
		List<String> list = getSupportedImageSizes();
		List<Integer> convertImageSizeList = null;
		// ICatchWificamUtil sizeAss = new ICatchWificamUtil();
		try {
			convertImageSizeList = ICatchWificamUtil.convertImageSizes(list);
		} catch (IchInvalidArgumentException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getConvertSupportedImageSizes size =" + convertImageSizeList.size());
		return convertImageSizeList;
	}

	public List<ICatchVideoSize> getConvertSupportedVideoSizes() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getConvertSupportedVideoSizes");
		List<String> list = getSupportedVideoSizes();
		List<ICatchVideoSize> convertVideoSizeList = null;
		// ICatchWificamUtil sizeAss = new ICatchWificamUtil();
		try {
			convertVideoSizeList = ICatchWificamUtil.convertVideoSizes(list);
		} catch (IchInvalidArgumentException e) {
			// TODO Auto-generated catch block
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidArgumentException");
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ",
				"end getConvertSupportedVideoSizes convertVideoSizeList.size() =" + convertVideoSizeList.size());
		return convertVideoSizeList;
	}

	public boolean hasFuction(int fuc) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin hasFuction query fuction = " + fuc);
		if (fuction == null) {
			fuction = getSupportFuction();
		}
		Boolean retValue = false;
		if (fuction.contains(fuc) == true) {
			retValue = true;
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end hasFuction retValue =" + retValue);
		return retValue;
	}

	/**
	 * Added by zhangyanhu C01012,2014-7-4
	 */
	public List<Integer> getsupportedDateStamps() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getsupportedDateStamps");
		List<Integer> list = null;

		try {
			list = cameraConfiguration.getSupportedDateStamps();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getsupportedDateStamps list.size() =" + list.size());
		return list;
	}

	public List<Integer> getsupportedBurstNums() {
		// TODO Auto-generated method stub
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getsupportedBurstNums");
		List<Integer> list = null;

		try {
			list = cameraConfiguration.getSupportedBurstNumbers();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getsupportedBurstNums list.size() =" + list.size());
		return list;
	}

	/**
	 * Added by zhangyanhu C01012,2014-7-4
	 */
	public List<Integer> getSupportedFrequencies() {
		// TODO Auto-generated method stub
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getSupportedFrequencies");
		List<Integer> list = null;
		try {
			list = cameraConfiguration.getSupportedLightFrequencies();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getSupportedFrequencies list.size() =" + list.size());
		return list;
	}

	/**
	 * 
	 * Added by zhangyanhu C01012,2014-8-21
	 * 
	 * @return
	 */
	public List<ICatchMode> getSupportedModes() {
		WriteLogToDevice.writeLog("[Normal] -- CameraAction: ", "begin getSupportedModes");

		List<ICatchMode> list = null;
		try {
			list = cameraAction.getSupportedModes();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraAction: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraAction: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraAction: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraAction: ", "end getSupportedModes list =" + list.size());
		return list;
	}

	// public List<Integer> getSupportedTimeLapseStillDurations() {
	// WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ",
	// "begin getSupportedTimeLapseStillDurations");
	// List<Integer> list = null;
	// //boolean retValue = false;
	// try {
	// list = cameraConfiguration.getSupportedTimeLapseStillDurations();
	// } catch (IchSocketException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (IchCameraModeException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (IchDevicePropException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (IchInvalidSessionException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ",
	// "end getSupportedTimeLapseStillDurations list =" + list.size());
	// return list;
	// }
	//
	// public List<Integer> getSupportedTimeLapseStillintervals() {
	// WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ",
	// "begin getSupportedTimeLapseStillintervals");
	// List<Integer> list = null;
	// //boolean retValue = false;
	// try {
	// list = cameraConfiguration.getSupportedTimeLapseStillIntervals();
	// } catch (IchSocketException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (IchCameraModeException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (IchDevicePropException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// } catch (IchInvalidSessionException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ",
	// "end getSupportedTimeLapseStillintervals list =" + list.size());
	// return list;
	// }

	public List<Integer> getSupportedTimeLapseDurations() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getSupportedTimeLapseDurations");
		List<Integer> list = null;
		// boolean retValue = false;
		try {
			list = cameraConfiguration.getSupportedTimeLapseDurations();
		} catch (IchSocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int ii = 0; ii < list.size(); ii++) {
			WriteLogToDevice.writeLog("[Normal] -- CameraProperties:", "list.get(ii) =" + list.get(ii));
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getSupportedTimeLapseDurations list =" + list.size());
		return list;
	}

	public List<Integer> getSupportedTimeLapseIntervals() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getSupportedTimeLapseIntervals");
		List<Integer> list = null;
		// boolean retValue = false;
		try {
			list = cameraConfiguration.getSupportedTimeLapseIntervals();
		} catch (IchSocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int ii = 0; ii < list.size(); ii++) {
			WriteLogToDevice.writeLog("[Normal] -- CameraProperties:", "list.get(ii) =" + list.get(ii));
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getSupportedTimeLapseIntervals list =" + list.size());
		return list;
	}

	public boolean setTimeLapseDuration(int timeDuration) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin setTimeLapseDuration videoDuration =" + timeDuration);
		boolean retVal = false;
//		if (timeDuration < 0 || timeDuration == 0xff) {
//			return false;
//		}
		try {
			retVal = cameraConfiguration.setTimeLapseDuration(timeDuration);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end setTimeLapseDuration retVal=" + retVal);
		return retVal;
	}

	public int getCurrentTimeLapseDuration() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getCurrentTimeLapseDuration");
		int retVal = 0xff;
		try {
			retVal = cameraConfiguration.getCurrentTimeLapseDuration();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getCurrentTimeLapseDuration retVal=" + retVal);
		return retVal;
	}

	public boolean setTimeLapseInterval(int timeInterval) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin setTimeLapseInterval videoDuration =" + timeInterval);
		boolean retVal = false;
		//JIRA ICOM-1764 Start modify by b.jiang 20160126
//		if (timeInterval < 0 || timeInterval == 0xff) {
//			return false;
//		}
		//JIRA ICOM-1764 Start modify by b.jiang 20160126
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "timeInterval=" + timeInterval);
		
		try {
			retVal = cameraConfiguration.setTimeLapseInterval(timeInterval);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end setTimeLapseInterval retVal=" + retVal);
		return retVal;
	}

	public int getCurrentTimeLapseInterval() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getCurrentTimeLapseInterval");
		int retVal = 0xff;
		try {
			retVal = cameraConfiguration.getCurrentTimeLapseInterval();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getCurrentTimeLapseInterval retVal=" + retVal);
		return retVal;
	}

	public int getMaxZoomRatio() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start getMaxZoomRatio");
		int retValue = 0;
		try {
			retValue = cameraConfiguration.getMaxZoomRatio();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getMaxZoomRatio retValue =" + retValue);
		return retValue;
	}

	public int getCurrentZoomRatio() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start getCurrentZoomRatio");
		int retValue = 0;
		try {
			retValue = cameraConfiguration.getCurrentZoomRatio();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getCurrentZoomRatio retValue =" + retValue);
		return retValue;
	}

	public int getCurrentUpsideDown() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start getCurrentUpsideDown");
		int retValue = 0;
		try {
			retValue = cameraConfiguration.getCurrentUpsideDown();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getCurrentUpsideDown retValue =" + retValue);
		return retValue;
	}

	public boolean setUpsideDown(int upside) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start setUpsideDown upside = " + upside);
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setUpsideDown(upside);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end setUpsideDown retValue =" + retValue);
		return retValue;
	}

	public int getCurrentSlowMotion() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start getCurrentSlowMotion");
		int retValue = 0;
		try {
			retValue = cameraConfiguration.getCurrentSlowMotion();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getCurrentSlowMotion retValue =" + retValue);
		return retValue;
	}

	public boolean setSlowMotion(int slowMotion) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start setSlowMotion slowMotion = " + slowMotion);
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setSlowMotion(slowMotion);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end setSlowMotion retValue =" + retValue);
		return retValue;
	}

	public boolean setCameraDate(String date) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start setCameraDate date = " + date);
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setStringPropertyValue(PropertyId.CAMERA_DATE, date);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end setCameraDate retValue =" + retValue);
		return retValue;
	}

	public boolean setCameraEssidName(String ssidName) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start setCameraEssidName date = " + ssidName);
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setStringPropertyValue(PropertyId.ESSID_NAME, ssidName);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end setCameraEssidName retValue =" + retValue);
		return retValue;
	}

	public String getCameraEssidName() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start getCameraEssidName");
		String retValue = null;
		try {
			retValue = cameraConfiguration.getCurrentStringPropertyValue(PropertyId.ESSID_NAME);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getCameraEssidName retValue =" + retValue);
		return retValue;
	}

	public String getCameraEssidPassword() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start getCameraEssidPassword");
		String retValue = null;
		try {
			retValue = cameraConfiguration.getCurrentStringPropertyValue(PropertyId.ESSID_PASSWORD);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getCameraEssidPassword retValue =" + retValue);
		return retValue;
	}

	public boolean setCameraEssidPassword(String ssidPassword) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start setStringPropertyValue date = " + ssidPassword);
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setStringPropertyValue(PropertyId.ESSID_PASSWORD, ssidPassword);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end setCameraSsid retValue =" + retValue);
		return retValue;
	}

	public boolean setCameraSsid(String ssid) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start setCameraSsid date = " + ssid);
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setStringPropertyValue(PropertyId.CAMERA_ESSID, ssid);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end setCameraSsid retValue =" + retValue);
		return retValue;
	}

	public boolean setCameraName(String cameraName) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start setStringPropertyValue cameraName = " + cameraName);
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setStringPropertyValue(PropertyId.CAMERA_NAME, cameraName);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end setStringPropertyValue retValue =" + retValue);
		return retValue;
	}

	public String getCameraName() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start getCameraName");
		String retValue = null;
		try {
			retValue = cameraConfiguration.getCurrentStringPropertyValue(PropertyId.CAMERA_NAME);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getCameraName retValue =" + retValue);
		return retValue;
	}

	public String getCameraName(ICatchWificamProperty cameraConfiguration1) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start getCameraName");
		String retValue = null;
		try {
			retValue = cameraConfiguration1.getCurrentStringPropertyValue(PropertyId.CAMERA_NAME);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getCameraName retValue =" + retValue);
		return retValue;
	}

	public String getCameraPasswordNew() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start getCameraPassword");
		String retValue = null;
		try {
			retValue = cameraConfiguration.getCurrentStringPropertyValue(PropertyId.CAMERA_PASSWORD_NEW);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getCameraPassword retValue =" + retValue);
		return retValue;
	}

	public boolean setCameraPasswordNew(String cameraNamePassword) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start setCameraPasswordNew cameraName = " + cameraNamePassword);
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setStringPropertyValue(PropertyId.CAMERA_PASSWORD_NEW, cameraNamePassword);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end setCameraPasswordNew retValue =" + retValue);
		return retValue;
	}

	public String getCameraSsid() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start getCameraSsid date = ");
		String retValue = null;
		try {
			retValue = cameraConfiguration.getCurrentStringPropertyValue(PropertyId.CAMERA_ESSID);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getCameraSsid retValue =" + retValue);
		return retValue;
	}

	public boolean setCameraPassword(String password) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start setCameraSsid date = " + password);
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setStringPropertyValue(PropertyId.CAMERA_PASSWORD, password);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end setCameraSsid retValue =" + retValue);
		return retValue;
	}

	public String getCameraPassword() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start getCameraPassword date = ");
		String retValue = null;
		try {
			retValue = cameraConfiguration.getCurrentStringPropertyValue(PropertyId.CAMERA_PASSWORD);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getCameraPassword retValue =" + retValue);
		return retValue;
	}

	public boolean setCaptureDelayMode(int value) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start setCaptureDelayMode value = " + value);
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setPropertyValue(PropertyId.CAPTURE_DELAY_MODE, value);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end setCaptureDelayMode retValue =" + retValue);
		return retValue;
	}
	
	public int getCurrentCaptureDelayMode() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start getCurrentCaptureDelayMode");
		int retValue = -1;
		try {
			retValue = cameraConfiguration.getCurrentPropertyValue(PropertyId.CAPTURE_DELAY_MODE);
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getCurrentCaptureDelayMode retValue =" + retValue);
		return retValue;
	}

	public int getVideoRecordingTime() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start getRecordingTime");
		int retValue = 0;
		try {
			// JIRA ICOM-1608 Begin:Modify by b.jiang C01063 2015-07-20
			// 0xD7F0 -> PropertyId.VIDEO_RECORDING_TIME
			// retValue = cameraConfiguration.getCurrentPropertyValue(0xD7F0);
			retValue = cameraConfiguration.getCurrentPropertyValue(PropertyId.VIDEO_RECORDING_TIME);
			// JIRA ICOM-1608 End:Modify by b.jiang C01063 2015-07-20
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getRecordingTime retValue =" + retValue);
		return retValue;
	}

	public boolean setServiceEssid(String value) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start setServiceEssid value = " + value);
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setStringPropertyValue(PropertyId.SERVICE_ESSID, value);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end setServiceEssid retValue =" + retValue);
		return retValue;
	}

	public boolean setServicePassword(String value) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start setServicePassword value = " + value);
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setStringPropertyValue(PropertyId.SERVICE_PASSWORD, value);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end setServicePassword retValue =" + retValue);
		return retValue;
	}

	public boolean notifyFwToShareMode(int value) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start notifyFwToShareMode value = " + value);
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setPropertyValue(PropertyId.NOTIFY_FW_TO_SHARE_MODE, value);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end notifyFwToShareMode retValue =" + retValue);
		return retValue;
	}

	public List<Integer> getSupportedPropertyValues(int propertyId) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "begin getSupportedPropertyValues propertyId =" + propertyId);
		List<Integer> list = null;
		try {
			list = cameraConfiguration.getSupportedPropertyValues(propertyId);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getSupportedPropertyValues list.size() =" + list.size());
		return list;
	}

	public int getCurrentPropertyValue(int propertyId) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start getCurrentPropertyValue propertyId = " + propertyId);
		int retValue = 0;
		try {
			retValue = cameraConfiguration.getCurrentPropertyValue(propertyId);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getCurrentPropertyValue retValue =" + retValue);
		return retValue;
	}

	public String getCurrentStringPropertyValue(int propertyId) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start getCurrentStringPropertyValue propertyId = " + propertyId);
		String retValue = null;
		try {
			retValue = cameraConfiguration.getCurrentStringPropertyValue(propertyId);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getCurrentStringPropertyValue retValue =" + retValue);
		return retValue;
	}

	public boolean setPropertyValue(int propertyId, int value) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start setPropertyValue value = " + value);
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setPropertyValue(propertyId, value);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end setPropertyValue retValue =" + retValue);
		return retValue;
	}

	public boolean setStringPropertyValue(int propertyId, String value) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start setStringPropertyValue value = " + value);
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setStringPropertyValue(propertyId, value);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end setStringPropertyValue retValue =" + retValue);
		return retValue;
	}

	public int getVideoSizeFlow() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start getVideoSizeFlow");
		int retValue = 0;
		try {
			retValue = cameraConfiguration.getCurrentPropertyValue(PropertyId.VIDEO_SIZE_FLOW);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getVideoSizeFlow retValue =" + retValue);
		return retValue;
	}

	public boolean notifyCameraConnectChnage(int value) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start notifyCameraConnectChnage value = " + value);
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setPropertyValue(PropertyId.CAMERA_CONNECT_CHANGE, value);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end notifyCameraConnectChnage retValue =" + retValue);
		return retValue;
	}

	public List<ICatchVideoFormat> getResolutionList() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start getResolution");
		List<ICatchVideoFormat> retList = null;
		try {
			retList = cameraConfiguration.getSupportedStreamingInfos();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getResolution retList.size() =" + retList.size());
		return retList;
	}

	public String getBestResolution() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start getBestResolution");
		String bestResolution = null;

		List<ICatchVideoFormat> tempList = getResolutionList(cameraConfiguration);
		if (tempList == null || tempList.size() == 0) {
			return null;
		}
		Log.d("1111", "getResolutionList() tempList.size() = " + tempList.size());
		int tempWidth = 0;
		int tempHeigth = 0;

		ICatchVideoFormat temp;

		for (int ii = 0; ii < tempList.size(); ii++) {
			temp = tempList.get(ii);
			if (temp.getCodec() == ICatchCodec.ICH_CODEC_H264) {
				if (bestResolution == null) {
					bestResolution = "H264?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS=" + temp.getFps() + "&";
				}

				if (temp.getVideoW() == 640 && temp.getVideoH() == 360) {
					bestResolution = "H264?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS=" + temp.getFps() + "&";
					return bestResolution;
				} else if (temp.getVideoW() == 640 && temp.getVideoH() == 480) {
					if (tempWidth != 640) {
						bestResolution = "H264?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS=" + temp.getFps()
								+ "&";
						tempWidth = 640;
						tempHeigth = 480;
					}
				} else if (temp.getVideoW() == 720) {
					if (tempWidth != 640) {
						if (temp.getVideoW() * 9 == temp.getVideoH() * 16)// 16:9优先
						{
							bestResolution = "H264?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS="
									+ temp.getFps() + "&";
							tempWidth = 720;
							tempHeigth = temp.getVideoH();
						} else if (temp.getVideoW() * 3 == temp.getVideoH() * 4)// 4:3
						{
							if (tempWidth != 720)
								bestResolution = "H264?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS="
										+ temp.getFps() + "&";
							tempWidth = 720;
							tempHeigth = temp.getVideoH();
						}
					}
				} else if (temp.getVideoW() < tempWidth) {
					if (temp.getVideoW() * 9 == temp.getVideoH() * 16)// 16:9优先
					{
						bestResolution = "H264?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS=" + temp.getFps()
								+ "&";
						tempWidth = temp.getVideoW();
						tempHeigth = temp.getVideoH();
					} else if (temp.getVideoW() * 3 == temp.getVideoH() * 4)// 4:3
					{
						if (tempWidth != temp.getVideoW())
							bestResolution = "H264?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS="
									+ temp.getFps() + "&";
						tempWidth = temp.getVideoW();
						tempHeigth = temp.getVideoH();
					}
				}
			}
		}
		if (bestResolution != null) {
			return bestResolution;
		}
		for (int ii = 0; ii < tempList.size(); ii++) {
			temp = tempList.get(ii);
			if (temp.getCodec() == ICatchCodec.ICH_CODEC_JPEG) {
				if (bestResolution == null) {
					bestResolution = "MJPG?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS=" + temp.getFps() + "&";
				}

				if (temp.getVideoW() == 640 && temp.getVideoH() == 360) {
					bestResolution = "MJPG?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS=" + temp.getFps() + "&";
					return bestResolution;
				} else if (temp.getVideoW() == 640 && temp.getVideoH() == 480) {
					if (tempWidth != 640) {
						bestResolution = "MJPG?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS=" + temp.getFps()
								+ "&";
						tempWidth = 640;
						tempHeigth = 480;
					}
				} else if (temp.getVideoW() == 720) {
					if (tempWidth != 640) {
						if (temp.getVideoW() * 9 == temp.getVideoH() * 16)// 16:9优先
						{
							bestResolution = "MJPG?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS="
									+ temp.getFps() + "&";
							tempWidth = 720;
							tempHeigth = temp.getVideoH();
						} else if (temp.getVideoW() * 3 == temp.getVideoH() * 4)// 4:3
						{
							if (tempWidth != 720)
								bestResolution = "MJPG?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS="
										+ temp.getFps() + "&";
							tempWidth = 720;
							tempHeigth = temp.getVideoH();
						}
					}
				} else if (temp.getVideoW() < tempWidth) {
					if (temp.getVideoW() * 9 == temp.getVideoH() * 16)// 16:9优先
					{
						bestResolution = "MJPG?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS=" + temp.getFps()
								+ "&";
						tempWidth = temp.getVideoW();
						tempHeigth = temp.getVideoH();
					} else if (temp.getVideoW() * 3 == temp.getVideoH() * 4)// 4:3
					{
						if (tempWidth != temp.getVideoW())
							bestResolution = "MJPG?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS="
									+ temp.getFps() + "&";
						tempWidth = temp.getVideoW();
						tempHeigth = temp.getVideoH();
					}
				}
			}
		}

		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getBestResolution");
		return bestResolution;

	}

	public List<ICatchVideoFormat> getResolutionList(ICatchWificamProperty cameraConfiguration) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start getResolutionList");
		List<ICatchVideoFormat> retList = null;
		try {
			retList = cameraConfiguration.getSupportedStreamingInfos();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int ii = 0; ii < retList.size(); ii++) {
			Log.d("1111", " retList.get(ii)==" + retList.get(ii));
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getResolutionList retList.size() =" + retList.size());
		return retList;
	}

	public String getAppDefaultResolution() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start getAppDefaultResolution");
		String appDefaultResolution = null;

		List<ICatchVideoFormat> tempList = getResolutionList(cameraConfiguration);
		if (tempList == null || tempList.size() == 0) {
			return null;
		}
		Log.d("1111", "getResolutionList() tempList.size() = " + tempList.size());

		ICatchVideoFormat temp;

		for (int ii = 0; ii < tempList.size(); ii++) {
			temp = tempList.get(ii);

			if (temp.getCodec() == ICatchCodec.ICH_CODEC_H264) {
				if (temp.getVideoW() == 1280 && temp.getVideoH() == 720 && temp.getBitrate() == 500000) {
					appDefaultResolution = "H264?" + "W=" + temp.getVideoW() + "&H=" + temp.getVideoH() + "&BR=" + temp.getBitrate() + "&FPS=" + temp.getFps()
							+ "&";
					return appDefaultResolution;
				}
			}
		}

		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getAppDefaultResolution");
		return appDefaultResolution;

	}

	public String getFWDefaultResolution() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start getFWDefaultResolution");
		String resolution = null;
		ICatchVideoFormat retValue = null;
		try {
			retValue = cameraConfiguration.getCurrentStreamingInfo();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (retValue != null) {
			if (retValue.getCodec() == ICatchCodec.ICH_CODEC_H264) {
				resolution = "H264?" + "W=" + retValue.getVideoW() + "&H=" + retValue.getVideoH() + "&BR=" + retValue.getBitrate() + "&FPS="
						+ retValue.getFps() + "&";
			} else if (retValue.getCodec() == ICatchCodec.ICH_CODEC_JPEG) {
				resolution = "MJPG?" + "W=" + retValue.getVideoW() + "&H=" + retValue.getVideoH() + "&BR=" + retValue.getBitrate() + "&FPS="
						+ retValue.getFps() + "&";
			}
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getFWDefaultResolution");
		return resolution;

	}

	public boolean setStreamingInfo(ICatchVideoFormat iCatchVideoFormat) {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start setStreamingInfo");
		boolean retValue = false;
		try {
			retValue = cameraConfiguration.setStreamingInfo(iCatchVideoFormat);
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end setStreamingInfo");
		return retValue;

	}

	public String getCurrentStreamInfo() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start getCurrentStreamInfo");

		ICatchVideoFormat retValue = null;
		String bestResolution = null;
		try {
			retValue = cameraConfiguration.getCurrentStreamingInfo();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (retValue == null) {
			WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getCurrentStreamInfo retValue = " + retValue);
			return null;
		}
		//JIRA ICOM-2520 Start add by b.jiang 2015-12-23
		//判断是否支援property code 0XD7AE，如果支援送带FPS的，不支援就不送带fps的
		if (hasFuction(0xd7ae)) {
			if (retValue.getCodec() == ICatchCodec.ICH_CODEC_H264) {
				bestResolution = "H264?" + "W=" + retValue.getVideoW() + "&H=" + retValue.getVideoH() + "&BR=" + retValue.getBitrate() + "&FPS="
						+ retValue.getFps() + "&";
			} else if (retValue.getCodec() == ICatchCodec.ICH_CODEC_JPEG) {
				bestResolution = "MJPG?" + "W=" + retValue.getVideoW() + "&H=" + retValue.getVideoH() + "&BR=" + retValue.getBitrate() + "&FPS="
						+ retValue.getFps() + "&";
			}
		} else {
			if (retValue.getCodec() == ICatchCodec.ICH_CODEC_H264) {
				bestResolution = "H264?" + "W=" + retValue.getVideoW() + "&H=" + retValue.getVideoH() + "&BR=" + retValue.getBitrate();
			} else if (retValue.getCodec() == ICatchCodec.ICH_CODEC_JPEG) {
				bestResolution = "MJPG?" + "W=" + retValue.getVideoW() + "&H=" + retValue.getVideoH() + "&BR=" + retValue.getBitrate();
			}
		}
		//JIRA ICOM-2520 End add by b.jiang 2015-12-23

		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getCurrentStreamInfo bestResolution =" + bestResolution);
		return bestResolution;
	}

	public int getPreviewCacheTime() {
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "start getPreviewCacheTime");
		int retValue = 0;
		try {
			retValue = cameraConfiguration.getPreviewCacheTime();
		} catch (IchSocketException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchSocketException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchCameraModeException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchInvalidSessionException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchInvalidSessionException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			WriteLogToDevice.writeLog("[Error] -- CameraProperties: ", "IchDevicePropException");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WriteLogToDevice.writeLog("[Normal] -- CameraProperties: ", "end getPreviewCacheTime retValue =" + retValue);
		return retValue;
	}

	public int getCameraTimeLapseVideoSizeListMask() {
		int retValue = 0;
		try {
			retValue = cameraConfiguration.getCurrentPropertyValue(PropertyId.TIMELAPSE_VIDEO_SIZE_LIST_MASK);
		} catch (IchInvalidSessionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchSocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchCameraModeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IchDevicePropException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return retValue;
	}

}
