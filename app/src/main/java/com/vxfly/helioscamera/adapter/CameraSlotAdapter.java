package com.vxfly.helioscamera.adapter;

import java.util.List;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vxfly.helioscamera.global.App.CameraSlot;
import com.vxfly.helioscamera.global.App.GlobalInfo;
import com.vxfly.helioscamera.log.WriteLogToDevice;
import com.vxfly.helioscamera.R;

public class CameraSlotAdapter extends BaseAdapter {
	private List<CameraSlot> camSlotList;
	private Context context;
	private int listViewHeight = 0;
	private Handler myHandler;

	public CameraSlotAdapter(Context context, List<CameraSlot> camSlotList, int height, Handler handler) {
		this.context = context;
		this.camSlotList = camSlotList;
		this.listViewHeight = height;
		this.myHandler = handler;
		WriteLogToDevice.writeLog("[Normal] -- CameraSlotAdapter: ", "listViewHeight=" + listViewHeight);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return camSlotList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int arg0, View convertView, ViewGroup arg2) {
		CameraSlot camSlotItem = camSlotList.get(arg0);
		final int arg = arg0;
		if (camSlotItem.isOccupied) {
			convertView = LayoutInflater.from(context).inflate(R.layout.cam_slot_item_recorded, null);
			TextView slotConnectState = (TextView) convertView.findViewById(R.id.slot_connect_state);
			ImageView slotConnectSign = (ImageView) convertView.findViewById(R.id.slot_connect_sign);
			TextView slotCameraName = (TextView) convertView.findViewById(R.id.slot_camera_name);
			ImageView slotPhoto = (ImageView) convertView.findViewById(R.id.slotPhoto);
			LinearLayout slot_layout = (LinearLayout) convertView.findViewById(R.id.slot_layout);
			LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) slot_layout.getLayoutParams();
			ImageView deleteCamera = (ImageView) convertView.findViewById(R.id.delete_camera);
			deleteCamera.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					myHandler.obtainMessage(GlobalInfo.MESSAGE_DELETE_CAMERA, arg).sendToTarget();
				}
			});

			params.height = (int) (listViewHeight / 7);
			slot_layout.setLayoutParams(params);
			byte[] imageBuf = camSlotItem.cameraPhoto;
			if (imageBuf != null && imageBuf.length > 0) {
				Bitmap imageBitmap = BitmapFactory.decodeByteArray(imageBuf, 0, imageBuf.length);
				slotPhoto.setImageBitmap(imageBitmap);

			} else {
				slotPhoto.setImageBitmap(null);
			}
			if (camSlotItem.enableConnect) {
				slotConnectState.setTextColor(context.getResources().getColor(R.color.cambridge_blue));
				slotConnectState.setText(context.getResources().getString(R.string.text_connected));
				slotConnectSign.setImageDrawable(context.getResources().getDrawable(R.drawable.connected_sign));
			} else {
				slotConnectState.setTextColor(context.getResources().getColor(R.color.graywhite));
				slotConnectState.setText(context.getResources().getString(R.string.text_disconnect));
				slotConnectSign.setImageDrawable(context.getResources().getDrawable(R.drawable.disconnected_sign));
			}
			slotCameraName.setText(camSlotItem.cameraName);
		} else {
			convertView = LayoutInflater.from(context).inflate(R.layout.cam_slot_item_add, null);
			LinearLayout slot_layout_add = (LinearLayout) convertView.findViewById(R.id.slot_layout_add);
			LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) slot_layout_add.getLayoutParams();
			Log.d("1111", "params = " + params);
			params.height = (int) (listViewHeight / 7);
			slot_layout_add.setLayoutParams(params);
		}
		WriteLogToDevice.writeLog("[Normal] -- Appstart: ", "CameraSlotAdapter height=");
		return convertView;
	}

	public final class ViewHolder {
		LinearLayout slot_layout;
		TextView slotConnectState;
		TextView slotCameraName;
		ImageView slotPhoto;
	}
}
