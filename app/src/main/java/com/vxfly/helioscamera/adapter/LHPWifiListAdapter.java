package com.vxfly.helioscamera.adapter;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.vxfly.helioscamera.R;

import java.util.HashMap;
import java.util.List;

/**
 * Created by kaifa on 2016/12/29.
 */
public class LHPWifiListAdapter extends BaseAdapter {
    LayoutInflater inflater;
    List<ScanResult> list;
    private Context context;

    private boolean bSelected;
    private int currentSelected;

    public LHPWifiListAdapter(Context context, List<ScanResult> list) {
        // TODO Auto-generated constructor stub
        this.inflater = LayoutInflater.from(context);
        this.list = list;
        this.context = context;
        this.bSelected=false;
        this.currentSelected=0;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return list.size();
    }

    @Override
    public String getItem(int position) {
        // TODO Auto-generated method stub
        return list.get(position).SSID;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
//        View view = null;
//        view = inflater.inflate(R.layout.wifi_status, null);
//        ScanResult scanResult = list.get(position);
//        TextView wifiName = (TextView) view.findViewById(R.id.wifi_name);
//        wifiName.setText(scanResult.SSID);
//        ImageView imageView = (ImageView) view.findViewById(R.id.wifi_signal);
//        WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
//        int level = wifiManager.calculateSignalLevel(scanResult.level, 5);
//        // 判断信号强度，显示对应的指示图标
//        if (level == 4) {
//            imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.wifi_4));
//        } else if (level == 3) {
//            imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.wifi_3));
//        } else if (level <= 2) {
//            imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.wifi_2));
//        }
//        return view;


        //////////////////////
        ViewHolder holder=null;
        View view=convertView;
        if (view==null){
            view = inflater.inflate(R.layout.wifi_scan_list, parent,false);
            holder=new ViewHolder();
            assert view != null;
            holder.textViewWifiName =(TextView) view.findViewById(R.id.scan_wifi_ssid);
            holder.imageViewWifiLevel = (ImageView) view.findViewById(R.id.wifi_strong_level);
            view.setTag(holder);
        }else {
            holder = (ViewHolder) view.getTag();
        }

        ScanResult scanResult = list.get(position);
        holder.textViewWifiName.setText(scanResult.SSID);
        WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        int level = wifiManager.calculateSignalLevel(scanResult.level, 5);
        // 判断信号强度，显示对应的指示图标
        if (level == 4) {
            holder.imageViewWifiLevel.setImageDrawable(context.getResources().getDrawable(R.drawable.wifi_4));
        } else if (level == 3) {
            holder.imageViewWifiLevel.setImageDrawable(context.getResources().getDrawable(R.drawable.wifi_3));
        } else if (level <= 2) {
            holder.imageViewWifiLevel.setImageDrawable(context.getResources().getDrawable(R.drawable.wifi_2));
        }

        if(currentSelected==position){
            view.setBackgroundResource(R.color.blue);
        }else{
            view.setBackgroundResource(R.color.black);
        }
        view.invalidate();
        return view;
    }

    /**
     * Added by zhangyanhu C01012,2014-4-4
     */
    private WifiManager getSystemService(String wifiService) {
        // TODO Auto-generated method stub
        return null;
    }

    int getCurrentSelectedIndex(){
        return currentSelected;
    }

    public  void nextItemSelected(){
        if(currentSelected>=0 && currentSelected <list.size()-1) {
            currentSelected++;
            setCurrentItemSelected();
        }
    }
    public  boolean preItemSelected(){
        if(currentSelected>0 && currentSelected <list.size()-1) {
            currentSelected--;
            setCurrentItemSelected();
            return true;
        }else{
            return false;
        }
    }
    public void downItemSelected(){
        final int colNum = 3;
        int ri = currentSelected/colNum;
        int mi = currentSelected%colNum;

        int index = (ri+1)*colNum + mi;
        if(index < list.size()-1){
            currentSelected  = index;
            setCurrentItemSelected();
        }
    }

    public void upItemSelected(){
        final int colNum = 3;
        int ri = currentSelected/colNum;
        int mi = currentSelected%colNum;

        int index = (ri-1)*colNum + mi;
        if(index >=0 ){
            currentSelected  = index;
            setCurrentItemSelected();
        }

    }

    public  void setCurrentItemSelected(){
        bSelected=true;
        //通知数据有改变
        notifyDataSetChanged();
    }

    public void setIsSelected(boolean isSelected) {
        this.bSelected = isSelected;
    }

    public ScanResult getCurrentItemScanResult(){
        return list.get(currentSelected);
    }


   private final class ViewHolder{
       public TextView textViewWifiName;
       public ImageView imageViewWifiLevel;
   }
}
