package com.vxfly.helioscamera.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.vxfly.helioscamera.function.SettingView;
import com.vxfly.helioscamera.global.App.GlobalInfo;
import com.vxfly.helioscamera.global.App.SettingMenu;
import com.vxfly.helioscamera.R;

/**
 * 
 * @author zhangyanhu_C01012
 * 
 */
public class OptionListAdapter extends BaseAdapter {
	private Context context;
	private SettingView.SettingHandler settingHandler;
	private List<SettingMenu> menuList;

	public OptionListAdapter(Context context, SettingView.SettingHandler settingHandler, List<SettingMenu> menuList) {
		this.context = context;
		this.settingHandler = settingHandler;
		this.menuList = menuList;
	}

	@Override
	 public int getCount() {
		return menuList.size();
		
		
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (menuList.get(position).name == R.string.setting_auto_download) {
			convertView = LayoutInflater.from(context).inflate(R.layout.auto_download_layout, null);
			final ToggleButton toggleButton = (ToggleButton) convertView.findViewById(R.id.switcher);
			toggleButton.setChecked(GlobalInfo.autoDownloadAllow);
			toggleButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					settingHandler.obtainMessage(SettingView.SETTING_OPTION_AUTO_DOWNLOAD, toggleButton.isChecked()).sendToTarget();
				}
			});

			return convertView;
		}

		if (menuList.get(position).name == R.string.setting_auto_download_size_limit) {
			convertView = LayoutInflater.from(context).inflate(R.layout.auto_download_layout_size, null);
			final EditText autoDownloadSize = (EditText) convertView.findViewById(R.id.download_size);
			autoDownloadSize.setText(GlobalInfo.autoDownloadSizeLimit + "");
			autoDownloadSize.setOnFocusChangeListener(new android.view.View.OnFocusChangeListener() {
				@Override
				public void onFocusChange(View v, boolean hasFocus) {
					if (hasFocus) {
					} else {
						settingHandler.obtainMessage(SettingView.SETTING_OPTION_AUTO_DOWNLOAD_SIZE, autoDownloadSize.getText().toString())
								.sendToTarget();
					}
				}
			});
			return convertView;
		}
		convertView = LayoutInflater.from(context).inflate(R.layout.setup_mainmenu_item, null);
		holder = new ViewHolder();
		holder.title = (TextView) convertView.findViewById(R.id.item_text);
		holder.text = (TextView) convertView.findViewById(R.id.item_value);
		convertView.setTag(holder);
		holder.title.setText(menuList.get(position).name);
		holder.text.setText(menuList.get(position).value);
		int tempName = menuList.get(position).name;

		if (tempName == R.string.setting_app_version || tempName == R.string.setting_product_name
				|| tempName == R.string.setting_firmware_version) {
			holder.title.setTextColor(context.getResources().getColor(R.color.gray));
		} else {
			holder.title.setTextColor(context.getResources().getColor(R.color.white));
		}

		holder.text.setTextColor(context.getResources().getColor(R.color.gray));
		return convertView;
	}

	public final class ViewHolder {
		public TextView title;
		public TextView text;
	}
}
