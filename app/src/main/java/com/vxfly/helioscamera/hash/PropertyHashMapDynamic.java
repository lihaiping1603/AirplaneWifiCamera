package com.vxfly.helioscamera.hash;

import java.util.HashMap;
import java.util.List;

import android.util.Log;

import com.vxfly.helioscamera.SDKAPI.CameraProperties;
import com.vxfly.helioscamera.baseItems.ItemInfo;
import com.vxfly.helioscamera.global.App.PropertyId;
import com.vxfly.helioscamera.log.WriteLogToDevice;
import com.icatch.wificam.customer.ICatchWificamUtil;
import com.icatch.wificam.customer.exception.IchInvalidArgumentException;
import com.icatch.wificam.customer.type.ICatchImageSize;
import com.icatch.wificam.customer.type.ICatchVideoSize;

public class PropertyHashMapDynamic {

	// public static HashMap<String, ItemInfo> imageSizeMap = new
	// HashMap<String, ItemInfo>();

	private static PropertyHashMapDynamic propertyHashMap;

	public static PropertyHashMapDynamic getInstance() {
		if (propertyHashMap == null) {
			propertyHashMap = new PropertyHashMapDynamic();
		}
		return propertyHashMap;
	}

	public HashMap<Integer, ItemInfo> getDynamicHashInt(int propertyId) {
		switch (propertyId) {
		case PropertyId.CAPTURE_DELAY:
			return getCaptureDelayMap();

		default:
			return null;
		}
	}

	public HashMap<String, ItemInfo> getDynamicHashString(int propertyId) {
		switch (propertyId) {
		case PropertyId.IMAGE_SIZE:
			return getImageSizeMap();
		case PropertyId.VIDEO_SIZE:
			return getVideoSizeMap();
		default:
			return null;
		}
	}

	private HashMap<Integer, ItemInfo> getCaptureDelayMap() {
		HashMap<Integer, ItemInfo> captureDelayMap = new HashMap<Integer, ItemInfo>();
		List<Integer> delyaList = CameraProperties.getInstance().getSupportedPropertyValues(PropertyId.CAPTURE_DELAY);
		String temp;
		for (int ii = 0; ii < delyaList.size(); ii++) {
			if (delyaList.get(ii) == 0) {
				temp = "OFF";
			} else {
				temp = delyaList.get(ii) / 1000 + "S";
			}
			Log.d("1111", "delyaList.get(ii) ==" + delyaList.get(ii));
			captureDelayMap.put(delyaList.get(ii), new ItemInfo(temp, temp, 0));
		}
		return captureDelayMap;
	}

	private HashMap<String, ItemInfo> getImageSizeMap() {
		WriteLogToDevice.writeLog("[Normal] -- SDKReflectToUI: ", "begin initImageSizeMap");
		HashMap<String, ItemInfo> imageSizeMap = new HashMap<String, ItemInfo>();
		List<String> imageSizeList = null;
		imageSizeList = CameraProperties.getInstance().getSupportedImageSizes();
		List<Integer> convertImageSizeList = null;
		try {
			convertImageSizeList = ICatchWificamUtil.convertImageSizes(imageSizeList);
		} catch (IchInvalidArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String temp = "Undefined";
		String temp1 = "Undefined";
		for (int ii = 0; ii < imageSizeList.size(); ii++) {
			if (convertImageSizeList.get(ii) == ICatchImageSize.ICH_IMAGE_SIZE_VGA) {
				temp = "VGA" + "(" + imageSizeList.get(ii) + ")";
				imageSizeMap.put(imageSizeList.get(ii), new ItemInfo(temp, "VGA", 0));
			} else {
				temp = convertImageSizeList.get(ii) + "M" + "(" + imageSizeList.get(ii) + ")";
				temp1 = convertImageSizeList.get(ii) + "M";
				imageSizeMap.put(imageSizeList.get(ii), new ItemInfo(temp, temp1, 0));
			}
			WriteLogToDevice.writeLog("[Normal] -- SDKReflectToUI: ", "imageSize =" + temp);
		}
		WriteLogToDevice.writeLog("[Normal] -- SDKReflectToUI: ", "end initImageSizeMap imageSizeMap =" + imageSizeMap.size());
		return imageSizeMap;
	}

	private HashMap<String, ItemInfo> getVideoSizeMap() {
		WriteLogToDevice.writeLog("[Normal] -- SDKReflectToUI: ", "begin initVideoSizeMap");
		HashMap<String, ItemInfo> videoSizeMap = new HashMap<String, ItemInfo>();
		List<String> videoSizeList = CameraProperties.getInstance().getSupportedVideoSizes();
		List<ICatchVideoSize> convertVideoSizeList = null;
		try {
			convertVideoSizeList = ICatchWificamUtil.convertVideoSizes(videoSizeList);
		} catch (IchInvalidArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// videoSizeArray = new String[convertVideoSizeList.size()];
		for (int ii = 0; ii < convertVideoSizeList.size(); ii++) {
			WriteLogToDevice.writeLog("[Normal] -- SDKReflectToUI: ", "videoSizeList_" + ii + " = " + videoSizeList.get(ii));
			if (convertVideoSizeList.get(ii) == ICatchVideoSize.ICH_VIDEO_SIZE_1080P_WITH_30FPS) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("1920x1080 30fps", "FHD30", 0));
				// cs[1] = "FHD";
			} else if (convertVideoSizeList.get(ii) == ICatchVideoSize.ICH_VIDEO_SIZE_1080P_WITH_60FPS) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("1920x1080 60fps", "FHD60", 0));
				// cs[1] = "FHD";
			} else if (convertVideoSizeList.get(ii) == ICatchVideoSize.ICH_VIDEO_SIZE_1440P_30FPS) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("1920x1440 30fps", "1440P", 0));
				// cs[1] = "FHD";
			} else if (convertVideoSizeList.get(ii) == ICatchVideoSize.ICH_VIDEO_SIZE_720P_120FPS) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("1280x720 120fps", "HD120", 0));
				// cs[1] = "HD";
			} else if (convertVideoSizeList.get(ii) == ICatchVideoSize.ICH_VIDEO_SIZE_720P_WITH_30FPS) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("1280x720 30fps", "HD30", 0));
				// cs[1] = "HD";
			} else if (convertVideoSizeList.get(ii) == ICatchVideoSize.ICH_VIDEO_SIZE_720P_WITH_60FPS) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("1280x720 60fps", "HD60", 0));
				// cs[1] = "HD";
			} else if (videoSizeList.get(ii).equals("1280x720 50")) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("1280x720 50fps", "HD50", 0));
				// cs[1] = "HD";
			} else if (videoSizeList.get(ii).equals("1280x720 25")) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("1280x720 25fps", "HD25", 0));
				// cs[1] = "HD";
			}
			/*
			 * else if (videoSizeList.get(ii).equals("1280x720 12")) {
			 * videoSizeMap.put(videoSizeList.get(ii), new
			 * ItemInfo("1280x720 12fps", "HD12", 0)); }
			 */

			else if (convertVideoSizeList.get(ii) == ICatchVideoSize.ICH_VIDEO_SIZE_960P_60FPS) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("1280x960 60fps", "960P", 0));
			} else if (videoSizeList.get(ii).equals("1280x960 120")) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("1280x960 120fps", "960P", 0));
			} else if (videoSizeList.get(ii).equals("1280x960 30")) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("1280x960 30fps", "960P", 0));
			}

//			else if (convertVideoSizeList.get(ii) == ICatchVideoSize.ICH_VIDEO_SIZE_VGA_120FPS) {
//				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("640x480 120fps", "VGA120", 0));
//				// cs[1] = "VGA";
//			} else if (convertVideoSizeList.get(ii) == ICatchVideoSize.ICH_VIDEO_SIZE_640_360_240FPS) {
//				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("640x480 240fps", "VGA240", 0));
//				// cs[1] = "VGA";
//			}
			
			//start add by b.jiang 20160106
			else if (videoSizeList.get(ii).equals("640x480 240")) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("640x480 240fps", "VGA240", 0));
			} else if (videoSizeList.get(ii).equals("640x480 120")) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("640x480 120fps", "VGA120", 0));
			}
			else if (videoSizeList.get(ii).equals("640x360 240")) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("640x360 240fps", "VGA240", 0));
			} else if (videoSizeList.get(ii).equals("640x360 120")) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("640x360 120fps", "VGA120", 0));
			}
			
			//end add by b.jiang 20160106
			

			else if (videoSizeList.get(ii).equals("1920x1080 24")) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("1920x1080 24fps", "FHD24", 0));
			} else if (videoSizeList.get(ii).equals("1920x1080 50")) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("1920x1080 50fps", "FHD50", 0));
			} else if (videoSizeList.get(ii).equals("1920x1080 25")) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("1920x1080 25fps", "FHD25", 0));
			}
			//Start add by b.jiang 2016-01-15
			else if (videoSizeList.get(ii).equals("3840x2160 60")) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("3840x2160 60fps", "4K60", 0));
				
			} else if (videoSizeList.get(ii).equals("3840x2160 50")) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("3840x2160 50fps", "4K50", 0));
				
			} else if (videoSizeList.get(ii).equals("3840x2160 25")) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("3840x2160 25fps", "4K25", 0));
				
			}else if (videoSizeList.get(ii).equals("3840x2160 24")) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("3840x2160 24fps", "4K24", 0));
				
			}
			//end add by b.jiang 2016-01-15

			else if (videoSizeList.get(ii).equals("3840x2160 30")) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("3840x2160 30fps", "4K30", 0));
				
			} else if (videoSizeList.get(ii).equals("3840x2160 15")) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("3840x2160 15fps", "4K15", 0));
				
			} else if (videoSizeList.get(ii).equals("3840x2160 10")) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("3840x2160 10fps", "4K10", 0));
				
			}
			
			//ICOM-2442 Start add by b.jiang 2015-12-14
			else if (videoSizeList.get(ii).equals("2704x1524 30")) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("2704x1524 30fps", "2.7K30", 0));
			} else if (videoSizeList.get(ii).equals("2704x1524 15")) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("2704x1524 15fps", "2.7K15", 0));
			}
			//ICOM-2442 End add by b.jiang 2015-12-14
			
			//Start add by b.jiang 2016-01-15
			else if (videoSizeList.get(ii).equals("2704x1524 60")) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("2704x1524 60fps", "2.7K60", 0));
			} else if (videoSizeList.get(ii).equals("2704x1524 50")) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("2704x1524 50fps", "2.7K50", 0));
			}else if (videoSizeList.get(ii).equals("2704x1524 25")) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("2704x1524 25fps", "2.7K25", 0));
			} else if (videoSizeList.get(ii).equals("2704x1524 24")) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("2704x1524 24fps", "2.7K24", 0));
			}
			//end add by b.jiang 2016-01-15
			
			// ICOM-1902 Start add by zhangyanhu C01012 2015-8-17
			else if (videoSizeList.get(ii).equals("848x480 240")) {
				videoSizeMap.put(videoSizeList.get(ii), new ItemInfo("848x480 240fps", "WVGA", 0));
			}
			// ICOM-1902 End add by zhangyanhu C01012 2015-8-17
		}
		WriteLogToDevice.writeLog("[Normal] -- SDKReflectToUI: ", "end initVideoSizeMap videoSizeList =" + videoSizeList.size());
		return videoSizeMap;
	}
}
