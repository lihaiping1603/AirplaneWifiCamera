package com.vxfly.helioscamera.Tool;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.vxfly.helioscamera.global.App.GlobalInfo;
import com.vxfly.helioscamera.R;

public class CustomToast {

	public static void showDialog(String message, Context context) {
		LayoutInflater inflater = LayoutInflater.from(GlobalInfo.getInstance().getAppContext());

		View view = inflater.inflate(R.layout.toast_show_dialog, null);
		TextView textView = (TextView) view.findViewById(R.id.mytoast);

		// Toast toast =Toast.makeText(GlobalInfo.getInstance().getCurrentApp(),
		// "!!!!!!!!!!", Toast.LENGTH_LONG);
		Toast toast = new Toast(context);
		textView.setText(message);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.setView(view);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}
}
