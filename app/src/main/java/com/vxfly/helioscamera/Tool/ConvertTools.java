package com.vxfly.helioscamera.Tool;

public class ConvertTools {
	public static String secondsToHours(int remainTime) {
		String time = "";
		Integer h = remainTime / 3600;
		Integer m = (remainTime % 3600) / 60;
		Integer s = remainTime % 60;
		if (h < 10) {
			time = "0" + h.toString();
		} else {
			time = h.toString();
		}
		time = time + ":";
		if (m < 10) {
			time = time + "0" + m.toString();
		} else {
			time = time + m.toString();
		}
		time = time + ":";
		if (s < 10) {
			time = time + "0" + s.toString();
		} else {
			time = time + s.toString();
		}
		return time;
	}

}
