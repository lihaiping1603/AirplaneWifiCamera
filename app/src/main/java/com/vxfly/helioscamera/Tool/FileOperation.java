package com.vxfly.helioscamera.Tool;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.content.Context;

import com.vxfly.helioscamera.global.App.GlobalInfo;
import com.vxfly.helioscamera.log.WriteLogToDevice;

public class FileOperation {
	public static void clearFile() {
		FileOutputStream outStream = null;
		try {
			outStream = GlobalInfo.getInstance().getCurrentApp().openFileOutput("tutkUid.txt", Context.MODE_APPEND);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			WriteLogToDevice.writeLog("[Error] -- Appstart: ", "open outStream  FileNotFoundException");
			e.printStackTrace();
		}
		try {
			outStream.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void saveString(String myString) {
		WriteLogToDevice.writeLog("[Normal] -- Appstart: ", "saveString myString =" + myString);
		String readfile[] = readString();
		if (readfile != null) {
			for (String temp : readfile) {
				if (temp.equals(myString) == true) {
					return;
				}
			}
		}

		FileOutputStream outStream = null;
		String tempString = myString + ";";
		try {
			outStream = GlobalInfo.getInstance().getCurrentApp().openFileOutput("tutkUid.txt", Context.MODE_APPEND);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			WriteLogToDevice.writeLog("[Error] -- Appstart: ", "open outStream  FileNotFoundException");
			e.printStackTrace();
		}

		try {
			outStream.write(tempString.getBytes());
			outStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			WriteLogToDevice.writeLog("[Error] -- Appstart: ", "open outStream IOException");
			e.printStackTrace();
		}
		// outStream.
	}

	public static String[] readString() {
		WriteLogToDevice.writeLog("[Normal] -- Appstart: ", "start readString");
		FileInputStream inStream = null;
		try {
			inStream = GlobalInfo.getInstance().getCurrentApp().openFileInput("tutkUid.txt");
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block`
			WriteLogToDevice.writeLog("[Error] -- Appstart: ", "FileNotFoundException");
			e1.printStackTrace();
		}
		if (inStream == null) {

			return null;
		}
		try {
			if (inStream.available() <= 0) {
				return null;
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		byte[] b = null;
		try {
			b = new byte[inStream.available()];
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}// 新建一个字节数组
		try {
			inStream.read(b);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}// 将文件中的内容读取到字节数组中
		try {
			inStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (b == null) {
			return null;
		}
		String str2 = new String(b);// 再将字节数组中的内容转化成字符串形式输出
		if (str2 == null) {
			return null;
		}
		String newStr = str2.replaceAll("\"", "");
		WriteLogToDevice.writeLog("[Normal] -- Appstart: ", "------newStr =" + newStr);
		String[] temp = newStr.split(";");
		if (temp.length == 0) {
			return null;
		}
		return temp;
	}
}
