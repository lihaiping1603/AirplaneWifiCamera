package com.vxfly.helioscamera.Tool;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import com.vxfly.helioscamera.global.App.AppInfo;
import com.vxfly.helioscamera.global.App.GlobalInfo;
import com.vxfly.helioscamera.log.WriteLogToDevice;
import com.icatch.wificam.customer.ICatchWificamConfig;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

public class ConfigureInfo {
	public static void readCfgInfo(Context context) {
		// TODO Auto-generated method stub
		Log.d("1111", "readCfgInfo..........");
		String path = null;
		// path = Environment.getExternalStorageDirectory().toString() +
		// GlobalInfo.PROPERTY_CFG;

		// String path1 = Environment.getExternalStorageDirectory().toString() +
		// "/SportCamResoure/";
		path = context.getExternalCacheDir() + GlobalInfo.PROPERTY_CFG;
		String path1 = context.getExternalCacheDir() + "/SportCamResoure/";
		Log.d("1111", "getExternalCacheDir..........=" + path1);
		String appVersion = AppInfo.getAppVer();

		String info = "\n#false true\n" + "SupportAutoReconnection=false\n" + "SaveStreamVideo=false\n" + "SaveStreamAudio=false\n" + "broadcast=false\n"
				+ "SupportSetting=false\n" + "SaveSDKLog=true\n" + "SaveAPPLog=true\n" + "disconnectRetry=3\n" + "enableSoftwareDecoder=false\n";
		info = "AppVersion=" + appVersion + info;

		File directory = null;

		if (path1 != null) {
			directory = new File(path1);
			if (!directory.exists()) {
				directory.mkdirs();
			}
		}
		String fileName = "netconfig.properties";
		File file = new File(directory, fileName);
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			FileOutputStream out = null;
			try {
				out = new FileOutputStream(path);
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				out.write(info.getBytes());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				out.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		// 如果配置文件存在，判断是否需要替换旧版的配置文件;
		else {
			CfgProperty cfgInfo = new CfgProperty(path);
			String cfgVersion = null;
			try {
				cfgVersion = cfgInfo.getProperty("AppVersion");
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (cfgVersion != null) {
				Log.d("1111", "cfgVersion..........=" + cfgVersion);
				if (!cfgVersion.equals(appVersion)) {
					writeCfgInfo(path, info);
				}
				Log.d("1111", "cfgVersion=" + cfgVersion + " appVersion=" + appVersion);
			} else {
				writeCfgInfo(path, info);
				Log.d("1111", "cfgVersion=" + cfgVersion + " appVersion=" + appVersion);
			}
		}

		CfgProperty cfgInfo = new CfgProperty(path);
		String AutoReconnection = null;
		String saveStreamVideo = null;
		String saveStreamAudio = null;
		String tutkMode = null;
		String broadcast = null;
		String supportSetting = null;
		String saveSDKLog = null;
		String saveAPPLog = null;
		String disconnectRetry = null;
		String enableSoftwareDecoder = null;
		try {
			AutoReconnection = cfgInfo.getProperty("SupportAutoReconnection");
			saveStreamVideo = cfgInfo.getProperty("SaveStreamVideo");
			saveStreamAudio = cfgInfo.getProperty("SaveStreamAudio");
			broadcast = cfgInfo.getProperty("broadcast");
			supportSetting = cfgInfo.getProperty("SupportSetting");
			saveSDKLog = cfgInfo.getProperty("SaveSDKLog");
			saveAPPLog = cfgInfo.getProperty("SaveAPPLog");
			enableSoftwareDecoder = cfgInfo.getProperty("enableSoftwareDecoder");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			disconnectRetry = cfgInfo.getProperty("disconnectRetry");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String path2 = null;
		path2 = Environment.getExternalStorageDirectory().toString() + "/SportCam/Raw";
		Log.d("AppStart", "path2: " + path2);
		if (path2 != null) {
			File file1 = new File(path2);
			if (!file1.exists()) {
				file1.mkdirs();
			}
		}
		if (AutoReconnection != null) {
			if (AutoReconnection.equals("true")) {
				GlobalInfo.isSupportAutoReconnection = true;
			} else {
				GlobalInfo.isSupportAutoReconnection = false;
			}
			WriteLogToDevice.writeLog("ConfigureInfo", " end isSupportAutoReconnection = " + GlobalInfo.isSupportAutoReconnection);
		}

		if (saveStreamVideo != null) {
			if (saveStreamVideo.equals("true")) {
				ICatchWificamConfig.getInstance().enableDumpMediaStream(true, path2); // save
																						// video
			}
		}
		if (saveStreamAudio != null) {
			if (saveStreamAudio.equals("true")) {
				Log.d("1111", "enableDumpMediaStream..........=" + false);
				ICatchWificamConfig.getInstance().enableDumpMediaStream(false, path2);// save
																						// audio
			}
		}
		if (saveStreamAudio != null) {
			if (saveStreamAudio.equals("true")) {
				Log.d("1111", "enableDumpMediaStream..........=" + false);
				ICatchWificamConfig.getInstance().enableDumpMediaStream(false, path2);// save
																						// audio
			}
		}
		if (broadcast != null) {
			Log.d("1111", "broadcast..........=" + broadcast);
			if (broadcast.equals("true")) {
				Log.d("1111", "broadcast..........=" + broadcast);
				GlobalInfo.isSupportBroadcast = true;
			}
			Log.d("1111", "GlobalInfo.isSupportBroadcast..........=" + GlobalInfo.isSupportBroadcast);
		}
		if (supportSetting != null) {
			WriteLogToDevice.writeLog("ConfigureInfo", "supportSetting..........=" + supportSetting);
			if (supportSetting.equals("true")) {
				WriteLogToDevice.writeLog("ConfigureInfo", "supportSetting..........=" + supportSetting);
				GlobalInfo.isSupportSetting = true;
			}
			WriteLogToDevice.writeLog("ConfigureInfo", "GlobalInfo.isSupportSetting..........=" + GlobalInfo.isSupportSetting);
		}
		if (saveSDKLog != null) {
			WriteLogToDevice.writeLog("ConfigureInfo", "saveSDKLog=" + saveSDKLog);
			if (saveSDKLog.equals("true")) {
				WriteLogToDevice.writeLog("ConfigureInfo", "saveSDKLog=" + saveSDKLog);
				GlobalInfo.saveSDKLog = true;
			}
			WriteLogToDevice.writeLog("ConfigureInfo", "GlobalInfo.saveSDKLog=" + GlobalInfo.saveSDKLog);
		}
		if (saveAPPLog != null) {
			WriteLogToDevice.writeLog("ConfigureInfo", "saveAPPLog=" + saveAPPLog);
			if (saveAPPLog.equals("true")) {
				WriteLogToDevice.writeLog("ConfigureInfo", "saveAPPLog=" + saveAPPLog);
				GlobalInfo.saveAPPLog = true;
			}else{
				GlobalInfo.saveAPPLog = false;
			}
		}

		Log.d("1111", "disconnectRetry=" + disconnectRetry);
		if (disconnectRetry != null) {
			int retryCount = Integer.parseInt(disconnectRetry);
			ICatchWificamConfig.getInstance().setConnectionCheckParam(retryCount, 15);
			Log.d("1111", "retryCount=" + retryCount);
		}
		Log.d("1111", "isSupportAutoReconnection=" + GlobalInfo.isSupportAutoReconnection);
		
		Log.d("1111", "enableSoftwareDecoder=" + enableSoftwareDecoder);
		
		if (enableSoftwareDecoder != null) {
			if(enableSoftwareDecoder.equals("true")){
				ICatchWificamConfig.getInstance().enableSoftwareDecoder(true);
				Log.d("1111", "open SoftwareDecoder");
				GlobalInfo.enableSoftwareDecoder = true;
			}else{
				ICatchWificamConfig.getInstance().enableSoftwareDecoder(false);
				GlobalInfo.enableSoftwareDecoder = false;
			}
			
			
		}
		
	}

	private static void writeCfgInfo(String path, String cfgInfo) {
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(path);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			out.write(cfgInfo.getBytes(), 0, cfgInfo.getBytes().length);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			out.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

}
