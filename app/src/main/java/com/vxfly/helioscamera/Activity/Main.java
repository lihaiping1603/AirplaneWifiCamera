package com.vxfly.helioscamera.Activity;

import java.io.File;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.vxfly.helioscamera.ExtendComponent.DirectionImageView;
import com.vxfly.helioscamera.ExtendComponent.PreviewH264;
import com.vxfly.helioscamera.ExtendComponent.PreviewMjpg;
import com.vxfly.helioscamera.ExtendComponent.ZoomBar;
import com.vxfly.helioscamera.SDKAPI.CameraAction;
import com.vxfly.helioscamera.SDKAPI.CameraProperties;
import com.vxfly.helioscamera.SDKAPI.CameraState;
import com.vxfly.helioscamera.SDKAPI.FileOperation;
import com.vxfly.helioscamera.SDKAPI.PreviewStream;
import com.vxfly.helioscamera.Tool.BitmapUtil;
import com.vxfly.helioscamera.Tool.ConvertTools;
import com.vxfly.helioscamera.Tool.FileTools;
import com.vxfly.helioscamera.Tool.ResolutionConvert;
import com.vxfly.helioscamera.baseItems.SlowMotion;
import com.vxfly.helioscamera.baseItems.TimeLapseInterval;
import com.vxfly.helioscamera.baseItems.TimeLapseMode;
import com.vxfly.helioscamera.baseItems.Tristate;
import com.vxfly.helioscamera.baseItems.Upside;
import com.vxfly.helioscamera.camera.MyCamera;
import com.vxfly.helioscamera.function.ActivityCollector;
import com.vxfly.helioscamera.function.CameraCaptureThread;
import com.vxfly.helioscamera.function.PreviewMove;
import com.vxfly.helioscamera.function.SettingView;
import com.vxfly.helioscamera.function.SystemCheckTools;
import com.vxfly.helioscamera.function.WifiCheck;
import com.vxfly.helioscamera.function.ZoomThread;
import com.vxfly.helioscamera.global.App.AppProperties;
import com.vxfly.helioscamera.global.App.ExitApp;
import com.vxfly.helioscamera.global.App.GlobalInfo;
import com.vxfly.helioscamera.global.App.PropertyId;
import com.vxfly.helioscamera.global.sdk.SDKEvent;
import com.vxfly.helioscamera.log.WriteLogToDevice;
import com.vxfly.helioscamera.R;
import com.icatch.wificam.customer.ICatchWificamConfig;
import com.icatch.wificam.customer.ICatchWificamPreview;
import com.icatch.wificam.customer.type.ICatchCameraProperty;
import com.icatch.wificam.customer.type.ICatchCodec;
import com.icatch.wificam.customer.type.ICatchCustomerStreamParam;
import com.icatch.wificam.customer.type.ICatchDateStamp;
import com.icatch.wificam.customer.type.ICatchEventID;
import com.icatch.wificam.customer.type.ICatchFile;
import com.icatch.wificam.customer.type.ICatchMJPGStreamParam;
import com.icatch.wificam.customer.type.ICatchMode;
import com.icatch.wificam.customer.type.ICatchPreviewMode;

public class Main extends FragmentActivity {
	private PreviewMjpg previewMjpg;
	private PreviewH264 previewH264;

	private WifiSSReceiver wifiSSReceiver;
	private int curMode;
	private GlobalInfo globalInfo;
	private WifiCheck wifiTool;

	private ExecutorService executor;
	private Future<Object> future;
	private MainHandler mainHandler;
	private Timer videoCaptureTimer;
	private Handler mainTimerHandler;
	// private ProgressDialog captureDialog;
	private ProgressDialog progressDialog;

	private static final int APP_STATE_STILL_PREVIEW = 0x0001;
	private static final int APP_STATE_STILL_CAPTURE = 0x0002;
	private static final int APP_STATE_VIDEO_PREVIEW = 0x0003;
	private static final int APP_STATE_VIDEO_CAPTURE = 0x0004;
	private static final int APP_STATE_TIMELAPSE_VIDEO_CAPTURE = 0x0005;
	private static final int APP_STATE_TIMELAPSE_STILL_CAPTURE = 0x0006;
	private static final int APP_STATE_TIMELAPSE_PREVIEW_VIDEO = 0x0007;
	private static final int APP_STATE_TIMELAPSE_PREVIEW_STILL = 0x0008;
	// Added by zhangyanhu 2013.11.27
	private static final int MESSAGE_QUIT_APP = 0x0001;
	private static final int MESSAGE_ZOOM_BAR_DISAPPEAR = 0x0003;
	private static final int MESSAGE_UPDATE_RECORDING_TIME = 0X0004;
	// private static Boolean QUITFLAG = false;
	// End add by zhangyanhu 2013.11.27
	private static final int MESSAGE_VIDEO_CAPTURE_ON = 0x0002;

	private SDKEvent sdkEvent;
	protected boolean intentLock = false;

	private CameraProperties cameraProperties;
	private CameraAction cameraAction;
	private CameraState cameraState;
	private PreviewStream previewStream;
	private ICatchWificamPreview cameraPreviewStreamClint;

	private Boolean captureDelayMode = false;
	private FileOperation fileOperation;
	private int cacheTime;
	private int currentCodec = ICatchCodec.ICH_CODEC_RGBA_8888;
	private Boolean supportStreaming = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		WriteLogToDevice.writeLog("[Normal] -- Main: ", "start onCreate");
		WriteLogToDevice.writeLog("[Normal] -- Main: ", "SD card exist = " + GlobalInfo.isSdCardExist);
		// TODO: make sure session is created and in prepared state
		// never sleep when run this activity
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		// do not display menu bar
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.main);
		mainHandler = new MainHandler();

		videoCaptureTimer = new Timer(false);

		previewMjpg = (PreviewMjpg) findViewById(R.id.preview_mjpg);
		previewH264 = (PreviewH264) findViewById(R.id.preview_h264);

		initClint();
		initStatus();

		//add by lihaiping1603@aliyun.com on 2016-12-26
		ActivityCollector.addActivity(this);
	}

	private void initStatus() {
		// set default UI to display
		WriteLogToDevice.writeLog("[Normal] -- Main: ", "initStatus");

		ExitApp.getInstance().addActivity(this);
		executor = Executors.newSingleThreadExecutor();

		mainTimerHandler = new MyTimerHandler();
		globalInfo = GlobalInfo.getInstance();
		globalInfo.setCurrentApp(Main.this);

		if (cameraProperties.cameraModeSupport(ICatchMode.ICH_MODE_VIDEO) == false) {
			curMode = APP_STATE_STILL_PREVIEW;
		} else {
			curMode = APP_STATE_VIDEO_PREVIEW;
		}


		sdkEvent = new SDKEvent(mainHandler);
		wifiTool = new WifiCheck();
		wifiTool.openConnectCheck();
		// for test
		// load Menu data and set listener
		wifiTool.checkWifiPolicy();

		if (cameraProperties.hasFuction(0xD7F0)) {
			if (cameraProperties.setCaptureDelayMode(1))
				captureDelayMode = true;
		}
		WriteLogToDevice.writeLog("[Normal] -- Main: ", "captureDelayMode=" +captureDelayMode);
		cacheTime = cameraProperties.getPreviewCacheTime();
		Log.e("1111", "-----------cacheTime =" + cacheTime);
		if (cacheTime < 200) {
			cacheTime = 200;
		}
		ICatchWificamConfig.getInstance().setPreviewCacheParam(cacheTime, 200);
		WriteLogToDevice.writeLog("[Normal] -- Main: ", "end initStatus");
	}

	private class MainHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
		}
	};

	private class MyTimerHandler extends Handler {
		public void handleMessage(Message msg) {
		};
	};

	private void sendOkMsg(int what) {
		mainHandler.obtainMessage(what).sendToTarget();
	}

	private void sendTimerMsg(int what, int arg1) {
		mainTimerHandler.obtainMessage(what, arg1, 0).sendToTarget();
	}

	@Override
	protected void onDestroy() {
		WriteLogToDevice.writeLog("[Normal] -- Main: ", "main:onDestroy");
		super.onDestroy();
		stopMediaStream();
		//add by lihaiping1603@aliyun.com on 2016-12-26
		ActivityCollector.removeActivity(this);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		// TODO Auto-generated method stub
		super.onConfigurationChanged(newConfig);
	}

	@Override
	protected void onStart() {
		super.onStart();
		WriteLogToDevice.writeLog("[Normal] -- Main: ", "main .............onStart");
		Integer temp = cameraProperties.getRemainImageNum();
		mainTimerHandler = new MyTimerHandler();
		Tristate ret = Tristate.FALSE;

		if (curMode == APP_STATE_VIDEO_PREVIEW) {
			WriteLogToDevice.writeLog("[Normal] -- Main: ", "curMode == APP_STATE_VIDEO_PREVIEW");
			ret = changeCameraMode(ICatchPreviewMode.ICH_VIDEO_PREVIEW_MODE);
			if (ret == Tristate.FALSE) {
				return;
			}
			// normal state, app show preview
		} else if (curMode == APP_STATE_TIMELAPSE_PREVIEW_VIDEO) {
			WriteLogToDevice.writeLog("[Normal] -- Main: ", "curMode == APP_STATE_TIMELAPSE_PREVIEW_VIDEO");
			AppProperties.getInstanse().setTimeLapseMode(TimeLapseMode.TIME_LAPSE_MODE_VIDEO);
			ret = changeCameraMode(ICatchPreviewMode.ICH_TIMELAPSE_VIDEO_PREVIEW_MODE);
			if (ret == Tristate.FALSE) {
				return;
			}
			// normal state, app show preview
		} else if (curMode == APP_STATE_TIMELAPSE_PREVIEW_STILL) {
			WriteLogToDevice.writeLog("[Normal] -- Main: ", "curMode == APP_STATE_TIMELAPSE_PREVIEW_STILL");
			AppProperties.getInstanse().setTimeLapseMode(TimeLapseMode.TIME_LAPSE_MODE_STILL);
			ret = changeCameraMode(ICatchPreviewMode.ICH_TIMELAPSE_STILL_PREVIEW_MODE);
			if (ret == Tristate.FALSE) {
				return;
			}
			// normal state, app show preview
		} else if (curMode == APP_STATE_STILL_PREVIEW) {
			WriteLogToDevice.writeLog("[Normal] -- Main: ", "curMode == ICH_STILL_PREVIEW_MODE");
			ret = changeCameraMode(ICatchPreviewMode.ICH_STILL_PREVIEW_MODE);
			if (ret == Tristate.FALSE) {
				return;
			}
			// changeCameraMode(ICatchPreviewMode.ICH_STILL_PREVIEW_MODE);
		}

		// JIRA ICOM-2098 Begin Delete by b.jiang 2015-10-19
		// destroySurface();
		// JIRA ICOM-2098 End Delete by b.jiang 2015-10-19
		if (ret == Tristate.NORMAL) {
			startPreview();
		}

		IntentFilter wifiSSFilter = new IntentFilter(WifiManager.RSSI_CHANGED_ACTION);
		wifiSSReceiver = new WifiSSReceiver();
		this.registerReceiver(wifiSSReceiver, wifiSSFilter);

		sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_SDCARD_FULL);
		sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_BATTERY_LEVEL_CHANGED);
		sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_VIDEO_OFF);
		sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_VIDEO_ON);
		sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_CAPTURE_START);
		sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_CAPTURE_COMPLETE);
		sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_FILE_ADDED);
		sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_CONNECTION_DISCONNECTED);
		sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_TIMELAPSE_STOP);
		sdkEvent.addCustomizeEvent(0x5001);// vidoe recording event
		sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_FILE_DOWNLOAD);

		// if (GlobalInfo.isSdCard == false) {
		// sdCardIsNotReadyAlert(Main.this.getString(R.string.dialog_card_removed));
		// }
	}

	@Override
	protected void onStop() {
		super.onStop();
		WriteLogToDevice.writeLog("[Normal] -- Main: ", "onStop");
		if (wifiSSReceiver != null) {
			this.unregisterReceiver(wifiSSReceiver);
		}
		if (videoCaptureTimer != null) {
			videoCaptureTimer.cancel();
		}
		if (SystemCheckTools.isApplicationSentToBackground(Main.this) == true) {
			ExitApp.getInstance().exit();
		}
	}

	@Override
	protected void onPause() {
		WriteLogToDevice.writeLog("[Normal] -- Main: ", "onPause");
		super.onPause();
		// stopPreview();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		// pbBtn.setClickable(true);
		WriteLogToDevice.writeLog("[Normal] -- Main: ", "onRestart");
	}

	@Override
	protected void onResume() {
		super.onResume();
		globalInfo.setCurrentApp(Main.this);
		intentLock = false;
		WriteLogToDevice.writeLog("[Normal] -- Main: ", "onResume");
	}

	private class WifiSSReceiver extends BroadcastReceiver {
		private WifiManager wifi;

		public WifiSSReceiver() {
			super();

			wifi = (WifiManager) getSystemService(WIFI_SERVICE);
			changeWifiStatusIcon();
		}

		@Override
		public void onReceive(Context arg0, Intent arg1) {
			changeWifiStatusIcon();
		}

		private void changeWifiStatusIcon() {
			WifiInfo info = wifi.getConnectionInfo();
			if (info.getBSSID() != null) {
				int strength = WifiManager.calculateSignalLevel(info.getRssi(), 8);

				WriteLogToDevice.writeLog("Main", "change Wifi Status：" + strength);
				switch (strength) {
				case 0:
				case 1:
//					wifiStatus.setImageResource(R.drawable.wifi_1);
					break;
				case 2:
				case 3:
//					wifiStatus.setImageResource(R.drawable.wifi_2);
					break;
				case 4:
				case 5:
//					wifiStatus.setImageResource(R.drawable.wifi_3);
					break;
				case 6:
				case 7:
//					wifiStatus.setImageResource(R.drawable.wifi_4);
					break;
				default:
					break;
				}
			}
		}
	}

	// Added by zhangyanhu 2013.11.27
	private Timer quitTimer = new Timer(true);
	private TimerTask quitTask = new TimerTask() {
		@Override
		public void run() {
			sendOkMsg(MESSAGE_QUIT_APP);
		}
	};

	private Tristate changeCameraModeNormal(ICatchPreviewMode previewMode) {
		WriteLogToDevice.writeLog("[Normal] -- Main: ", "changeCameraModeNormal previewMode =" + previewMode);
		ICatchMJPGStreamParam param = new ICatchMJPGStreamParam();
		Tristate ret = Tristate.FALSE;

		WriteLogToDevice.writeLog("[Normal] -- Main: ", "begin start media stream");
		ret = previewStream.startMediaStream(cameraPreviewStreamClint, param, previewMode);
		WriteLogToDevice.writeLog("[Normal] -- Main: ", "end  changeCameraModeNormal ret = " + ret);
		// JIRA ICOM-1839 Start add by b.jiang 2015-08-13
		if (ret == Tristate.FALSE) {
			Toast.makeText(Main.this, R.string.text_stream_launch_failed, Toast.LENGTH_LONG).show();
		}
		// JIRA ICOM-1839 End add by b.jiang 2015-08-13
		// JIRA ICOM-1787 Start add by b.jiang 2015-08-13
		else if (ret == Tristate.ABNORMAL) {
			previewMjpg.setVisibility(View.GONE);
			previewH264.setVisibility(View.GONE);
			supportStreaming = false;
		} else {
			supportStreaming = true;
		}
		// JIRA ICOM-1787 End add by b.jiang 2015-08-13
		return ret;

	}

	private Tristate changeCameraMode(ICatchPreviewMode previewMode) {
		WriteLogToDevice.writeLog("[Normal] -- Main: ", "start changeCameraMode previewMode =" + previewMode);
		Tristate ret = Tristate.FALSE;
		String cmd = cameraProperties.getCurrentStreamInfo();
		// String cmd = "H264?W=1280&H=720&BR=2000000&FPS=15&";
		WriteLogToDevice.writeLog("[Normal] -- Main: ", "start changeCameraMode cmd =" + cmd);
		if (cmd == null) {
			return changeCameraModeNormal(previewMode);
		}

		WriteLogToDevice.writeLog("[Normal] -- Main: ", "Resolution cmd = " + cmd);
		if (cmd.contains("MJPG") == true) {
			ICatchMJPGStreamParam param = new ICatchMJPGStreamParam(getResolutionWidth(cmd), getResolutionHeigth(cmd), getResolutionBitrate(cmd), 50);

			WriteLogToDevice.writeLog("[Normal] -- Main: ", "begin startMediaStream");

			ret = previewStream.startMediaStream(cameraPreviewStreamClint, param, previewMode);

			WriteLogToDevice.writeLog("[Normal] -- Main: ", "end  startMediaStream ret = " + ret);
			// JIRA ICOM-1839 Start add by b.jiang 2015-08-13
			if (ret == Tristate.FALSE) {
				Toast.makeText(Main.this, R.string.text_stream_launch_failed, Toast.LENGTH_LONG).show();
			}
			// JIRA ICOM-1839 End add by b.jiang 2015-08-13
			// JIRA ICOM-1787 Start add by b.jiang 2015-08-13
			else if (ret == Tristate.ABNORMAL) {
				supportStreaming = false;
				previewMjpg.setVisibility(View.GONE);
				previewH264.setVisibility(View.GONE);
			} else {
				supportStreaming = true;
			}
			// JIRA ICOM-1787 End add by b.jiang 2015-08-13
			return ret;
		} else {
			ICatchCustomerStreamParam param = null;
			if (GlobalInfo.enableSoftwareDecoder) {
				cmd = ResolutionConvert.convert(cmd);
			}
			param = new ICatchCustomerStreamParam(554, cmd);
			WriteLogToDevice.writeLog("[Normal] -- Main: ", "begin startMediaStream cmd=" + cmd);
			ret = previewStream.startMediaStream(cameraPreviewStreamClint, param, previewMode);
			WriteLogToDevice.writeLog("[Normal] -- Main: ", "end  startMediaStream ret = " + ret);
			// JIRA ICOM-1839 Start add by b.jiang 2015-08-13
			if (ret == Tristate.FALSE) {
				Toast.makeText(Main.this, R.string.stream_set_error, Toast.LENGTH_LONG).show();
			}
			// JIRA ICOM-1839 Start add by b.jiang 2015-08-13
			// JIRA ICOM-1787 Start add by b.jiang 2015-08-13
			else if (ret == Tristate.ABNORMAL) {
				supportStreaming = false;
				previewMjpg.setVisibility(View.GONE);
				previewH264.setVisibility(View.GONE);
			} else {
				supportStreaming = true;
			}

			// JIRA ICOM-1787 End add by b.jiang 2015-08-13
			return ret;
		}
	}

	/**
	 * Added by zhangyanhu C01012,2014-1-23
	 */
	private boolean stopMediaStream() {
		boolean b = false;
		int ii = 0;
		WriteLogToDevice.writeLog("[Normal] -- Main: ", "begin stop media stream");
		b = previewStream.stopMediaStream(cameraPreviewStreamClint);
		WriteLogToDevice.writeLog("[Normal] -- Main: ", "end stop media stream =" + b);
		WriteLogToDevice.writeLog("[Normal] -- Main: ", "end  stopMediaStream ret = " + b);
		return b;
	}

	private void startPreview() {
		WriteLogToDevice.writeLog("[Normal] -- Main: ", "begin startPreview");

		WriteLogToDevice.writeLog("[Normal] -- Main: ", "previewStream.getCodec() = " + previewStream.getCodec(cameraPreviewStreamClint));
		if (previewStream.getCodec(cameraPreviewStreamClint) == ICatchCodec.ICH_CODEC_RGBA_8888) {
			previewMjpg.setVisibility(View.VISIBLE);
			previewMjpg.start(globalInfo.getCurrentCamera());

			if (previewH264 != null) {
				previewH264.setVisibility(View.GONE);
			}
		} else if (previewStream.getCodec(cameraPreviewStreamClint) == ICatchCodec.ICH_CODEC_H264) {
			previewH264.setVisibility(View.VISIBLE);
			previewH264.start(globalInfo.getCurrentCamera());

			if (previewMjpg != null) {
				previewMjpg.setVisibility(View.GONE);
			}
		}
		currentCodec = previewStream.getCodec(cameraPreviewStreamClint);
		WriteLogToDevice.writeLog("[Normal] -- Main: ", "end startPreview");

	}

	private boolean stopPreview() {
		WriteLogToDevice.writeLog("[Normal] -- Main: ", "begin stopPreview");
		boolean retValue = false;
		WriteLogToDevice.writeLog("[Normal] -- Main: ", "currentCodec = " + currentCodec);
		if (currentCodec == ICatchCodec.ICH_CODEC_RGBA_8888) {
			retValue = previewMjpg.stop();
		} else if (currentCodec == ICatchCodec.ICH_CODEC_H264) {
			retValue = previewH264.stop();
		}
		WriteLogToDevice.writeLog("[Normal] -- Main: ", "end stopPreview ret=" + retValue);
		return retValue;

	}

	private void initClint() {
		cameraProperties = CameraProperties.getInstance();
		cameraAction = CameraAction.getInstance();
		cameraState = CameraState.getInstance();
		GlobalInfo.getInstance().initClint();
		previewStream = PreviewStream.getInstance();
		fileOperation = FileOperation.getInstance();
		cameraPreviewStreamClint = GlobalInfo.currentpreviewStreamClint;
	}

	private int getResolutionWidth(String resolution) {
		// MJPG?W=720&H=400&BR=4000000&
		String temp = resolution;
		temp = temp.replace("MJPG?W=", "");
		temp = temp.replace("&H=", " ");
		temp = temp.replace("&BR=", " ");
		temp = temp.replace("&", " ");
		String[] tempArray = temp.split(" ");
		return Integer.parseInt(tempArray[0]);
	}

	private int getResolutionHeigth(String resolution) {
		String temp = resolution;
		temp = temp.replace("MJPG?W=", "");
		temp = temp.replace("&H=", " ");
		temp = temp.replace("&BR=", " ");
		temp = temp.replace("&", " ");
		String[] tempArray = temp.split(" ");
		return Integer.parseInt(tempArray[1]);
	}

	private int getResolutionBitrate(String resolution) {
		String temp = resolution;
		temp = temp.replace("MJPG?W=", "");
		temp = temp.replace("&H=", " ");
		temp = temp.replace("&BR=", " ");
		temp = temp.replace("&", " ");
		String[] tempArray = temp.split(" ");
		int bitrate = Integer.parseInt(tempArray[2]);
		WriteLogToDevice.writeLog("[Normal] -- Main: ", " getResolutionBitrate = " + bitrate);
		return bitrate;
	}

	////add by lihaiping1603@aliyun.com on 2016-12-26
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event){
		switch (keyCode) {
			case KeyEvent.KEYCODE_1://搜索
				//启动网络选择界面
				Intent intent = new Intent();
				intent.setClass(Main.this, LHPWifiConnectActivity.class);
				startActivity(intent);
				break;
			case KeyEvent.KEYCODE_2:
				Log.d("Main", "home");
				ActivityCollector.finishAll();
				break;
			default:
				return super.onKeyDown(keyCode, event);
		}
		return true;
	}
}
