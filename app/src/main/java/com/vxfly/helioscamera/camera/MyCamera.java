package com.vxfly.helioscamera.camera;

import java.util.List;

import android.util.Log;

import com.vxfly.helioscamera.SDKAPI.CameraAction;
import com.vxfly.helioscamera.SDKAPI.CameraFixedInfo;
import com.vxfly.helioscamera.SDKAPI.CameraProperties;
import com.vxfly.helioscamera.SDKAPI.CameraState;
import com.vxfly.helioscamera.SDKAPI.FileOperation;
import com.vxfly.helioscamera.SDKAPI.VideoPlayback;
import com.vxfly.helioscamera.baseItems.PropertyTypeInteger;
import com.vxfly.helioscamera.baseItems.PropertyTypeString;
import com.vxfly.helioscamera.baseItems.StreamResolution;
import com.vxfly.helioscamera.baseItems.TimeLapseDuration;
import com.vxfly.helioscamera.baseItems.TimeLapseInterval;
import com.vxfly.helioscamera.global.App.GlobalInfo;
import com.vxfly.helioscamera.global.App.PropertyId;
import com.vxfly.helioscamera.global.sdk.SDKSession;
import com.vxfly.helioscamera.hash.PropertyHashMapStatic;
import com.vxfly.helioscamera.log.WriteLogToDevice;
import com.icatch.wificam.customer.ICatchWificamAssist;
import com.icatch.wificam.customer.ICatchWificamControl;
import com.icatch.wificam.customer.ICatchWificamInfo;
import com.icatch.wificam.customer.ICatchWificamPlayback;
import com.icatch.wificam.customer.ICatchWificamPreview;
import com.icatch.wificam.customer.ICatchWificamProperty;
import com.icatch.wificam.customer.ICatchWificamState;
import com.icatch.wificam.customer.ICatchWificamVideoPlayback;
import com.icatch.wificam.customer.exception.IchInvalidSessionException;
import com.icatch.wificam.customer.type.ICatchCameraProperty;

public class MyCamera {

	private SDKSession mSDKSession;

	private ICatchWificamPlayback photoPlayback;
	private ICatchWificamControl cameraAction;
	private ICatchWificamVideoPlayback videoPlayback;
	private ICatchWificamPreview previewStream;
	private ICatchWificamInfo cameraInfo;
	private ICatchWificamProperty cameraProperty;
	private ICatchWificamState cameraState;
	private ICatchWificamAssist cameraAssist;
	

	private PropertyTypeInteger whiteBalance;
	private PropertyTypeInteger burst;
	private PropertyTypeInteger electricityFrequency;
	private PropertyTypeInteger dateStamp;
	private PropertyTypeInteger slowMotion;
	private PropertyTypeInteger upside;
	private PropertyTypeInteger captureDelay;
	private PropertyTypeString videoSize;
	private PropertyTypeString imageSize;
	private StreamResolution streamResolution;
	private TimeLapseInterval timeLapseInterval;
	private TimeLapseDuration timeLapseDuration;
	private PropertyTypeInteger timeLapseMode;
	
	public String ipAddress;
	public int mode;
	public String inputPassword;
	public String uid;
	public boolean needInputPassword = true;

	public MyCamera(String ipAddress, String uid, String username, String password) {
		mSDKSession = new SDKSession(ipAddress, uid, username, password);
	}
	public MyCamera() {
		mSDKSession = new SDKSession();
	}
	
	public MyCamera(String ipAddress, int mode, String uid) {
		mSDKSession = new SDKSession();
		this.ipAddress = ipAddress;
		this.mode = mode;
		this.uid = uid;
	}
	
	public Boolean initCamera() {
		boolean retValue = false;
		WriteLogToDevice.writeLog("[Normal] -- MyCamera:", "Start initCamera");
		try {
			photoPlayback = mSDKSession.getSDKSession().getPlaybackClient();
			cameraAction = mSDKSession.getSDKSession().getControlClient();
			previewStream = mSDKSession.getSDKSession().getPreviewClient();
			videoPlayback = mSDKSession.getSDKSession().getVideoPlaybackClient();
			cameraProperty = mSDKSession.getSDKSession().getPropertyClient();
			cameraInfo = mSDKSession.getSDKSession().getInfoClient();
			cameraState = mSDKSession.getSDKSession().getStateClient();
			cameraAssist = ICatchWificamAssist.getInstance();
			retValue = true;
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		}
		CameraAction.getInstance().initCameraAction();
		CameraFixedInfo.getInstance().initCameraFixedInfo();
		CameraProperties.getInstance().initCameraProperties();
		CameraState.getInstance().initCameraState();
		FileOperation.getInstance().initICatchWificamPlayback();
		VideoPlayback.getInstance().initVideoPlayback();
		PropertyHashMapStatic.getInstance().initPropertyHashMap();
		initProperty();
//		uid = mSDKSession.getUId();
		return retValue;
	}
	
	public Boolean initCameraByClint() {
		boolean retValue = false;
		WriteLogToDevice.writeLog("[Normal] -- MyCamera:", "Start initCamera");
		try {
			photoPlayback = mSDKSession.getSDKSession().getPlaybackClient();
			cameraAction = mSDKSession.getSDKSession().getControlClient();
			previewStream = mSDKSession.getSDKSession().getPreviewClient();
			videoPlayback = mSDKSession.getSDKSession().getVideoPlaybackClient();
			cameraProperty = mSDKSession.getSDKSession().getPropertyClient();
			cameraInfo = mSDKSession.getSDKSession().getInfoClient();
			cameraState = mSDKSession.getSDKSession().getStateClient();
			cameraAssist = ICatchWificamAssist.getInstance();
			retValue = true;
		} catch (IchInvalidSessionException e) {
			e.printStackTrace();
		}
		return retValue;
	}

	private void initProperty() {
		// TODO Auto-generated method stub
		WriteLogToDevice.writeLog("[Normal] -- MyCamera:", "Start initProperty");
		whiteBalance = new PropertyTypeInteger(PropertyHashMapStatic.whiteBalanceMap, PropertyId.WHITE_BALANCE, GlobalInfo
				.getInstance().getAppContext());
		burst = new PropertyTypeInteger(PropertyHashMapStatic.burstMap, ICatchCameraProperty.ICH_CAP_BURST_NUMBER, GlobalInfo
				.getInstance().getAppContext());
		dateStamp = new PropertyTypeInteger(PropertyHashMapStatic.dateStampMap, PropertyId.DATE_STAMP, GlobalInfo.getInstance()
				.getAppContext());
		slowMotion = new PropertyTypeInteger(PropertyHashMapStatic.slowMotionMap, PropertyId.SLOW_MOTION, GlobalInfo.getInstance()
				.getAppContext());
		upside = new PropertyTypeInteger(PropertyHashMapStatic.upsideMap, PropertyId.UP_SIDE, GlobalInfo.getInstance()
				.getAppContext());

		electricityFrequency = new PropertyTypeInteger(PropertyHashMapStatic.electricityFrequencyMap, PropertyId.LIGHT_FREQUENCY,
				GlobalInfo.getInstance().getAppContext());

		captureDelay = new PropertyTypeInteger(PropertyId.CAPTURE_DELAY, GlobalInfo.getInstance().getAppContext());
		videoSize = new PropertyTypeString(PropertyId.VIDEO_SIZE, GlobalInfo.getInstance().getAppContext());
		imageSize = new PropertyTypeString(PropertyId.IMAGE_SIZE, GlobalInfo.getInstance().getAppContext());
		streamResolution = new StreamResolution();
		timeLapseInterval = new TimeLapseInterval();
		timeLapseDuration = new TimeLapseDuration();
		timeLapseMode = new PropertyTypeInteger(PropertyHashMapStatic.timeLapseMode, PropertyId.TIMELAPSE_MODE,
				GlobalInfo.getInstance().getAppContext());
		WriteLogToDevice.writeLog("[Normal] -- MyCamera:", "End initProperty");
	}

	public void setMyMode(int mode){
		this.mode = mode;
	}
	
	public int getMyMode(){
		return mode;
	}
	public Boolean destroyCamera() {
		return mSDKSession.destroySession();
	}

	public SDKSession getSDKsession() {
		return mSDKSession;
	}

	public ICatchWificamPlayback getplaybackClient() {
		return photoPlayback;
	}

	public ICatchWificamControl getcameraActionClient() {
		Log.d("1111","getcameraActionClient =="+cameraAction);
		return cameraAction;
	}

	public ICatchWificamVideoPlayback getVideoPlaybackClint() {
		return videoPlayback;
	}

	public ICatchWificamPreview getpreviewStreamClient() {
		return previewStream;
	}

	public ICatchWificamInfo getCameraInfoClint() {
		return cameraInfo;
	}

	public ICatchWificamProperty getCameraPropertyClint() {
		return cameraProperty;
	}

	public ICatchWificamState getCameraStateClint() {
		return cameraState;
	}
	
	public ICatchWificamAssist getCameraAssistClint() {
		return cameraAssist;
	}

	public PropertyTypeInteger getWhiteBalance() {
		return whiteBalance;
	}

	public PropertyTypeInteger getBurst() {
		return burst;
	}

	public PropertyTypeInteger getDateStamp() {
		return dateStamp;
	}

	public PropertyTypeInteger getCaptureDelay() {
		return captureDelay;
	}
	
	public PropertyTypeInteger getSlowMotion() {
		return slowMotion;
	}

	public PropertyTypeInteger getUpside() {
		return upside;
	}

	public PropertyTypeString getVideoSize() {
		return videoSize;
	}
	
	public PropertyTypeString getImageSize() {
		return imageSize;
	}
	public PropertyTypeInteger getElectricityFrequency() {
		return electricityFrequency;
	}
	
	public StreamResolution getStreamResolution() {
		return streamResolution;
	}
	
	public TimeLapseInterval getTimeLapseInterval() {
		return timeLapseInterval;
	}
	
	public TimeLapseDuration gettimeLapseDuration() {
		return timeLapseDuration;
	}
	
	public PropertyTypeInteger getTimeLapseMode(){
		return timeLapseMode;
	}
	//JIRA ICOM-2246 Begin Add by b.jiang 2015-12-04
	/*public PropertyTypeString getTimeLapseVideoSize(){				
		return timeLapseVideoSize;
	}*/
	
	public void resetVideoSize() {
		//videoSize = new PropertyTypeString(PropertyId.VIDEO_SIZE, GlobalInfo.getInstance().getAppContext());
		videoSize = new PropertyTypeString(PropertyId.VIDEO_SIZE, GlobalInfo.getInstance().getAppContext());
		List<String> videoSizeList = videoSize.getValueListUI();
		for(int i = 0;i < videoSizeList.size(); i++){
			Log.d("TigerTiger" ,"resetVideoSize - videoSizeList["+ i+ "] = " + videoSizeList.get(i));
		}
	}
	public void resetTimeLapseVideoSize() {
		Log.d("TigerTiger" ,"start resetTimeLapseVideoSize ");
		//videoSize = new PropertyTypeString(PropertyId.VIDEO_SIZE, GlobalInfo.getInstance().getAppContext());
		videoSize = new PropertyTypeString(PropertyId.TIMELAPSE_VIDEO_SIZE_LIST_MASK, GlobalInfo.getInstance().getAppContext());
		List<String> videoSizeList = videoSize.getValueListUI();
		for(int i = 0;i < videoSizeList.size(); i++){
			Log.d("TigerTiger" ,"resetTimeLapseVideoSize - timeLapseVideoSizeList["+ i+ "] = " + videoSizeList.get(i));
		}
	}
	
	public void resetTimeLapseInterval(){
		timeLapseInterval = new TimeLapseInterval();
		String[] timeLapseList = timeLapseInterval.getValueStringList();
		for(int i = 0;i < timeLapseList.length; i++){
			Log.d("TigerTiger" ,"timeLapse["+ i+ "] = " + timeLapseList[i]);
		}
		
	}
	
	//JIRA ICOM-2246 End Add by b.jiang 2015-12-04
}
