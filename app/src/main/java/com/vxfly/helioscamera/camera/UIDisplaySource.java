package com.vxfly.helioscamera.camera;

import java.util.LinkedList;

import android.util.Log;

import com.vxfly.helioscamera.SDKAPI.CameraFixedInfo;
import com.vxfly.helioscamera.SDKAPI.CameraProperties;
import com.vxfly.helioscamera.SDKAPI.CameraState;
import com.vxfly.helioscamera.global.App.AppInfo;
import com.vxfly.helioscamera.global.App.PropertyId;
import com.vxfly.helioscamera.global.App.SettingMenu;
import com.vxfly.helioscamera.R;
import com.icatch.wificam.customer.type.ICatchCameraProperty;
import com.icatch.wificam.customer.type.ICatchMode;

public class UIDisplaySource {
	public static final int CAPTURE_SETTING_MENU = 1;
	public static final int VIDEO_SETTING_MENU = 2;
	public static final int TIMELAPSE_SETTING_MENU = 3;

	private static UIDisplaySource uiDisplayResource;
	private CameraState cameraState = CameraState.getInstance();

	public static UIDisplaySource getinstance() {
		if (uiDisplayResource == null) {
			uiDisplayResource = new UIDisplaySource();
		}

		return uiDisplayResource;
	}

	public static void createInstance() {
		uiDisplayResource = new UIDisplaySource();
	}

	public LinkedList<SettingMenu> getList(int type, MyCamera currCamera) {
		switch (type) {
		case CAPTURE_SETTING_MENU:
			return getForCaptureMode(currCamera);
		case VIDEO_SETTING_MENU:
			return getForVideoMode(currCamera);
		case TIMELAPSE_SETTING_MENU:
			return getForTimelapseMode(currCamera);
		default:
			return null;
		}

	}

	public LinkedList<SettingMenu> getForCaptureMode(MyCamera currCamera) {
		LinkedList<SettingMenu> temp = new LinkedList<SettingMenu>();
		if (CameraProperties.getInstance().hasFuction(ICatchCameraProperty.ICH_CAP_BURST_NUMBER) == true) {
			temp.add(new SettingMenu(R.string.setting_burst, currCamera.getBurst().getCurrentUiStringInSetting()));
		}
		if (CameraProperties.getInstance().hasFuction(ICatchCameraProperty.ICH_CAP_WHITE_BALANCE)) {
			temp.add(new SettingMenu(R.string.setting_awb, currCamera.getWhiteBalance().getCurrentUiStringInSetting()));
		}
		if (CameraProperties.getInstance().hasFuction(ICatchCameraProperty.ICH_CAP_LIGHT_FREQUENCY)) {
			temp.add(new SettingMenu(R.string.setting_power_supply, currCamera.getElectricityFrequency().getCurrentUiStringInSetting()));
		}
		if (CameraProperties.getInstance().hasFuction(ICatchCameraProperty.ICH_CAP_DATE_STAMP) == true) {
			temp.add(new SettingMenu(R.string.setting_datestamp, currCamera.getDateStamp().getCurrentUiStringInSetting()));
		}
		if (cameraState.isSupportImageAutoDownload()) {
			temp.add(new SettingMenu(R.string.setting_auto_download, ""));
			temp.add(new SettingMenu(R.string.setting_auto_download_size_limit, ""));
		}
		// temp.add(new SettingMenu(R.string.setting_auto_download,""));
		// temp.add(new
		// SettingMenu(R.string.setting_auto_download_size_limit,""));
		temp.add(new SettingMenu(R.string.setting_format, ""));
		// temp.add(new SettingMenu(R.string.setting_update_fw,""));

		if (CameraProperties.getInstance().hasFuction(PropertyId.UP_SIDE)) {
			temp.add(new SettingMenu(R.string.upside, currCamera.getUpside().getCurrentUiStringInSetting()));
		}
		if(CameraProperties.getInstance().hasFuction(PropertyId.CAMERA_ESSID)){//camera password and wifi
			temp.add(new SettingMenu(R.string.camera_wifi_configuration,""));
		}
			

		temp.add(new SettingMenu(R.string.setting_app_version, AppInfo.getAppVer()));
		temp.add(new SettingMenu(R.string.setting_product_name, CameraFixedInfo.getInstance().getCameraName()));
		if (CameraProperties.getInstance().hasFuction(ICatchCameraProperty.ICH_CAP_FW_VERSION)) {
			temp.add(new SettingMenu(R.string.setting_firmware_version, CameraFixedInfo.getInstance().getCameraVersion()));
		}
		return temp;
	}

	public LinkedList<SettingMenu> getForVideoMode(MyCamera currCamera) {
		LinkedList<SettingMenu> temp = new LinkedList<SettingMenu>();
		Log.d("1111", "currCamera ==" + currCamera);
		if (CameraProperties.getInstance().hasFuction(ICatchCameraProperty.ICH_CAP_WHITE_BALANCE)) {
			temp.add(new SettingMenu(R.string.setting_awb, currCamera.getWhiteBalance().getCurrentUiStringInSetting()));
		}
		if (CameraProperties.getInstance().hasFuction(ICatchCameraProperty.ICH_CAP_LIGHT_FREQUENCY)) {
			temp.add(new SettingMenu(R.string.setting_power_supply, currCamera.getElectricityFrequency().getCurrentUiStringInSetting()));
		}
		if (CameraProperties.getInstance().hasFuction(ICatchCameraProperty.ICH_CAP_DATE_STAMP) == true) {
			temp.add(new SettingMenu(R.string.setting_datestamp, currCamera.getDateStamp().getCurrentUiStringInSetting()));
		}
		if (cameraState.isSupportImageAutoDownload()) {
			temp.add(new SettingMenu(R.string.setting_auto_download, ""));
			temp.add(new SettingMenu(R.string.setting_auto_download_size_limit, ""));
		}
		// temp.add(new SettingMenu(R.string.setting_auto_download,""));
		// temp.add(new
		// SettingMenu(R.string.setting_auto_download_size_limit,""));
		temp.add(new SettingMenu(R.string.setting_format, ""));
		// temp.add(new SettingMenu(R.string.setting_update_fw,""));

		if (CameraProperties.getInstance().hasFuction(PropertyId.SLOW_MOTION)) {
			temp.add(new SettingMenu(R.string.slowmotion, currCamera.getSlowMotion().getCurrentUiStringInSetting()));
		}
		if (CameraProperties.getInstance().hasFuction(PropertyId.UP_SIDE)) {
			temp.add(new SettingMenu(R.string.upside,currCamera.getUpside().getCurrentUiStringInSetting()));
		}	
		if(CameraProperties.getInstance().hasFuction(PropertyId.CAMERA_ESSID)){//camera password and wifi
			temp.add(new SettingMenu(R.string.camera_wifi_configuration,""));
		}

		temp.add(new SettingMenu(R.string.setting_app_version, AppInfo.getAppVer()));
		temp.add(new SettingMenu(R.string.setting_product_name, CameraFixedInfo.getInstance().getCameraName()));
		if (CameraProperties.getInstance().hasFuction(ICatchCameraProperty.ICH_CAP_FW_VERSION)) {
			temp.add(new SettingMenu(R.string.setting_firmware_version, CameraFixedInfo.getInstance().getCameraVersion()));
		}

		return temp;
	}

	public LinkedList<SettingMenu> getForTimelapseMode(MyCamera currCamera) {
		LinkedList<SettingMenu> temp = new LinkedList<SettingMenu>();

		if (CameraProperties.getInstance().hasFuction(ICatchCameraProperty.ICH_CAP_WHITE_BALANCE)) {
			temp.add(new SettingMenu(R.string.setting_awb, currCamera.getWhiteBalance().getCurrentUiStringInSetting()));
		}
		if (CameraProperties.getInstance().hasFuction(ICatchCameraProperty.ICH_CAP_LIGHT_FREQUENCY)) {
			temp.add(new SettingMenu(R.string.setting_power_supply, currCamera.getElectricityFrequency().getCurrentUiStringInSetting()));
		}
		if (cameraState.isSupportImageAutoDownload()) {
			temp.add(new SettingMenu(R.string.setting_auto_download, ""));
			temp.add(new SettingMenu(R.string.setting_auto_download_size_limit, ""));
		}
		// temp.add(new SettingMenu(R.string.setting_auto_download,""));
		// temp.add(new
		// SettingMenu(R.string.setting_auto_download_size_limit,""));
		temp.add(new SettingMenu(R.string.setting_format, ""));
		// temp.add(new SettingMenu(R.string.setting_update_fw,""));

		if (CameraProperties.getInstance().cameraModeSupport(ICatchMode.ICH_MODE_TIMELAPSE)) {

			temp.add(new SettingMenu(R.string.timeLapse_mode, currCamera.getTimeLapseMode().getCurrentUiStringInSetting()));
			temp.add(new SettingMenu(R.string.setting_time_lapse_interval, currCamera.getTimeLapseInterval().getCurrentValue()));
			temp.add(new SettingMenu(R.string.setting_time_lapse_duration, currCamera.gettimeLapseDuration().getCurrentValue()));
		}

		if (CameraProperties.getInstance().hasFuction(PropertyId.UP_SIDE)) {
			temp.add(new SettingMenu(R.string.upside, currCamera.getUpside().getCurrentUiStringInSetting()));
		}
		if(CameraProperties.getInstance().hasFuction(PropertyId.CAMERA_ESSID)){//camera password and wifi
			temp.add(new SettingMenu(R.string.camera_wifi_configuration,""));
		
		}
		temp.add(new SettingMenu(R.string.setting_app_version, AppInfo.getAppVer()));
		temp.add(new SettingMenu(R.string.setting_product_name, CameraFixedInfo.getInstance().getCameraName()));
		if (CameraProperties.getInstance().hasFuction(ICatchCameraProperty.ICH_CAP_FW_VERSION)) {
			temp.add(new SettingMenu(R.string.setting_firmware_version, CameraFixedInfo.getInstance().getCameraVersion()));
		}
		return temp;
	}

}
