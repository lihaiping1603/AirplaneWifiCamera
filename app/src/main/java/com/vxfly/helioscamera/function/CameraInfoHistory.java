package com.vxfly.helioscamera.function;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;

import android.content.Context;
import android.util.Log;

import com.vxfly.helioscamera.baseItems.CameraStoreInfo;
import com.vxfly.helioscamera.log.WriteLogToDevice;

public class CameraInfoHistory {
	private final static String fileName = "CameraStoreInfo.txt";

	public static void clearFile(Context context) {
		FileOutputStream outStream = null;
		try {
			outStream = context.openFileOutput(fileName, Context.MODE_APPEND);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			outStream.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void save(CameraStoreInfo cameraStoreInfo, Context context) {
		if (cameraStoreInfo.cameraUid == null) {
			return;
		}
		LinkedList<CameraStoreInfo> retList = read(context);
		CameraStoreInfo temp = null;
		if (retList != null) {
			for (int ii = 0; ii < retList.size(); ii++) {
				temp = retList.get(ii);
				if ((temp.cameraUid).equals(cameraStoreInfo.cameraUid) == true) {
					if (temp.cameraName.equals(cameraStoreInfo.cameraName) == true) {
						return;
					} else {
						// need to update camera name
						temp.cameraName = cameraStoreInfo.cameraName;
						updateAllInfo(retList, context);
						return;
					}
				}
			}
		}

		FileOutputStream outStream = null;
		try {
			outStream = context.openFileOutput(fileName, Context.MODE_APPEND);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			outStream.write((cameraStoreInfo.cameraName + ";").getBytes());
			outStream.write((cameraStoreInfo.cameraUid + ";").getBytes());
			outStream.write((cameraStoreInfo.cameraPassword + ";").getBytes());
			outStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// outStream.
	}

	private static void updateAllInfo(LinkedList<CameraStoreInfo> cameraStoreInfoList, Context context) {
		FileOutputStream outStream = null;
		try {
			outStream = context.openFileOutput(fileName, Context.MODE_PRIVATE);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (CameraStoreInfo temp : cameraStoreInfoList) {
			try {
				outStream.write((temp.cameraName + ";").getBytes());
				outStream.write((temp.cameraUid + ";").getBytes());
				outStream.write((temp.cameraPassword + ";").getBytes());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			outStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static boolean isExist(String cameraName, Context context) {
		// boolean retValue = false;
		if (cameraName == null) {
			return false;
		}
		LinkedList<CameraStoreInfo> retList = read(context);
		CameraStoreInfo temp = null;
		if (retList != null) {
			for (int ii = 0; ii < retList.size(); ii++) {
				// Log.e("1111", "match string = " + temp);
				temp = retList.get(ii);
				if ((temp.cameraName).equals(cameraName) == true) {
					return true;
				}
			}
		}
		return false;
	}

	public static String getPwdByName(String cameraName, Context context) {
		// boolean retValue = false;

		LinkedList<CameraStoreInfo> retList = read(context);
		CameraStoreInfo temp = null;
		if (retList != null) {
			for (int ii = 0; ii < retList.size(); ii++) {
				// Log.e("1111", "match string = " + temp);
				temp = retList.get(ii);
				if ((temp.cameraName).equals(cameraName) == true) {
					return temp.cameraPassword;
				}
			}
		}
		return null;
	}

	public static LinkedList<CameraStoreInfo> read(Context context) {
		WriteLogToDevice.writeLog("[Normal] -- CameraInfoHistory: ", "Start read CameraInfoHistory");
		FileInputStream inStream = null;
		try {
			inStream = context.openFileInput(fileName);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block`
			e1.printStackTrace();
		}
		if (inStream == null) {

			return null;
		}
		try {
			if (inStream.available() <= 0) {
				return null;
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		byte[] b = null;
		try {
			b = new byte[inStream.available()];
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			inStream.read(b);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			inStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (b == null) {
			return null;
		}
		String str2 = new String(b);//
		if (str2 == null) {
			return null;
		}
		String newStr = str2.replaceAll("\"", "");
		String[] temp = newStr.split(";");
		if (temp.length == 0) {
			return null;
		}
		for (int ii = 0; ii < temp.length; ii = ii + 1) {
			Log.e("1111", "temp[ii] = " + temp[ii]);
		}
		LinkedList<CameraStoreInfo> retList = new LinkedList<CameraStoreInfo>();
		for (int ii = 0; ii < temp.length; ii = ii + 3) {
			retList.addLast(new CameraStoreInfo(temp[ii], temp[ii + 1], temp[ii + 2]));
		}

		return retList;
	}
}