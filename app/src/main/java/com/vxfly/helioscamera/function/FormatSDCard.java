/**
 * Added by zhangyanhu C01012,2014-8-20
 */
package com.vxfly.helioscamera.function;

import android.os.Handler;

import com.vxfly.helioscamera.SDKAPI.CameraAction;
import com.vxfly.helioscamera.global.App.GlobalInfo;

/**
 * Added by zhangyanhu C01012,2014-8-20
 */
public class FormatSDCard extends Thread{
	//private CameraAction cameraAction;
	private Handler handler;
	FormatSDCard(Handler handler){
		this.handler = handler;
		//cameraAction = new CameraAction();
	}
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		// TODO Auto-generated method stub
		handler.obtainMessage(GlobalInfo.MESSAGE_FORMAT_SD_START).sendToTarget();
		try {
			sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(CameraAction.getInstance().formatStorage()){
			handler.obtainMessage(GlobalInfo.MESSAGE_FORMAT_SUCCESS).sendToTarget();
		}else{
			handler.obtainMessage(GlobalInfo.MESSAGE_FORMAT_FAILED).sendToTarget();
		}
	}

}
