package com.vxfly.helioscamera.function;

import android.app.Activity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kaifa on 2016/12/26.
 */
public class ActivityCollector {

    //添加一个list用于记录目前所有运行的activity
    //add by lihaiping1603@aliyun.com on 2016-12-26 start
    public static List<Activity> activitysList=new ArrayList<Activity>();

    public static void addActivity(Activity activity){
        activitysList.add(activity);
    }

    public static void removeActivity(Activity activity){
        activitysList.remove(activity);
    }

    public static void finishAll(){
        for(Activity activity:activitysList){
            if(!activity.isFinishing()){
                activity.finish();
            }
        }
    }
    //add by lihaiping1603@aliyun.com on 2016-12-26 end
}
