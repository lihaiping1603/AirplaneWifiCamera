package com.vxfly.helioscamera.function;

import java.io.ByteArrayOutputStream;

import com.vxfly.helioscamera.global.App.CameraSlot;
import com.vxfly.helioscamera.log.WriteLogToDevice;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;

public class CameraSlotSQLite {
	private SQLiteDatabase db;
	private String currentSlotName = null;
	private String currentCamSsid = null;

	public String getCurrentSlotName() {
		return currentSlotName;
	}

	public void setCurrentSlotName(String currentSlotName) {
		this.currentSlotName = currentSlotName;
	}

	public String getCurrentCamSsid() {
		return currentCamSsid;
	}

	public void setCurrentCamSsid(String currentCamSsid) {
		this.currentCamSsid = currentCamSsid;
	}

	private static CameraSlotSQLite instance = new CameraSlotSQLite();

	public static CameraSlotSQLite getInstance() {
		if (instance == null) {
			instance = new CameraSlotSQLite();
		}
		return instance;
	}

	public void creatTable(Context context) {
		CameraSlotSQLiteHelper dbHelper = new CameraSlotSQLiteHelper(context);
		this.db = dbHelper.getWritableDatabase();
		WriteLogToDevice.writeLog("[Normal] -- SQLiteOperate: ", "openSQLite");
		// 创建caminfo表
		// db.execSQL("CREATE TABLE IF NOT EXISTS caminfo (_id INTEGER PRIMARY KEY AUTOINCREMENT, slotname VARCHAR, cname VARCHAR,imagebuffer BLOB)");

	}

	// 插入数据
	public void insert(CameraSlot camSlot) {
		WriteLogToDevice.writeLog("[Normal] -- SQLiteOperate: ", "start insert slotName=" + camSlot.slotName);
		// 实例化常量值
		ContentValues cValue = new ContentValues();
		cValue.put("slotname", camSlot.slotName);
		cValue.put("cname", camSlot.cameraName);
		cValue.put("imagebuffer", camSlot.cameraPhoto);

		// 调用insert()方法插入数据
		db.insert("caminfo", null, cValue);
		WriteLogToDevice.writeLog("[Normal] -- SQLiteOperate: ", "end insert slotName=" + camSlot.slotName);
	}

	// 更新数据
	public void update(CameraSlot camSlot) {
		WriteLogToDevice.writeLog("[Normal] -- SQLiteOperate: ", "start update slotName=" + camSlot.slotName);
		ContentValues values = new ContentValues();
		// 在values中添加内容
		values.put("imagebuffer", camSlot.cameraPhoto);
		// 修改条件
		String whereClause = "slotname=?";
		// 修改添加参数
		String[] whereArgs = { String.valueOf(camSlot.slotName) };
		// 修改
		db.update("caminfo", values, whereClause, whereArgs);
		WriteLogToDevice.writeLog("[Normal] -- SQLiteOperate: ", "end start update slotName=" + camSlot.slotName);
	}

	// 删除数据
	public void delete(String slotName) {
		WriteLogToDevice.writeLog("[Normal] -- SQLiteOperate: ", "start delete slotName=" + slotName);
		// 删除SQL语句
		String whereClause = "slotname=?";
		// 修改添加参数
		String[] whereArgs = { String.valueOf(slotName) };
		db.delete("caminfo", whereClause, whereArgs);
		WriteLogToDevice.writeLog("[Normal] -- SQLiteOperate: ", "end delete slotName=" + slotName);
	}

	// 查找数据
	public Cursor query(String slotName) {
		WriteLogToDevice.writeLog("[Normal] -- SQLiteOperate: ", "start query slotName=" + slotName);
		String selection = "slotname=?";
		// 修改添加参数
		String[] selectionArgs = { String.valueOf(slotName) };
		// 查询获得游标
		Cursor cursor = db.query("caminfo", null, selection, selectionArgs, null, null, null);

		// 判断游标是否为空
		while (cursor.moveToNext()) {
			// 遍历游标
			int id = cursor.getInt(cursor.getColumnIndex("_id"));
			String sName = cursor.getString(cursor.getColumnIndex("slotname"));
			String camName = cursor.getString(cursor.getColumnIndex("cname"));
			WriteLogToDevice.writeLog("[Normal] -- SQLite: ", "_id=" + id + " slotName=" + sName + " camName=" + camName);
		}
		WriteLogToDevice.writeLog("[Normal] -- SQLiteOperate: ", "end query slotName=" + slotName);
		return cursor;
	}

	public Cursor queryCamName(String camName) {
		WriteLogToDevice.writeLog("[Normal] -- SQLiteOperate: ", "start queryCamName camName=" + camName);
		String selection = "cname=?";
		// 修改添加参数
		String[] selectionArgs = { String.valueOf(camName) };
		// 查询获得游标
		Cursor cursor = db.query("caminfo", null, selection, selectionArgs, null, null, null);

		// 判断游标是否为空
		while (cursor.moveToNext()) {
			// 遍历游标
			int id = cursor.getInt(cursor.getColumnIndex("_id"));
			String sName = cursor.getString(cursor.getColumnIndex("slotname"));
			String curCamName = cursor.getString(cursor.getColumnIndex("cname"));
			WriteLogToDevice.writeLog("[Normal] -- SQLite: ", "_id=" + id + " slotName=" + sName + " camName=" + curCamName);
		}
		WriteLogToDevice.writeLog("[Normal] -- SQLiteOperate: ", "end queryCamName camName=" + camName);
		return cursor;
	}

	// 更新图片数据
	public void updateImage(Bitmap bitmap) {
		CameraSlot camSlot;
		if (currentSlotName == null || currentCamSsid == null) {
			return;
		}
		WriteLogToDevice.writeLog("[Normal] -- SQLiteOperate: ", "start updateImage slotName=" + currentSlotName);
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		if (bitmap != null) {
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
			camSlot = new CameraSlot(currentSlotName, true, currentCamSsid, os.toByteArray(), false);
		} else {
			camSlot = new CameraSlot(currentSlotName, true, currentCamSsid, null, false);

		}
		if (query(currentSlotName).getCount() > 0) {
			update(camSlot);
			WriteLogToDevice.writeLog("[ERROR] -- AppStartActivity: ", currentSlotName + " is exists!");
		} else {
			insert(camSlot);
		}

		WriteLogToDevice.writeLog("[Normal] -- SQLiteOperate: ", "end start updateImage slotName=" + currentSlotName);
	}

	public void closeDB() {
		db.close();
	}

}
