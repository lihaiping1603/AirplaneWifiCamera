package com.vxfly.helioscamera.function;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;

import android.content.Context;

import com.vxfly.helioscamera.baseItems.WifiSsidPwd;
import com.vxfly.helioscamera.log.WriteLogToDevice;

public class WifiHistory {
	private final static String fileName = "WifiSsidPwd.txt";

	public static void clearFile(Context context) {
		FileOutputStream outStream = null;
		try {
			outStream = context.openFileOutput(fileName, Context.MODE_APPEND);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			outStream.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void save(WifiSsidPwd wifiSsidPwd, Context context) {
		if (wifiSsidPwd == null) {
			return;
		}

		FileOutputStream outStream = null;
		// String tempString = myString +";";
		try {
			outStream = context.openFileOutput(fileName, Context.MODE_PRIVATE);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			outStream.write((wifiSsidPwd.ssid + ";").getBytes());
			outStream.write((wifiSsidPwd.pwd + ";").getBytes());
			outStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void updateAllInfo(LinkedList<WifiSsidPwd> wifiSsidPwdList, Context context) {
		FileOutputStream outStream = null;
		try {
			outStream = context.openFileOutput(fileName, Context.MODE_PRIVATE);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (WifiSsidPwd temp : wifiSsidPwdList) {
			try {
				// outStream.write((temp.cameraName + ";").getBytes());
				outStream.write((temp.ssid + ";").getBytes());
				outStream.write((temp.pwd + ";").getBytes());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			outStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static boolean isExist(String ssid, Context context) {
		// boolean retValue = false;
		if (ssid == null) {
			return false;
		}
		LinkedList<WifiSsidPwd> retList = read(context);
		WifiSsidPwd temp = null;
		if (retList != null) {
			for (int ii = 0; ii < retList.size(); ii++) {
				temp = retList.get(ii);
				if ((temp.ssid).equals(ssid) == true) {
					return true;
				}
			}
		}
		return false;
	}

	public static String getPwdByName(String ssid, Context context) {
		LinkedList<WifiSsidPwd> retList = read(context);
		WifiSsidPwd temp = null;
		if (retList != null) {
			for (int ii = 0; ii < retList.size(); ii++) {
				temp = retList.get(ii);
				if ((temp.ssid).equals(ssid) == true) {
					return temp.pwd;
				}
			}
		}
		return null;
	}

	public static LinkedList<WifiSsidPwd> read(Context context) {
		WriteLogToDevice.writeLog("[Normal] -- WifiHistory: ", "Start read WifiHistory");
		FileInputStream inStream = null;
		try {
			inStream = context.openFileInput(fileName);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block`
			e1.printStackTrace();
		}
		if (inStream == null) {

			return null;
		}
		try {
			if (inStream.available() <= 0) {
				return null;
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		byte[] b = null;
		try {
			b = new byte[inStream.available()];
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			inStream.read(b);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			inStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (b == null) {
			return null;
		}
		String str2 = new String(b);//
		if (str2 == null) {
			return null;
		}
		String[] temp = str2.split(";");
		if (temp.length == 0) {
			return null;
		}
		LinkedList<WifiSsidPwd> retList = new LinkedList<WifiSsidPwd>();
		for (int ii = 0; ii < temp.length; ii = ii + 2) {
			retList.addLast(new WifiSsidPwd(temp[ii], temp[ii + 1]));
		}

		return retList;
	}
}