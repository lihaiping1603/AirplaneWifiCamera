package com.vxfly.helioscamera.function;

import java.util.LinkedList;
import java.util.Queue;

import com.vxfly.helioscamera.SDKAPI.CameraAction;

import android.util.Log;

public class PreviewMove {
	private static PreviewMove previewMove;
	private volatile Queue<MoveInfo> moveTaskList;
	private volatile boolean isThreadRunning = false;
	private  MoveInfo tempMoveInfo;
	PreviewMoveThread previewMoveThread;

	public static PreviewMove getInstance() {
		if ( previewMove == null) {
			previewMove = new PreviewMove();
		}
		return previewMove;
	}

	public void addMoveTask(int xMove, int yMove) {
		Log.d("1111","-------------------addMoveTask------------ ");
		if (moveTaskList == null) {
			moveTaskList = new LinkedList<MoveInfo>();
		}
		moveTaskList.offer(new MoveInfo(xMove, yMove));
		if(isThreadRunning == false){
			Log.d("1111","-------------------isThreadRunning == false------------ ");
			isThreadRunning = true;
			if(previewMoveThread == null || previewMoveThread.isAlive() == false){
				previewMoveThread = new PreviewMoveThread();
				previewMoveThread.start();
			}				
		}
	}

	private class  PreviewMoveThread extends Thread{
		@Override
		public void run() {
			while(isThreadRunning == true && moveTaskList.isEmpty() == false){
				
				tempMoveInfo = moveTaskList.poll();
				Log.d("1111","-------------------start previewMove------------ tempMoveInfo.x="+tempMoveInfo.x);
				Log.d("1111","-------------------start previewMove------------ tempMoveInfo.y="+tempMoveInfo.y);
				CameraAction.getInstance().previewMove(tempMoveInfo.x, tempMoveInfo.y);
			}
			isThreadRunning = false;
		}
	}
	
	public void resetPreview(){
		Log.d("1111","-------------------start resetPreviewMove------------ ");
		CameraAction.getInstance().resetPreviewMove();
	}

	public void stopThread(){
		if(moveTaskList != null){
			moveTaskList.clear();
		}
		
		if(isThreadRunning == true){
			if(previewMoveThread != null){
				previewMoveThread.interrupt();				
			}
		}
		isThreadRunning = false;
	}
	private class MoveInfo {

		public int x;
		public int y;

		public MoveInfo(int xMove, int yMove) {
			// TODO Auto-generated constructor stub
			this.x = xMove;
			this.y = yMove;
		}
	}
}
