package com.vxfly.helioscamera.function;

import android.os.Handler;

import com.vxfly.helioscamera.SDKAPI.CameraAction;
import com.vxfly.helioscamera.Tool.FileTools;
import com.vxfly.helioscamera.global.App.GlobalInfo;
import com.vxfly.helioscamera.R;

public class UpdateFW extends Thread {
	private Handler handler;

	public UpdateFW(Handler handler) {
		this.handler = handler;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		FileTools.copyFile(R.raw.sphost);
		boolean ret = false;
		ret = CameraAction.getInstance().updateFW();
		if (!ret) {
			handler.obtainMessage(GlobalInfo.MESSAGE_UPDATE_FW_FAILED).sendToTarget();
			return;
		}

	}
}