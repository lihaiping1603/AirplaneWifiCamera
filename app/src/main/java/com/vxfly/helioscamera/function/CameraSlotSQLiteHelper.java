package com.vxfly.helioscamera.function;

import com.vxfly.helioscamera.global.App.GlobalInfo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CameraSlotSQLiteHelper extends SQLiteOpenHelper {

	private String CREATE_CAMINFODB = "CREATE TABLE IF NOT EXISTS caminfo (_id INTEGER PRIMARY KEY AUTOINCREMENT, slotname VARCHAR, cname VARCHAR,imagebuffer BLOB)";
	private String DROP_CAMINFODB = "drop table if exists caminfo";

	public CameraSlotSQLiteHelper(Context context) {
		super(context, GlobalInfo.CAMERA_CONNECTINFO_DATEBASE_NAME, null, GlobalInfo.CAMERA_SLOTSQLITE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(CREATE_CAMINFODB);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL(DROP_CAMINFODB);
		onCreate(db);
	}

}
