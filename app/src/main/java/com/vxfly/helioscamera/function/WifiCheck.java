package com.vxfly.helioscamera.function;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiConfiguration.AuthAlgorithm;
import android.net.wifi.WifiConfiguration.KeyMgmt;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.vxfly.helioscamera.Activity.AppStartActivity;
import com.vxfly.helioscamera.adapter.WifiListAdapter;
import com.vxfly.helioscamera.global.App.ExitApp;
import com.vxfly.helioscamera.global.App.GlobalInfo;
import com.vxfly.helioscamera.global.sdk.SDKEvent;
import com.vxfly.helioscamera.log.WriteLogToDevice;
import com.vxfly.helioscamera.R;
import com.icatch.wificam.customer.type.ICatchEventID;

public class WifiCheck {
	private static final int CONNECT_FAILED = 0x02;
	private static final int IN_BACKGROUND = 0x03;
	private static final int RECONNECT_SUCCESS = 0x04;
	private static final int RECONNECT_FAILED = 0x05;
	public static final int WIFICIPHER_NOPASS = 0x06;
	public static final int WIFICIPHER_WEP = 0x07;
	public static final int WIFICIPHER_WAP = 0x08;
	private static final int RECONNECT_CAMERA = 0x09;
	private static int RECONNECT_WAITING = 10000;
	private static int RECONNECT_CHECKING_PERIOD = 5000;
	private GlobalInfo globalInfo = GlobalInfo.getInstance();
	private Boolean isShowed = false;
	private Timer connectTimer;
	private AlertDialog dialog;
	// 定义WifiManager对象
	private WifiManager mWifiManager;
	// 定义WifiInfo对象
	private WifiInfo mWifiInfo;
	// 扫描出的网络连接列表
	private List<ScanResult> mWifiList;
	// 网络连接列表
	private List<WifiConfiguration> mWifiConfiguration;
	// 定义一个WifiLock
	private WifiLock mWifiLock;
	private int reconnectTime = 0;
	private ProgressDialog progressDialog;
	private SDKEvent sdkEvent;
	private Timer reconnectTimer;
	protected AlertDialog reconnectDialog;

	public WifiCheck() {
		// 取得WifiManager对象
		mWifiManager = (WifiManager) (globalInfo.getCurrentApp()).getSystemService(Context.WIFI_SERVICE);
		// 取得WifiInfo对象
		mWifiInfo = mWifiManager.getConnectionInfo();
	}

	public static boolean isSSStrong(Context context) {
		int strength = 0;

		WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		WifiInfo info = wifi.getConnectionInfo();
		if (info.getBSSID() != null) {
			strength = WifiManager.calculateSignalLevel(info.getRssi(), 4);
		}
		return strength > 1;
	}

	public static boolean isOnline(Context context) {
		ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

		if (networkInfo != null && networkInfo.isConnected() == true) {
			return true;
		} else {
			return false;
		}

	}

	public void checkWifiPolicy() {
		ContentResolver resolver = GlobalInfo.getInstance().getCurrentApp().getContentResolver();
		int value = Settings.System.getInt(resolver, Settings.System.WIFI_SLEEP_POLICY, Settings.System.WIFI_SLEEP_POLICY_DEFAULT);
		WriteLogToDevice.writeLog("tigertiger", " Settings.System.WIFI_SLEEP_POLICY =" + value);
		if (Settings.System.WIFI_SLEEP_POLICY_NEVER != value) {
			AlertDialog.Builder builder = new AlertDialog.Builder(GlobalInfo.getInstance().getCurrentApp());
			builder.setMessage(GlobalInfo.getInstance().getCurrentApp().getString(R.string.check_wifi_policy));
			builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
			AlertDialog checkDialog = builder.create();
			checkDialog.setCancelable(true);
			checkDialog.show();
		}
	}

	/**
	 * Added by zhangyanhu C01012,2014-2-17
	 */
	public void cancelConnectCheck() {
		if (sdkEvent != null) {
			sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_CONNECTION_DISCONNECTED);
		}
	}

	/**
	 * check the wifi connect to camera periodly Added by zhangyanhu
	 * C01012,2014-2-13
	 */
	public void openConnectCheck() {
		sdkEvent = new SDKEvent(wifiCheckHandler);
		sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_CONNECTION_DISCONNECTED);
	}

	private final Handler wifiCheckHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case CONNECT_FAILED:
				break;
			case GlobalInfo.EVENT_CONNECTION_FAILURE:
				WriteLogToDevice.writeLog("WifiCheck", " receive EVENT_CONNECTION_FAILURE");
				Log.d("3333", "WifiCheck receive EVENT_CONNECTION_FAILURE");
				// ConfigureInfo.readCfgInfo();
				if (GlobalInfo.isSupportAutoReconnection == false) {
					showWarningDlg(globalInfo.getAppContext());
				} else {
					reconnectTimer = new Timer(true);
					ReconnectTask task = new ReconnectTask();
					reconnectTimer.schedule(task, 0, RECONNECT_CHECKING_PERIOD);
					if (dialog != null) {
						dialog.dismiss();
					}
					AlertDialog.Builder builder = new AlertDialog.Builder(globalInfo.getAppContext());
					builder.setIcon(R.drawable.warning).setTitle(R.string.dialog_btn_reconnect).setMessage(R.string.message_reconnect);
					builder.setPositiveButton(R.string.dialog_btn_exit, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							ExitApp.getInstance().exit();
						}
					});
					reconnectDialog = builder.create();
					reconnectDialog.setCancelable(false);
					reconnectDialog.show();
					// wifiCheckHandler.obtainMessage(RECONNECT_CAMERA).sendToTarget();
				}

				break;
			case IN_BACKGROUND:
				ExitApp.getInstance().exit();
				break;
			case RECONNECT_SUCCESS:
				ExitApp.getInstance().finishAllActivity();
				Intent intent = new Intent();
				intent.setClass(globalInfo.getAppContext(), AppStartActivity.class);
				globalInfo.startActivity(intent);
				progressDialog.dismiss();
				if (connectTimer != null) {
					connectTimer.cancel();
				}
				break;
			case RECONNECT_FAILED:
				ExitApp.getInstance().finishAllActivity();
				Intent intent1 = new Intent();
				intent1.setClass(globalInfo.getAppContext(), AppStartActivity.class);
				globalInfo.startActivity(intent1);
				progressDialog.dismiss();
				if (connectTimer != null) {
					connectTimer.cancel();
				}
				break;
			case RECONNECT_CAMERA:
				if (dialog != null) {
					dialog.dismiss();
				}
				AlertDialog.Builder builder = new AlertDialog.Builder(globalInfo.getAppContext());
				builder.setIcon(R.drawable.warning).setTitle(R.string.dialog_btn_reconnect).setMessage(R.string.message_reconnect);
				builder.setPositiveButton(R.string.dialog_btn_exit, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						ExitApp.getInstance().exit();
					}
				});
				reconnectDialog = builder.create();
				reconnectDialog.setCancelable(false);
				reconnectDialog.show();
				break;
			}
		}
	};
	private WifiManager wifiManager;
	private List<ScanResult> list;

	private void showWarningDlg(Context context) {
		if (isShowed == true) {
			return;
		}
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setIcon(R.drawable.warning).setTitle("Warning").setMessage(R.string.dialog_timeout);
		builder.setPositiveButton(R.string.dialog_btn_exit, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				ExitApp.getInstance().exit();
			}
		});
		builder.setNegativeButton(R.string.dialog_btn_reconnect, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				reconnectTimer = new Timer(true);
				ReconnectTask task = new ReconnectTask();
				reconnectTimer.schedule(task, 0, RECONNECT_CHECKING_PERIOD);
				wifiCheckHandler.obtainMessage(RECONNECT_CAMERA).sendToTarget();
			}
		});
		if (dialog == null) {
			dialog = builder.create();
			dialog.setCancelable(false);
			dialog.show();
		}
	}

	// 打开WIFI
	public void openWifi() {
		if (!mWifiManager.isWifiEnabled()) {
			mWifiManager.setWifiEnabled(true);
		}
	}

	// 关闭WIFI
	public void closeWifi() {
		if (mWifiManager.isWifiEnabled()) {
			mWifiManager.setWifiEnabled(false);
		}
	}

	// 检查当前WIFI状态
	public int checkState() {
		return mWifiManager.getWifiState();
	}

	// 锁定WifiLock
	public void acquireWifiLock() {
		mWifiLock.acquire();
	}

	// 解锁WifiLock
	public void releaseWifiLock() {
		// 判断时候锁定
		if (mWifiLock.isHeld()) {
			mWifiLock.acquire();
		}
	}

	// 创建一个WifiLock
	public void creatWifiLock() {
		mWifiLock = mWifiManager.createWifiLock("Test");
	}

	// 得到配置好的网络
	public List<WifiConfiguration> getConfiguration() {
		return mWifiConfiguration;
	}

	// 指定配置好的网络进行连接
	public void connectConfiguration(int index) {
		// 索引大于配置好的网络索引返回
		if (index > mWifiConfiguration.size()) {
			return;
		}
		// 连接配置好的指定ID的网络
		mWifiManager.enableNetwork(mWifiConfiguration.get(index).networkId, true);
	}

	public void startScan() {
		mWifiManager.startScan();
		// 得到扫描结果
		mWifiList = mWifiManager.getScanResults();
		// 得到配置好的网络连接
		mWifiConfiguration = mWifiManager.getConfiguredNetworks();
	}

	// 查看扫描结果
	public StringBuilder lookUpScan() {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < mWifiList.size(); i++) {
			stringBuilder.append("Index_" + new Integer(i + 1).toString() + ":");
			// 将ScanResult信息转换成一个字符串包
			// 其中把包括：BSSID、SSID、capabilities、frequency、level
			stringBuilder.append((mWifiList.get(i)).toString());
			stringBuilder.append("/n");
		}
		return stringBuilder;
	}

	// 得到MAC地址
	public String getMacAddress() {
		return (mWifiInfo == null) ? "NULL" : mWifiInfo.getMacAddress();
	}

	// 得到接入点的BSSID
	public String getBSSID() {
		return (mWifiInfo == null) ? "NULL" : mWifiInfo.getBSSID();
	}

	// 得到IP地址
	public int getIPAddress() {
		return (mWifiInfo == null) ? 0 : mWifiInfo.getIpAddress();
	}

	// 得到连接的ID
	public int getNetworkId() {
		return (mWifiInfo == null) ? 0 : mWifiInfo.getNetworkId();
	}

	// 得到WifiInfo的所有信息包
	public String getWifiInfo() {
		return (mWifiInfo == null) ? "NULL" : mWifiInfo.toString();
	}

	// 添加一个网络并连接
	public void addNetwork(WifiConfiguration wcg) {
		int wcgID = mWifiManager.addNetwork(wcg);
		boolean b = mWifiManager.enableNetwork(wcgID, true);
		System.out.println("a--" + wcgID);
		System.out.println("b--" + b);
	}

	// 断开指定ID的网络
	public void disconnectWifi(int netId) {
		mWifiManager.disableNetwork(netId);
		mWifiManager.disconnect();
	}

	// wifi connection is ok
	public boolean isWifiConnect(Context context) {
		WriteLogToDevice.writeLog("[Normal] -- WifiCheck: ", "isWifiConnect?");
		if (context != null) {
			ConnectivityManager mConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWiFiNetworkInfo = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			if (mWiFiNetworkInfo != null) {
				WifiManager mWifi = (WifiManager) GlobalInfo.getInstance().getCurrentApp().getSystemService(Context.WIFI_SERVICE);
				WifiInfo wifiInfo = mWifi.getConnectionInfo();
				if (wifiInfo.getIpAddress() != 0) {
					WriteLogToDevice.writeLog("[Normal] -- AppStart: ", "isWifiConnect? true!");
					return mWiFiNetworkInfo.isAvailable();
				}

			}
		}
		WriteLogToDevice.writeLog("[Normal] -- AppStart: ", "isWifiConnect? false");
		return false;
	}

	public boolean isWifiConnected(Context context, String nameFilter) {
		if (context != null) {
			ConnectivityManager mConnectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWiFiNetworkInfo = mConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			if (mWiFiNetworkInfo != null) {
				WifiManager mWifi = (WifiManager) GlobalInfo.getInstance().getCurrentApp().getSystemService(Context.WIFI_SERVICE);
				WifiInfo wifiInfo = mWifi.getConnectionInfo();
				if (wifiInfo.getIpAddress() != 0 && wifiInfo.getSSID().contains(nameFilter) == true) {
					return true;
				}

			}
		}

		return false;
	}

	public static boolean isWifiContected(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo wifiNetworkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		if (wifiNetworkInfo.isConnected()) {

			Log.i(Thread.currentThread().getName(), "isWifiContected");
			return true;
		}
		Log.i(Thread.currentThread().getName(), "isWifiDisconnected");
		return false;

	}

	public int getSecurity(ScanResult result) {
		if (result.capabilities.contains("WEP")) {
			return WIFICIPHER_WEP;
		} else if (result.capabilities.contains("WPA")) {
			return WIFICIPHER_WAP;
		} else {
			return WIFICIPHER_NOPASS;
		}
		// return 1;
	}

	public WifiConfiguration CreateWifiInfo(String SSID, String Password, int Type) {
		WifiConfiguration config = new WifiConfiguration();
		config.allowedAuthAlgorithms.clear();
		config.allowedGroupCiphers.clear();
		config.allowedKeyManagement.clear();
		config.allowedPairwiseCiphers.clear();
		config.allowedProtocols.clear();
		config.SSID = "\"" + SSID + "\"";
		if (Type == WIFICIPHER_NOPASS) // Data.WIFICIPHER_NOPASS
		{
			config.wepKeys[0] = "";
			config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
			config.wepTxKeyIndex = 0;
		}
		if (Type == WIFICIPHER_WEP) // Data.WIFICIPHER_WEP
		{
			if (Password.length() != 0) {
				int length = Password.length();
				// WEP-40, WEP-104, and 256-bit WEP
				// (WEP-232?)
				if ((length == 10 || length == 26 || length == 58) && Password.matches("[0-9A-Fa-f]*")) {
					config.wepKeys[0] = Password;
				} else {
					config.wepKeys[0] = '"' + Password + '"';
				}
			}
			config.allowedKeyManagement.set(KeyMgmt.NONE);
			config.allowedAuthAlgorithms.set(AuthAlgorithm.OPEN);
			config.allowedAuthAlgorithms.set(AuthAlgorithm.SHARED);
			config.wepTxKeyIndex = 0;
		}
		if (Type == WIFICIPHER_WAP) // Data.WIFICIPHER_WPA
		{
			config.preSharedKey = "\"" + Password + "\"";
			config.hiddenSSID = true;
			config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
			config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
			config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
			config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
			config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
			config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
			config.status = WifiConfiguration.Status.ENABLED;
		}
		return config;
	}

	public void connectWifi(String SSID, String Password, int Type) {
		WifiManager wifiManager = (WifiManager) globalInfo.getAppContext().getSystemService(Context.WIFI_SERVICE);
		WifiConfiguration config = CreateWifiInfo(SSID, Password, Type);
		int netID = wifiManager.addNetwork(config);
		boolean bRet = wifiManager.enableNetwork(netID, true);
		Log.d("tigertiger", "-----------bRet =" + bRet);
	}

	// 分为三种情况：1没有密码2用wep加密3用wpa加密
	/**
	 * 
	 * Added by zhangyanhu C01012,2014-4-4
	 */
	public void wifiListDialog() {
		wifiManager = (WifiManager) globalInfo.getAppContext().getSystemService(Context.WIFI_SERVICE);
		// openWifi();
		list = wifiManager.getScanResults();
		Log.d("tiger", "list.size(): " + list.size());
		for (int ii = 0; ii < list.size();) {
			if (list.get(ii).SSID.equals("") == true) {
				list.remove(ii);
				continue;
			} else {
				ii++;
			}
		}
		Log.d("tiger", "list.size(): " + list.size());
		final Builder builder = new AlertDialog.Builder(globalInfo.getAppContext());
		WifiListAdapter wifiListAdapter = new WifiListAdapter(globalInfo.getAppContext(), list);
		builder.setTitle("Please select wifi:");
		builder.setCancelable(false);
		builder.setAdapter(wifiListAdapter, new OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				// TODO Auto-generated method stub
				passwordDialog(arg1);
			}

		});
		builder.create().show();
	}

	/**
	 * 
	 * Added by zhangyanhu C01012,2014-4-9
	 */
	public void passwordDialog(int arg2) {
		final int position = arg2;
		final Builder builder = new AlertDialog.Builder(globalInfo.getAppContext());
		builder.setCancelable(false);
		builder.setTitle("Please input the password:");
		RelativeLayout passwordLayout = (RelativeLayout) (LayoutInflater.from(globalInfo.getAppContext()).inflate(R.layout.input_password, null));
		final EditText password = (EditText) passwordLayout.findViewById(R.id.password);
		builder.setView(passwordLayout);
		builder.setPositiveButton("Submit", new OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				// TODO Auto-generated method stub
				InputMethodManager imm = (InputMethodManager) globalInfo.getAppContext().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(password.getWindowToken(), 0);

				String temp = "";
				temp = password.getText().toString();
				connectWifi(list.get(position).SSID, temp, getSecurity(list.get(position)));

				connectTimer = new Timer(true);
				reconnectTime = 0;
				TimerTask task = new TimerTask() {

					@Override
					public void run() {
						if (isWifiConnect(GlobalInfo.getInstance().getCurrentApp()) == true) {
							wifiCheckHandler.obtainMessage(RECONNECT_SUCCESS).sendToTarget();
						} else if (reconnectTime > 5) {
							wifiCheckHandler.obtainMessage(RECONNECT_FAILED).sendToTarget();
						}
						reconnectTime++;
					}
				};
				connectTimer.schedule(task, 0, 2000);
				progressDialog = new ProgressDialog(globalInfo.getAppContext());
				progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				progressDialog.setMessage("connecting Wifi");
				progressDialog.setCancelable(false);
				progressDialog.show();
			}
		});
		builder.setNegativeButton("Cancel", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
			}
		});
		builder.create().show();
	}

	private class ReconnectTask extends TimerTask {

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.util.TimerTask#run()
		 */
		@Override
		public void run() {
			// TODO Auto-generated method stub
			// 取得WifiManager对象
			mWifiManager = (WifiManager) (globalInfo.getAppContext()).getSystemService(Context.WIFI_SERVICE);
			// 取得WifiInfo对象
			mWifiInfo = mWifiManager.getConnectionInfo();
			mWifiInfo.getSSID();
			Log.e("tigertiger", "reconnect mWifiInfo.getSSID(); ==" + mWifiInfo.getSSID());
			if (mWifiInfo.getSSID().equals(globalInfo.getSsid())) {
				if (reconnectTimer != null) {
					reconnectTimer.cancel();
				}
				globalInfo.getCurrentCamera().destroyCamera();
				TimerTask task = new TimerTask() {

					@Override
					public void run() {
						if (reconnectDialog != null) {
							reconnectDialog.dismiss();
						}
						cancelConnectCheck();
						ExitApp.getInstance().finishAllActivity();
						// ICOM-1890 Start delete by b.jiang 2015-09-01
						// Intent intent = new Intent();
						// intent.setClass(globalInfo.getAppContext(),
						// AppStartActivity.class);
						// globalInfo.startActivity(intent);
						// ICOM-1890 End delete by b.jiang 2015-09-01
					}
				};

				Timer tempTimer = new Timer(true);
				tempTimer.schedule(task, RECONNECT_WAITING);
			}

		}

	}

	public List<ScanResult> getWifiListByFilter(String nameFilter) {
		WifiManager wifiManager = (WifiManager) globalInfo.getAppContext().getSystemService(Context.WIFI_SERVICE);
		List<ScanResult> wifiList = wifiManager.getScanResults();
		List<ScanResult> tempList = new LinkedList<ScanResult>();
		for (int ii = 0; ii < wifiList.size(); ii++) {
			if ((wifiList.get(ii).SSID).contains(nameFilter) == false) {
				tempList.add(wifiList.get(ii));
			}
		}
		wifiList.removeAll(tempList);
		return wifiList;
	}

	public List<ScanResult> getWifiList() {
		WifiManager wifiManager = (WifiManager) (GlobalInfo.getInstance().getCurrentApp().getSystemService(Context.WIFI_SERVICE));
		wifiManager.startScan();
		List<ScanResult> wifiList = wifiManager.getScanResults();
		return wifiList;
	}
	//JIRA ICOM-1991 ICOM-1992 Begin Add by b.jiang 2015-09-21
	public List<ScanResult> getWifiListWithStartString() {
		WifiManager wifiManager = (WifiManager) (GlobalInfo.getInstance().getCurrentApp().getSystemService(Context.WIFI_SERVICE));
		wifiManager.startScan();
		List<ScanResult> wifiList = wifiManager.getScanResults();
		return wifiListSortWithStartString(wifiList, GlobalInfo.WIFILIST_SORT_WITH_STARTSTRING);
	}
	//JIRA ICOM-1991 ICOM-1992 End Add by b.jiang 2015-09-21

	public static List<ScanResult> getWifiList(Context context) {
		WifiManager wifiManager = (WifiManager) (context.getSystemService(Context.WIFI_SERVICE));
		List<ScanResult> wifiList = wifiManager.getScanResults();
		return wifiList;
	}

	public ScanResult hasThisWifi(String ssid) {
		List<ScanResult> retList = getWifiList();
		for (ScanResult temp : retList) {
			if (temp.SSID.equals(ssid) == true) {
				return temp;
			}
		}
		return null;
	}

	//JIRA ICOM-1991 ICOM-1992 Begin Add by b.jiang 2015-09-21
	public List<ScanResult> wifiListSortWithStartString(List<ScanResult> wifiList, String startString) {
		List<ScanResult> tempWifiList1 = new ArrayList<ScanResult>();
		List<ScanResult> tempWifiList2 = new ArrayList<ScanResult>();
		int i;
		String tempSsid;
		for (i = 0; i < wifiList.size(); i++) {
			WriteLogToDevice.writeLog("[Normal] -- WifiCheck: ", "wifi Signal level ssid" + wifiList.get(i).SSID + "=" + wifiList.get(i).level);
		}
		// 筛选出以字符串startString开头的ssid;
		for (i = 0; i < wifiList.size(); i++) {
			tempSsid = wifiList.get(i).SSID;
			if (tempSsid.startsWith(startString)) {
				tempWifiList1.add(wifiList.get(i));
			} else {
				tempWifiList2.add(wifiList.get(i));
			}
		}
		// 将ssid按信号强弱排序，信号强的排在前面;
		if (tempWifiList1.size() > 0) {
			wifiListSortWithSignalLevel(tempWifiList1);
		}
		if (tempWifiList2.size() > 0) {
			wifiListSortWithSignalLevel(tempWifiList2);
		}

		for (i = 0; i < tempWifiList2.size(); i++) {
			tempWifiList1.add(tempWifiList2.get(i));
		}

		for (i = 0; i < tempWifiList1.size(); i++) {
			WriteLogToDevice.writeLog("[Normal] -- WifiCheck: ", "sort ago wifi Signal level ssid" + tempWifiList1.get(i).SSID + "="
					+ tempWifiList1.get(i).level);
		}

		return tempWifiList1;
	}

	public List<ScanResult> wifiListSortWithSignalLevel(List<ScanResult> wifiList) {
		for (int i = 0; i < wifiList.size() - 1; i++) {
			for (int j = 0; j < wifiList.size() - i - 1; j++) {
				if (wifiList.get(j).level < wifiList.get(j + 1).level) {
					ScanResult temp = wifiList.get(j);
					wifiList.set(j, wifiList.get(j + 1));
					wifiList.set(j + 1, temp);
				}
			}
		}
		return wifiList;

	}
	//JIRA ICOM-1991 ICOM-1992 End Add by b.jiang 2015-09-21
}
