/**
 * Added by zhangyanhu C01012,2014-9-18
 */
package com.vxfly.helioscamera.function;

import android.os.Handler;

import com.vxfly.helioscamera.ExtendComponent.ZoomBar;
import com.vxfly.helioscamera.SDKAPI.CameraAction;
import com.vxfly.helioscamera.SDKAPI.CameraProperties;
import com.vxfly.helioscamera.global.App.GlobalInfo;

/**
 * Added by zhangyanhu C01012,2014-9-18
 */
public class ZoomThread extends Thread {
	private Handler handler;
	private int lastZoomRate;
	private ZoomBar zoomBar;
	private int zoomMinRate = 10;

	public ZoomThread(Handler handler, int lastZoomRate, ZoomBar zoomBar) {
		this.handler = handler;
		this.lastZoomRate = lastZoomRate;
		this.zoomBar = zoomBar;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		// TODO Auto-generated method stub
		int maxZoomCount = 100;
		while (lastZoomRate > (zoomBar.getZoomProgress()) && lastZoomRate > zoomMinRate && maxZoomCount-- > 0) {
			CameraAction.getInstance().zoomOut();
			lastZoomRate = CameraProperties.getInstance().getCurrentZoomRatio();
		}

		while (lastZoomRate < (zoomBar.getZoomProgress()) && lastZoomRate < CameraProperties.getInstance().getMaxZoomRatio() && maxZoomCount-- > 0) {
			CameraAction.getInstance().zoomIn();
			lastZoomRate = CameraProperties.getInstance().getCurrentZoomRatio();
		}
		handler.obtainMessage(GlobalInfo.MESSAGE_ZOOM_COMPLETED, lastZoomRate, 0).sendToTarget();
	}

}
