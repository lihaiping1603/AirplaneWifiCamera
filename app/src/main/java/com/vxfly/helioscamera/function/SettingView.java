/**
 * Added by zhangyanhu C01012,2014-6-25
 */
package com.vxfly.helioscamera.function;

import java.lang.reflect.Field;
import java.util.List;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.vxfly.helioscamera.SDKAPI.CameraProperties;
import com.vxfly.helioscamera.adapter.OptionListAdapter;
import com.vxfly.helioscamera.baseItems.TimeLapseMode;
import com.vxfly.helioscamera.camera.MyCamera;
import com.vxfly.helioscamera.camera.UIDisplaySource;
import com.vxfly.helioscamera.global.App.AppProperties;
import com.vxfly.helioscamera.global.App.ExitApp;
import com.vxfly.helioscamera.global.App.GlobalInfo;
import com.vxfly.helioscamera.global.App.SettingMenu;
import com.vxfly.helioscamera.global.sdk.SDKEvent;
import com.vxfly.helioscamera.log.WriteLogToDevice;
import com.vxfly.helioscamera.R;
import com.icatch.wificam.customer.type.ICatchEventID;

/**
 * Added by zhangyanhu C01012,2014-6-25
 */
public class SettingView {
	public static final int SETTING_OPTION_BURST = 0;
	public static final int SETTING_OPTION_WHITE_BALANCE = 1;
	public static final int SETTING_OPTION_ELETRICITY_FREQUENCY = 2;
	public static final int SETTING_OPTION_DATE_STAMP = 3;
	public static final int SETTING_OPTION_IMAGE_SIZE = 4;
	public static final int SETTING_OPTION_VIDEO_SIZE = 5;
	public static final int SETTING_OPTION_FORAMT = 6;
	public static final int SETTING_OPTION_CAMERA_AWAKE = 7;
	public static final int SETTING_OPTION_CAMERA_SLEEP = 8;
	public static final int SETTING_OPTION_DELAY_TIME = 9;
	public static final int SETTING_OPTION_TIME_LAPSE_INTERVAL = 11;
	public static final int SETTING_OPTION_TIME_LAPSE_DURATION = 12;
	public static final int SETTING_OPTION_TIME_LAPSE_MODE = 13;
	public static final int SETTING_OPTION_SLOW_MOTION = 14;
	public static final int SETTING_OPTION_UPSIDE = 15;
	public static final int SETTING_OPTION_CAMERA_CONFIGURATION = 16;
	public static final int SETTING_OPTION_AUTO_DOWNLOAD = 17;
	public static final int SETTING_OPTION_AUTO_DOWNLOAD_SIZE = 18;
	public static final int SETTING_OPTION_CAMERA_UPDATE_FW = 19;
	public static final int SETTING_OPTION_CAMERA_CHANGE_RESOLUTION = 21;

	public static final int CAPTURE_SETTING_MENU = 1;
	public static final int VIDEO_SETTING_MENU = 2;
	public static final int TIMELAPSE_SETTING_MENU = 3;

	public static final int REQUEST_FORMAT_SD_CARD = 1;

	private int type;
	private ListView settingView;
	private Context context;
	private SettingHandler settingHandler = new SettingHandler();
	private OptionListAdapter optionListAdapter = null;
	private AlertDialog optionDialog = null;
	private Resources res = null;
	private Handler previewHandler;
	private AlertDialog dialog;
	private MyHander handler = new MyHander();
	private SDKEvent sdkEvent;
	private UpdateFW updateFWTheard;
	ProgressDialog progressDialog;
	private List<SettingMenu> settingMenuList;
	private MyCamera currCamera;
	private int reconnectTime = 5;

	public SettingView(Context context, ListView settingView, int type, Handler previewHandler, MyCamera currCamera) {
		this.settingView = settingView;
		this.context = context;
		this.type = type;
		this.previewHandler = previewHandler;
		this.currCamera = currCamera;
		res = context.getResources();
		this.sdkEvent = new SDKEvent(handler);
		sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_FW_UPDATE_COMPLETED);
		sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_FW_UPDATE_POWEROFF);
	}

	public SettingView(Context context, Handler previewHandler, MyCamera currCamera) {
		this.context = context;
		this.previewHandler = previewHandler;
		this.currCamera = currCamera;
		res = context.getResources();
		this.sdkEvent = new SDKEvent(handler);
		sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_FW_UPDATE_COMPLETED);
		sdkEvent.addEventListener(ICatchEventID.ICH_EVENT_FW_UPDATE_POWEROFF);
	}

	private class MyHander extends Handler {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case GlobalInfo.MESSAGE_FORMAT_SD_START:
				progressDialog = new ProgressDialog(context);
				progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				progressDialog.setMessage(context.getString(R.string.setting_format));
				progressDialog.setCancelable(false);
				progressDialog.show();
				break;
			case GlobalInfo.MESSAGE_FORMAT_SUCCESS:
				if (progressDialog != null) {
					progressDialog.dismiss();
				}
				Toast.makeText(context, R.string.setting_formatted, Toast.LENGTH_SHORT).show();
				break;
			case GlobalInfo.MESSAGE_FORMAT_FAILED:
				if (progressDialog != null) {
					progressDialog.dismiss();
				}
				Toast.makeText(context, R.string.dialog_failed, Toast.LENGTH_SHORT).show();
				break;
			case GlobalInfo.MESSAGE_UPDATE_FW_FAILED:
				WriteLogToDevice.writeLog("[Normal] -- SettingView: ", "receive MESSAGE_UPDATE_FW_FAILED");
				if (progressDialog != null) {
					progressDialog.dismiss();
				}
				AlertDialog.Builder updateFWFailedBuilder = new AlertDialog.Builder(context);
				updateFWFailedBuilder.setMessage(R.string.setting_updatefw_failedInfo);
				updateFWFailedBuilder.setNegativeButton(R.string.dialog_btn_exit, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						Log.d("1111", "update FW has failed,App quit");
						ExitApp.getInstance().exit();
					}
				});
				dialog = updateFWFailedBuilder.create();
				dialog.setCancelable(false);
				dialog.show();
				break;
			case GlobalInfo.EVENT_FW_UPDATE_COMPLETED:
				WriteLogToDevice.writeLog("[Normal] -- SettingView: ", "receive EVENT_FW_UPDATE_COMPLETED");
				if (progressDialog != null) {
					progressDialog.dismiss();
				}
				AlertDialog.Builder builder2 = new AlertDialog.Builder(context);
				builder2.setMessage(R.string.setting_updatefw_closeAppInfo);
				builder2.setNegativeButton(R.string.ok, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						Log.d("1111", "update FW completed!");
					}
				});
				dialog = builder2.create();
				dialog.setCancelable(false);
				dialog.show();
				break;
			case GlobalInfo.EVENT_FW_UPDATE_POWEROFF:
				WriteLogToDevice.writeLog("[Normal] -- SettingView: ", "receive EVENT_FW_UPDATE_POWEROFF");
				sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_FW_UPDATE_COMPLETED);
				sdkEvent.delEventListener(ICatchEventID.ICH_EVENT_FW_UPDATE_POWEROFF);
				AlertDialog.Builder builder3 = new AlertDialog.Builder(context);
				builder3.setMessage(R.string.setting_updatefw_closeAppInfo);
				builder3.setNegativeButton(R.string.dialog_btn_exit, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						Log.d("1111", "App quit");
						ExitApp.getInstance().exit();
					}
				});
				dialog = builder3.create();
				dialog.setCancelable(false);
				dialog.show();
				break;

			}
		}
	}

	public void showSettingMainMenu() {
		switch (type) {
		case CAPTURE_SETTING_MENU:
			settingMenuList = UIDisplaySource.getinstance().getList(UIDisplaySource.CAPTURE_SETTING_MENU, currCamera);
			break;
		case VIDEO_SETTING_MENU:
			settingMenuList = UIDisplaySource.getinstance().getList(UIDisplaySource.VIDEO_SETTING_MENU, currCamera);
			break;
		case TIMELAPSE_SETTING_MENU:
			settingMenuList = UIDisplaySource.getinstance().getList(UIDisplaySource.TIMELAPSE_SETTING_MENU, currCamera);
			break;

		}
		optionListAdapter = new OptionListAdapter(context, settingHandler, settingMenuList);
		if (optionListAdapter != null) {
			settingView.setAdapter(optionListAdapter);
		}
		settingView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				// TODO Auto-generated method stub
				switch (settingMenuList.get(arg2).name) {
				case R.string.setting_burst:
					settingHandler.obtainMessage(SETTING_OPTION_BURST).sendToTarget();
					break;
				case R.string.setting_awb:
					settingHandler.obtainMessage(SETTING_OPTION_WHITE_BALANCE).sendToTarget();
					break;
				case R.string.setting_power_supply:
					settingHandler.obtainMessage(SETTING_OPTION_ELETRICITY_FREQUENCY).sendToTarget();
					break;
				case R.string.setting_datestamp:
					settingHandler.obtainMessage(SETTING_OPTION_DATE_STAMP).sendToTarget();
					break;
				case R.string.setting_format:
					settingHandler.obtainMessage(SETTING_OPTION_FORAMT).sendToTarget();
					break;
				case R.string.setting_time_lapse_interval:
					settingHandler.obtainMessage(SETTING_OPTION_TIME_LAPSE_INTERVAL).sendToTarget();
					break;
				case R.string.setting_time_lapse_duration:
					settingHandler.obtainMessage(SETTING_OPTION_TIME_LAPSE_DURATION).sendToTarget();
					break;
				case R.string.timeLapse_mode:
					settingHandler.obtainMessage(SETTING_OPTION_TIME_LAPSE_MODE).sendToTarget();
					break;
				case R.string.slowmotion:
					settingHandler.obtainMessage(SETTING_OPTION_SLOW_MOTION).sendToTarget();
					break;
				case R.string.upside:
					settingHandler.obtainMessage(SETTING_OPTION_UPSIDE).sendToTarget();
					break;
				case R.string.camera_wifi_configuration:
					settingHandler.obtainMessage(SETTING_OPTION_CAMERA_CONFIGURATION).sendToTarget();
					break;
				case R.string.setting_update_fw:
					settingHandler.obtainMessage(SETTING_OPTION_CAMERA_UPDATE_FW).sendToTarget();
					break;
				}

			}
		});
	}

	public class SettingHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub

			// super.handleMessage(msg);
			switch (msg.what) {
			case SETTING_OPTION_AUTO_DOWNLOAD:
				WriteLogToDevice.writeLog("[Normal] -- SettingView: ", "receive SETTING_OPTION_AUTO_DOWNLOAD");
				Boolean switcher = (Boolean) msg.obj;
				if (switcher == true) {
					// AutoDownLoad
					GlobalInfo.autoDownloadAllow = true;
				} else {
					GlobalInfo.autoDownloadAllow = false;
				}
				break;
			case SETTING_OPTION_AUTO_DOWNLOAD_SIZE:
				WriteLogToDevice.writeLog("[Normal] -- SettingView: ", "receive SETTING_OPTION_AUTO_DOWNLOAD_SIZE");
				String size = (String) msg.obj;
				float sizeLimit = Float.parseFloat(size);
				GlobalInfo.autoDownloadSizeLimit = sizeLimit;
				break;
			default:
				showSettingDialog(msg.what);
				break;
			}
		}

	}

	public void showSettingDialog(int type) {
		WriteLogToDevice.writeLog("[Normal] -- SettingView: ", "showSettingDialog type=" + type);
		switch (type) {
		case SETTING_OPTION_BURST:
			showBurstOptionDialog();
			break;
		case SETTING_OPTION_WHITE_BALANCE:
			Log.d("1111", "showWhiteBalanceOptionDialog =");
			showWhiteBalanceOptionDialog();
			break;
		case SETTING_OPTION_ELETRICITY_FREQUENCY:
			showElectricityFrequencyOptionDialog();
			break;
		case SETTING_OPTION_DATE_STAMP:
			showDateStampOptionDialog();
			break;
		case SETTING_OPTION_IMAGE_SIZE:
			showImageSizeOptionDialog();
			break;
		case SETTING_OPTION_VIDEO_SIZE:
			showVideoSizeOptionDialog();
			break;
		case SETTING_OPTION_DELAY_TIME:
			showDelayTimeOptionDialog();
			break;
		case SETTING_OPTION_FORAMT:
			if (CameraProperties.getInstance().isSDCardExist() == false) {
				sdCardIsNotReadyAlert();
				break;
			}
			showFormatConfirmDialog();
			break;
		case SETTING_OPTION_TIME_LAPSE_INTERVAL:
			showTimeLapseIntervalDialog();
			break;
		case SETTING_OPTION_TIME_LAPSE_DURATION:
			showTimeLapseDurationDialog();
			break;
		case SETTING_OPTION_TIME_LAPSE_MODE:
			showTimeLapseModeDialog();
			break;
		case SETTING_OPTION_SLOW_MOTION:
			showSlowMotionDialog();
			break;
		case SETTING_OPTION_UPSIDE:
			showUpsideDialog();
			break;
		case SETTING_OPTION_CAMERA_CONFIGURATION:
			showCameraConfigurationDialog();
			break;
		case SETTING_OPTION_CAMERA_UPDATE_FW:
			if (CameraProperties.getInstance().isSDCardExist() == false) {
				sdCardIsNotReadyAlert();
				break;
			}
			showUpdateFWDialog();
			break;

		}
	}

	private void showUpdateFWDialog() {
		WriteLogToDevice.writeLog("[Normal] -- SettingView: ", "showUpdateFWDialog");
		// TODO Auto-generated method stub
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(R.string.setting_updateFW_prompt);
		builder.setNegativeButton(R.string.setting_no, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
		});
		builder.setPositiveButton(R.string.setting_yes, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				progressDialog = new ProgressDialog(context);
				progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				progressDialog.setMessage(context.getString(R.string.setting_updatefw_start));
				progressDialog.setTitle(R.string.setting_updatefw_title);
				progressDialog.setCancelable(false);
				progressDialog.show();

				updateFWTheard = new UpdateFW(handler);
				updateFWTheard.start();
			}
		});
		dialog = builder.create();
		dialog.show();
	}

	/**
	 * Added by zhangyanhu C01012,2014-11-11
	 */
	private void showCameraConfigurationDialog() {
		// TODO Auto-generated method stub

		LayoutInflater factory = LayoutInflater.from(context);
		View textEntryView = factory.inflate(R.layout.camera_name_password_set, null);
		final EditText cameraName = (EditText) textEntryView.findViewById(R.id.camera_name);
		final String name = CameraProperties.getInstance().getCameraSsid();
		cameraName.setText(name);
		final EditText cameraPassword = (EditText) textEntryView.findViewById(R.id.wifi_password);
		final String password = CameraProperties.getInstance().getCameraPassword();
		cameraPassword.setText(password);
		AlertDialog.Builder ad1 = new AlertDialog.Builder(context);
		ad1.setTitle(R.string.camera_wifi_configuration);
		ad1.setIcon(android.R.drawable.ic_dialog_info);
		ad1.setView(textEntryView);
		ad1.setCancelable(true);

		ad1.setOnKeyListener(new DialogInterface.OnKeyListener() {

			@Override
			public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub
				if (keyCode == KeyEvent.KEYCODE_BACK) {
					Log.d("1111", "KeyEvent.KEYCODE_BACK");
					try {
						Field field = dialog.getClass().getSuperclass().getDeclaredField("mShowing");
						field.setAccessible(true);
						field.set(dialog, true);

					} catch (Exception e) {
						e.printStackTrace();
					}
					dialog.dismiss();
				}
				return true;
			}
		});

		ad1.setPositiveButton(R.string.camera_configuration_set, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int arg1) {

				String temp1 = cameraName.getText().toString();
				if (temp1.length() > 20) {
					Toast.makeText(context, R.string.camera_name_limit, Toast.LENGTH_LONG).show();
					// do not allow dialog close
					try {
						Field field = dialog.getClass().getSuperclass().getDeclaredField("mShowing");
						field.setAccessible(true);
						field.set(dialog, false);

					} catch (Exception e) {
						e.printStackTrace();
					}
					return;
				}
				String temp = cameraPassword.getText().toString();
				if (temp.length() > 10 || temp.length() < 8) {
					Toast.makeText(context, R.string.password_limit, Toast.LENGTH_LONG).show();
					// do not allow dialog close
					try {
						Field field = dialog.getClass().getSuperclass().getDeclaredField("mShowing");
						field.setAccessible(true);
						field.set(dialog, false);

					} catch (Exception e) {
						e.printStackTrace();
					}
					return;
				}

				// allow dialog close
				try {
					Field field = dialog.getClass().getSuperclass().getDeclaredField("mShowing");
					field.setAccessible(true);
					field.set(dialog, true);
				} catch (Exception e) {
					e.printStackTrace();

				}

				if (name.equals(cameraName.getText().toString()) == false) {
					CameraProperties.getInstance().setCameraSsid(cameraName.getText().toString());
				}
				if (password.equals(temp) == false) {
					CameraProperties.getInstance().setCameraPassword(cameraPassword.getText().toString());
				}
				settingMenuList = UIDisplaySource.getinstance().getList(type, currCamera);
				// settingMenuList = UIDisplaySource.getinstance().getList(type,
				// currCamera);
				if (optionListAdapter == null) {
					return;
				}
				// optionListAdapter.notifyDataSetChanged();
				optionListAdapter.notifyDataSetInvalidated();
				optionListAdapter = new OptionListAdapter(context, settingHandler, settingMenuList);
				if (optionListAdapter != null) {
					settingView.setAdapter(optionListAdapter);
				}
			}
		});
		ad1.show();
	}

	/**
	 * Added by zhangyanhu C01012,2014-9-2
	 */
	private void showUpsideDialog() {
		// TODO Auto-generated method stub
		CharSequence title = res.getString(R.string.upside);
		// final MyCamera currCamera =
		// GlobalInfo.getInstance().getCurrentCamera();
		final String[] upsideUIString = currCamera.getUpside().getValueList();
		if (upsideUIString == null) {
			WriteLogToDevice.writeLog("[Error] -- SettingView: ", "upsideUIString == null");
			return;
		}
		int length = upsideUIString.length;
		int curIdx = 0;

		for (int i = 0; i < length; i++) {
			if (upsideUIString[i].equals(currCamera.getUpside().getCurrentUiStringInSetting())) {
				curIdx = i;
			}
		}

		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				currCamera.getUpside().setValueByPosition(arg1);
				previewHandler.obtainMessage(GlobalInfo.MESSAGE_UPDATE_UI_UPSIDE_DOWN).sendToTarget();
				arg0.dismiss();
				settingMenuList = UIDisplaySource.getinstance().getList(type, currCamera);
				if (optionListAdapter == null) {
					return;
				}
				optionListAdapter.notifyDataSetChanged();
				optionListAdapter = new OptionListAdapter(context, settingHandler, settingMenuList);
				if (optionListAdapter != null) {
					settingView.setAdapter(optionListAdapter);
				}
			}
		};
		showOptionDialog(title, upsideUIString, curIdx, listener, true);
	}

	/**
	 * Added by zhangyanhu C01012,2014-9-2
	 */
	private void showSlowMotionDialog() {
		// TODO Auto-generated method stub
		CharSequence title = res.getString(R.string.slowmotion);
		final String[] slowmotionUIString = currCamera.getSlowMotion().getValueList();
		if (slowmotionUIString == null) {
			WriteLogToDevice.writeLog("[Error] -- SettingView: ", "slowmotionUIString == null");
			return;
		}
		int length = slowmotionUIString.length;

		int curIdx = 0;

		for (int i = 0; i < length; i++) {
			if (slowmotionUIString[i].equals(currCamera.getSlowMotion().getCurrentUiStringInSetting())) {
				curIdx = i;
			}
		}

		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				currCamera.getSlowMotion().setValueByPosition(arg1);
				previewHandler.obtainMessage(GlobalInfo.MESSAGE_UPDATE_UI_SLOW_MOTION).sendToTarget();
				arg0.dismiss();
				settingMenuList = UIDisplaySource.getinstance().getList(type, currCamera);
				if (optionListAdapter == null) {
					return;
				}
				optionListAdapter.notifyDataSetChanged();
				optionListAdapter = new OptionListAdapter(context, settingHandler, settingMenuList);
				if (optionListAdapter != null) {
					settingView.setAdapter(optionListAdapter);
				}
			}
		};
		showOptionDialog(title, slowmotionUIString, curIdx, listener, true);
	}

	/**
	 * Added by zhangyanhu C01012,2014-8-27
	 */
	private void showTimeLapseModeDialog() {
		// TODO Auto-generated method stub
		CharSequence title = res.getString(R.string.timeLapse_mode);
		final String[] timeLapseModeString = currCamera.getTimeLapseMode().getValueList();
		if (timeLapseModeString == null) {
			WriteLogToDevice.writeLog("[Error] -- SettingView: ", "timeLapseModeString == null");
			return;
		}
		int length = timeLapseModeString.length;
		int curIdx = 0;

		for (int i = 0; i < length; i++) {
			if (timeLapseModeString[i].equals(currCamera.getTimeLapseMode().getCurrentUiStringInSetting())) {
				Log.d("tigertiger", "timeLapseModeString[i] =" + timeLapseModeString[i]);
				curIdx = i;
			}
		}

		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {

				AppProperties.getInstanse().setTimeLapseMode(arg1);
				arg0.dismiss();
				Log.d("tigertiger", "showTimeLapseModeDialog  timeLapseMode =" + arg1);
				if (arg1 == TimeLapseMode.TIME_LAPSE_MODE_VIDEO) {
					previewHandler.obtainMessage(GlobalInfo.MESSAGE_SETTING_TIMELAPSE_VIDEO_MODE).sendToTarget();
				} else if (arg1 == TimeLapseMode.TIME_LAPSE_MODE_STILL) {
					previewHandler.obtainMessage(GlobalInfo.MESSAGE_SETTING_TIMELAPSE_STILL_MODE).sendToTarget();
				}
				settingMenuList = UIDisplaySource.getinstance().getList(type, currCamera);
				if (optionListAdapter == null) {
					return;
				}
				optionListAdapter.notifyDataSetChanged();
				optionListAdapter = new OptionListAdapter(context, settingHandler, settingMenuList);
				if (optionListAdapter != null) {
					settingView.setAdapter(optionListAdapter);
				}
			}
		};
		showOptionDialog(title, timeLapseModeString, curIdx, listener, true);
	}

	/**
	 * Added by zhangyanhu C01012,2014-8-21
	 */
	private void showTimeLapseDurationDialog() {
		// TODO Auto-generated method stub
		CharSequence title = res.getString(R.string.setting_time_lapse_duration);
		final String[] videoTimeLapseDurationString = currCamera.gettimeLapseDuration().getValueStringList();
		if (videoTimeLapseDurationString == null) {
			WriteLogToDevice.writeLog("[Error] -- SettingView: ", "videoTimeLapseDurationString == null");
			return;
		}
		int length = videoTimeLapseDurationString.length;

		int curIdx = 0;
		String temp = currCamera.gettimeLapseDuration().getCurrentValue();
		for (int i = 0; i < length; i++) {
			if (videoTimeLapseDurationString[i].equals(temp)) {
				curIdx = i;
			}
		}

		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				currCamera.gettimeLapseDuration().setValueByPosition(arg1);
				arg0.dismiss();
				settingMenuList = UIDisplaySource.getinstance().getList(type, currCamera);
				if (optionListAdapter == null) {
					return;
				}
				optionListAdapter.notifyDataSetInvalidated();
				optionListAdapter = new OptionListAdapter(context, settingHandler, settingMenuList);
				if (optionListAdapter != null) {
					settingView.setAdapter(optionListAdapter);
				}
			}
		};
		showOptionDialog(title, videoTimeLapseDurationString, curIdx, listener, true);
	}

	/**
	 * Added by zhangyanhu C01012,2014-8-21
	 */
	private void showTimeLapseIntervalDialog() {
		// TODO Auto-generated method stub
		CharSequence title = res.getString(R.string.setting_time_lapse_interval);
		final String[] videoTimeLapseIntervalString = currCamera.getTimeLapseInterval().getValueStringList();
		if (videoTimeLapseIntervalString == null) {
			WriteLogToDevice.writeLog("[Error] -- SettingView: ", "videoTimeLapseIntervalString == null");
			return;
		}
		int length = videoTimeLapseIntervalString.length;

		int curIdx = 0;
		String temp = currCamera.getTimeLapseInterval().getCurrentValue();
		for (int i = 0; i < length; i++) {
			if (videoTimeLapseIntervalString[i].equals(temp)) {
				curIdx = i;
			}
		}

		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				int timeLapseMode = AppProperties.getInstanse().getTimeLapseMode();
				currCamera.getTimeLapseInterval().setValueByPosition(arg1);
				arg0.dismiss();
				// JIRA ICOM-1935 Start Add by b.jiang 2015-09-18
				if (timeLapseMode == TimeLapseMode.TIME_LAPSE_MODE_STILL) {
					previewHandler.obtainMessage(GlobalInfo.MESSAGE_SETTING_TIMELAPSE_STILL_MODE).sendToTarget();
				}
				// JIRA ICOM-1935 End Add by b.jiang 2015-09-18
				settingMenuList = UIDisplaySource.getinstance().getList(type, currCamera);
				if (optionListAdapter == null) {
					return;
				}
				optionListAdapter.notifyDataSetInvalidated();
				optionListAdapter = new OptionListAdapter(context, settingHandler, settingMenuList);
				if (optionListAdapter != null) {
					settingView.setAdapter(optionListAdapter);
				}
			}
		};
		showOptionDialog(title, videoTimeLapseIntervalString, curIdx, listener, true);
	}

	/**
	 * Added by zhangyanhu C01012,2014-8-5
	 */

	/**
	 * Added by zhangyanhu C01012,2014-6-26
	 */
	private void showDelayTimeOptionDialog() {
		// TODO Auto-generated method stub
		CharSequence title = res.getString(R.string.stream_set_timer);
		final String[] delayTimeUIString = currCamera.getCaptureDelay().getValueList();
		if (delayTimeUIString == null) {
			WriteLogToDevice.writeLog("[Error] -- SettingView: ", "delayTimeUIString == null");
			return;
		}
		int length = delayTimeUIString.length;
		int curIdx = 0;
		String temp = currCamera.getCaptureDelay().getCurrentUiStringInPreview();
		for (int i = 0; i < length; i++) {
			if (delayTimeUIString[i].equals(temp)) {
				curIdx = i;
			}
		}

		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				currCamera.getCaptureDelay().setValueByPosition(arg1);
				previewHandler.obtainMessage(GlobalInfo.MESSAGE_UPDATE_UI_CAPTURE_DELAY).sendToTarget();
				arg0.dismiss();
				settingMenuList = UIDisplaySource.getinstance().getList(type, currCamera);
				if (optionListAdapter == null) {
					return;
				}
				optionListAdapter.notifyDataSetInvalidated();
				optionListAdapter = new OptionListAdapter(context, settingHandler, settingMenuList);
				if (optionListAdapter != null) {
					settingView.setAdapter(optionListAdapter);
				}
			}
		};
		showOptionDialog(title, delayTimeUIString, curIdx, listener, true);
	}

	/**
	 * Added by zhangyanhu C01012,2014-6-25
	 */
	private void showVideoSizeOptionDialog() {
		// TODO Auto-generated method stub
		CharSequence title = res.getString(R.string.stream_set_res_vid);
		final String[] videoSizeUIString = currCamera.getVideoSize().getValueArrayString();
		final List<String> videoSizeValueString = currCamera.getVideoSize().getValueList();
		if (videoSizeUIString == null) {
			WriteLogToDevice.writeLog("[Error] -- SettingView: ", "videoSizeUIString == null");
			previewHandler.obtainMessage(GlobalInfo.MESSAGE_UPDATE_UI_VIDEO_SIZE).sendToTarget();
			return;
		}
		int length = videoSizeUIString.length;

		int curIdx = 0;
		for (int i = 0; i < length; i++) {
			if (videoSizeUIString[i].equals(currCamera.getVideoSize().getCurrentUiStringInSetting())) {
				curIdx = i;
			}
		}

		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				final String value = videoSizeValueString.get(arg1);
				arg0.dismiss();
				if (value.equals("2704x1524 15") || value.equals("3840x2160 10")) {
					AlertDialog.Builder builder = new AlertDialog.Builder(context);
					builder.setMessage(R.string.not_support_preview);
					builder.setNegativeButton(R.string.setting_no, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							previewHandler.obtainMessage(GlobalInfo.MESSAGE_UPDATE_UI_VIDEO_SIZE).sendToTarget();
						}
					});
					builder.setPositiveButton(R.string.setting_yes, new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss();
							// CameraProperties.getInstance().setVideoSize(value);
							currCamera.getVideoSize().setValue(value);
							previewHandler.obtainMessage(GlobalInfo.MESSAGE_UPDATE_UI_VIDEO_SIZE).sendToTarget();
							settingMenuList = UIDisplaySource.getinstance().getList(type, currCamera);
							if (optionListAdapter == null) {
								return;
							}
							optionListAdapter.notifyDataSetChanged();
						}
					});
					builder.create().show();
				} else {
					currCamera.getVideoSize().setValue(value);
					previewHandler.obtainMessage(GlobalInfo.MESSAGE_UPDATE_UI_VIDEO_SIZE).sendToTarget();
					settingMenuList = UIDisplaySource.getinstance().getList(type, currCamera);
					if (optionListAdapter == null) {
						return;
					}
					optionListAdapter.notifyDataSetChanged();
				}

			}
		};
		showOptionDialog(title, videoSizeUIString, curIdx, listener, false);
	}

	/**
	 * Added by zhangyanhu C01012,2014-6-25
	 */
	private void showImageSizeOptionDialog() {
		// TODO Auto-generated method stub
		CharSequence title = res.getString(R.string.stream_set_res_photo);

		final String[] imageSizeUIString = currCamera.getImageSize().getValueArrayString();
		final List<String> imageSizeValueString = currCamera.getVideoSize().getValueList();
		if (imageSizeUIString == null) {
			WriteLogToDevice.writeLog("[Error] -- SettingView: ", "imageSizeUIString == null");
			previewHandler.obtainMessage(GlobalInfo.MESSAGE_UPDATE_UI_IMAGE_SIZE).sendToTarget();
			return;
		}
		int length = imageSizeUIString.length;

		int curIdx = 0;

		for (int ii = 0; ii < length; ii++) {
			if (imageSizeUIString[ii].equals(currCamera.getImageSize().getCurrentUiStringInSetting())) {
				curIdx = ii;
			}
		}

		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				currCamera.getImageSize().setValueByPosition(arg1);
				previewHandler.obtainMessage(GlobalInfo.MESSAGE_UPDATE_UI_IMAGE_SIZE).sendToTarget();
				arg0.dismiss();
				settingMenuList = UIDisplaySource.getinstance().getList(type, currCamera);
				if (optionListAdapter == null) {
					return;
				}
				optionListAdapter.notifyDataSetChanged();
			}
		};
		showOptionDialog(title, imageSizeUIString, curIdx, listener, true);
	}

	/**
	 * Added by zhangyanhu C01012,2014-6-25
	 */
	private void showFormatConfirmDialog() {
		// TODO Auto-generated method stub
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(R.string.setting_format_desc);
		builder.setNegativeButton(R.string.setting_no, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
		});
		builder.setPositiveButton(R.string.setting_yes, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				// handler.obtainMessage(REQUEST_FORMAT_SD_CARD).sendToTarget();
				FormatSDCard formatSDCard = new FormatSDCard(handler);
				formatSDCard.start();
			}
		});
		dialog = builder.create();
		dialog.show();
	}

	/**
	 * Added by zhangyanhu C01012,2014-6-25
	 */
	private void showDateStampOptionDialog() {
		// TODO Auto-generated method stub
		CharSequence title = res.getString(R.string.setting_datestamp);
		final String[] dateStampUIString = currCamera.getDateStamp().getValueList();
		if (dateStampUIString == null) {
			WriteLogToDevice.writeLog("[Error] -- SettingView: ", "dateStampUIString == null");
			return;
		}
		int length = dateStampUIString.length;

		int curIdx = 0;
		for (int i = 0; i < length; i++) {
			if (dateStampUIString[i].equals(currCamera.getDateStamp().getCurrentUiStringInSetting())) {
				curIdx = i;
			}
		}

		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				currCamera.getDateStamp().setValueByPosition(arg1);
				arg0.dismiss();
				settingMenuList = UIDisplaySource.getinstance().getList(type, currCamera);
				if (optionListAdapter == null) {
					return;
				}
				// optionListAdapter.notifyDataSetChanged();
				optionListAdapter.notifyDataSetInvalidated();
				optionListAdapter = new OptionListAdapter(context, settingHandler, settingMenuList);
				if (optionListAdapter != null) {
					settingView.setAdapter(optionListAdapter);
				}
			}
		};
		showOptionDialog(title, dateStampUIString, curIdx, listener, true);
	}

	/**
	 * Added by zhangyanhu C01012,2014-6-25
	 */
	private void showElectricityFrequencyOptionDialog() {
		// TODO Auto-generated method stub
		CharSequence title = res.getString(R.string.setting_power_supply);

		final String[] eleFreUIString = currCamera.getElectricityFrequency().getValueList();
		if (eleFreUIString == null) {
			WriteLogToDevice.writeLog("[Error] -- SettingView: ", "eleFreUIString == null");
			return;
		}
		int length = eleFreUIString.length;

		int curIdx = 0;
		for (int i = 0; i < length; i++) {
			if (eleFreUIString[i].equals(currCamera.getElectricityFrequency().getCurrentUiStringInSetting())) {
				curIdx = i;
			}
		}

		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				currCamera.getElectricityFrequency().setValueByPosition(arg1);
				arg0.dismiss();
				settingMenuList = UIDisplaySource.getinstance().getList(type, currCamera);
				if (optionListAdapter == null) {
					return;
				}
				// optionListAdapter.notifyDataSetChanged();
				optionListAdapter.notifyDataSetInvalidated();
				optionListAdapter = new OptionListAdapter(context, settingHandler, settingMenuList);
				if (optionListAdapter != null) {
					settingView.setAdapter(optionListAdapter);
				}
			}
		};
		showOptionDialog(title, eleFreUIString, curIdx, listener, true);
	}

	/**
	 * Added by zhangyanhu C01012,2014-6-25
	 */
	private void showWhiteBalanceOptionDialog() {
		// TODO Auto-generated method stub
		CharSequence title = res.getString(R.string.setting_awb);
		final List<Integer> whiteBalanceUIString1 = CameraProperties.getInstance().getSupportedWhiteBalances();
		final String[] whiteBalanceUIString = currCamera.getWhiteBalance().getValueList();
		if (whiteBalanceUIString == null) {
			WriteLogToDevice.writeLog("[Error] -- SettingView: ", "whiteBalanceUIString == null");
			return;
		}
		int length = whiteBalanceUIString.length;

		int curIdx = 0;
		for (int i = 0; i < length; i++) {
			if (whiteBalanceUIString[i].equals(currCamera.getWhiteBalance().getCurrentUiStringInSetting())) {
				curIdx = i;
			}
		}

		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				currCamera.getWhiteBalance().setValueByPosition(arg1);

				previewHandler.obtainMessage(GlobalInfo.MESSAGE_UPDATE_UI_WHITE_BALANCE_ICON).sendToTarget();
				arg0.dismiss();
				settingMenuList = UIDisplaySource.getinstance().getList(type, currCamera);
				if (optionListAdapter == null) {
					return;
				}
				optionListAdapter.notifyDataSetInvalidated();
				optionListAdapter = new OptionListAdapter(context, settingHandler, settingMenuList);
				if (optionListAdapter != null) {
					settingView.setAdapter(optionListAdapter);
				}

			}
		};
		showOptionDialog(title, whiteBalanceUIString, curIdx, listener, true);
	}

	/**
	 * Added by zhangyanhu C01012,2014-6-25
	 */
	private void showBurstOptionDialog() {
		// TODO Auto-generated method stub
		CharSequence title = res.getString(R.string.setting_burst);

		final String[] burstUIString = currCamera.getBurst().getValueList();
		if (burstUIString == null) {
			WriteLogToDevice.writeLog("[Error] -- SettingView: ", "burstUIString == null");
			return;
		}
		int length = burstUIString.length;

		int curIdx = 0;
		for (int i = 0; i < length; i++) {
			if (burstUIString[i].equals(currCamera.getBurst().getCurrentUiStringInSetting())) {
				curIdx = i;
			}
		}

		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				currCamera.getBurst().setValueByPosition(arg1);
				previewHandler.obtainMessage(GlobalInfo.MESSAGE_UPDATE_UI_BURST_ICON).sendToTarget();
				arg0.dismiss();
				settingMenuList = UIDisplaySource.getinstance().getList(type, currCamera);
				if (optionListAdapter == null) {
					return;
				}
				optionListAdapter.notifyDataSetInvalidated();
				optionListAdapter = new OptionListAdapter(context, settingHandler, settingMenuList);
				if (optionListAdapter != null) {
					settingView.setAdapter(optionListAdapter);
				}
				Log.d("1111", "refresh optionListAdapter!");
			}
		};
		showOptionDialog(title, burstUIString, curIdx, listener, true);
	}

	public void sdCardIsNotReadyAlert() {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(R.string.dialog_no_sd);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		AlertDialog dialog = builder.create();
		dialog.setCancelable(true);
		dialog.show();
	}

	private void showOptionDialog(CharSequence title, CharSequence[] items, int checkedItem, DialogInterface.OnClickListener listener,
			boolean cancelable) {
		if (optionDialog == null || optionDialog.isShowing() == false) {
			optionDialog = new AlertDialog.Builder(context).setTitle(title).setSingleChoiceItems(items, checkedItem, listener).create();
			optionDialog.setCancelable(cancelable);
			optionDialog.show();
		}
	}
}
