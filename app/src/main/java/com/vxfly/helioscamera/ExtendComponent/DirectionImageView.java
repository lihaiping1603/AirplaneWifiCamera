package com.vxfly.helioscamera.ExtendComponent;

import com.vxfly.helioscamera.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;



public class DirectionImageView extends LinearLayout {
	 
    private ImageView leftArrow;
    private ImageView rightArrow;
    private ImageView upArrow;
    private ImageView downArrow;
    private ImageView resetPreview;
    public static int minDistance = 10;
     
    public DirectionImageView(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
    }
    
    public DirectionImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        LayoutInflater inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.directionview, this);
        leftArrow=(ImageView) findViewById(R.id.left_arrow);
        rightArrow=(ImageView) findViewById(R.id.right_arrow);
        upArrow=(ImageView) findViewById(R.id.up_arrow);
        downArrow=(ImageView) findViewById(R.id.down_arrow);
        resetPreview=(ImageView) findViewById(R.id.reset_preview);
    }
     
    public ImageView getLeftArrow(){
    	return leftArrow;
    }
    
    public ImageView getRightArrow(){
    	return rightArrow;
    }
    
    public ImageView getUpArrow(){
    	return upArrow;
    }
    
    public ImageView getDownArrow(){
    	return downArrow;
    }
    
    public ImageView getReset(){
    	return resetPreview;
    }
    
    public void setOnClickLeftArrow(OnClickListener onClickListener){
    	//onClickListener.onClick(arg0);
    	leftArrow.setOnClickListener(onClickListener);
    }

	public void setOnClickUpArrow(OnClickListener onClickListener) {
		// TODO Auto-generated method stub
		upArrow.setOnClickListener(onClickListener);
	}

	public void setOnClickRightArrow(OnClickListener onClickListener) {
		// TODO Auto-generated method stub
		rightArrow.setOnClickListener(onClickListener);
	}

	public void setOnClickDownArrow(OnClickListener onClickListener) {
		// TODO Auto-generated method stub
		downArrow.setOnClickListener(onClickListener);
	}
	
	public void setOnClickResetPreivew(OnClickListener onClickListener) {
		// TODO Auto-generated method stub
		resetPreview.setOnClickListener(onClickListener);
	}
}
