package com.vxfly.helioscamera.ExtendComponent;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.vxfly.helioscamera.R;

public class ShareDialog extends Dialog {
	/**
	 * Added by zhangyanhu C01012,2014-7-17
	 */
	private TextView title;
	private TextView message;
	private ImageView share;
	private Button quitShare;

	public ShareDialog(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.dialog_share_style);
		title = (TextView) findViewById(R.id.title);
		message = (TextView) findViewById(R.id.message);
		share = (ImageView) findViewById(R.id.udid_share);
		quitShare = (Button) findViewById(R.id.quit_share);
	}

	public void setTitle(String myTitle) {
		title.setText(myTitle);
	}

	public void setMessage(String myMessage) {
		message.setText(myMessage);
	}

	public ImageView getShareButton() {
		return share;
	}
	
	public Button getQuitButton() {
		return quitShare;
	}
}