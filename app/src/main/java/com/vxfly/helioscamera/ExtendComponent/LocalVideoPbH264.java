package com.vxfly.helioscamera.ExtendComponent;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.Queue;

import android.content.Context;
import android.graphics.Rect;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.MediaCodec;
import android.media.MediaFormat;
import android.media.MediaCodec.BufferInfo;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.RelativeLayout;

import com.vxfly.helioscamera.SDKAPI.VideoPlayback;
import com.vxfly.helioscamera.Tool.ScaleTool;
import com.vxfly.helioscamera.log.WriteLogToDevice;
import com.icatch.wificam.customer.ICatchWificamVideoPlayback;
import com.icatch.wificam.customer.exception.IchAudioStreamClosedException;
import com.icatch.wificam.customer.exception.IchBufferTooSmallException;
import com.icatch.wificam.customer.exception.IchCameraModeException;
import com.icatch.wificam.customer.exception.IchInvalidArgumentException;
import com.icatch.wificam.customer.exception.IchInvalidSessionException;
import com.icatch.wificam.customer.exception.IchPbStreamPausedException;
import com.icatch.wificam.customer.exception.IchSocketException;
import com.icatch.wificam.customer.exception.IchStreamNotRunningException;
import com.icatch.wificam.customer.exception.IchTryAgainException;
import com.icatch.wificam.customer.exception.IchVideoStreamClosedException;
import com.icatch.wificam.customer.type.ICatchAudioFormat;
import com.icatch.wificam.customer.type.ICatchFrameBuffer;
import com.icatch.wificam.customer.type.ICatchVideoFormat;

public class LocalVideoPbH264 extends SurfaceView implements SurfaceHolder.Callback {

	private AudioTrack audioTrack;
	private SurfaceHolder holder;
	private H264DecodeThread mySurfaceViewThread;
	private boolean hasSurface = false;
	private AudioThread audioThread;
	private boolean hasInit = false;
	private VideoPlayback videoPlayback = VideoPlayback.getInstance();
	private ICatchWificamVideoPlayback videoPb;
	private int BUFFER_LENGTH = 1280 * 720 * 4;
	private int timeout = 0;// us
	private View parent;
	private int myWidth;
	private int myHeight;
	private Handler handler;

	public final int ADJUST_LAYOUT_H264 = 1;
	public final int test_message = 2;
	Queue<ICatchFrameBuffer> audioQueue;
	boolean audioPlayFlag;
	boolean audioInfoFetchFlag;
	boolean audioFist = true;
	volatile boolean avSyncFlag = false;
	boolean videotestflag = false;
	private boolean adjustLayoutH264Complete = false;
	private LocalH264VideoPbUpdateBarLitener videoPbUpdateBarLitener;
	private boolean H264DecodeThreadDone = false;
	private int frmW;
	private int frmH;
	private MediaCodec decoder;

	public LocalVideoPbH264(Context context, AttributeSet attrs) {
		super(context, attrs);
		holder = this.getHolder();
		holder.addCallback(this);
		this.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {

			@Override
			public void onGlobalLayout() {
				// WriteLogToDevice.writeLog("[normal] -- LocalVideoPbH264: ",
				// "addOnGlobalLayoutListener");
				// TODO Auto-generated method stub
				if (H264DecodeThreadDone) {
					if (myWidth != parent.getWidth() || myHeight != parent.getHeight()) {
						WriteLogToDevice.writeLog("[normal] -- LocalVideoPbH264: ", "getWidth=" + getWidth());
						WriteLogToDevice.writeLog("[normal] -- LocalVideoPbH264: ", "getHeight=" + getHeight());
						myWidth = parent.getWidth();
						myHeight = parent.getHeight();
						setSurfaceViewArea(parent.getWidth(), parent.getHeight());
					}
				}
			}

		});
	}

	private void initH264() {
		audioQueue = new LinkedList<ICatchFrameBuffer>();
		audioPlayFlag = false;
		audioInfoFetchFlag = false;
		audioFist = true;
		avSyncFlag = false;
		videotestflag = false;

		hasInit = true;
		parent = (View) this.getParent();
		myWidth = 0;
		myHeight = 0;
		handler = new Handler() {
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case ADJUST_LAYOUT_H264:
					setSurfaceViewArea(parent.getWidth(), parent.getHeight());
					adjustLayoutH264Complete = true;
					WriteLogToDevice.writeLog("[Normal] -- LocalVideoPbH264: ", "end message ADJUST_LAYOUT_H264");
					break;
				}
			}
		};
	}

	public void start(ICatchWificamVideoPlayback videoPlayback) {
		WriteLogToDevice.writeLog("[Normal] -- LocalVideoPbH264: ", "start preview");
		this.videoPb = videoPlayback;
		this.videoPlayback.initVideoPlayback(videoPlayback);

		if (hasInit == false) {
			initH264();
		}
		// 创建和启动图像更新线程
		if (mySurfaceViewThread == null) {
			// mySurfaceViewThread = new VideoThread();
			mySurfaceViewThread = new H264DecodeThread();
			if (hasSurface == true) {
				setFormat();
				mySurfaceViewThread.start();
			}
		}
		// 启动音频线程
		// JIRA ICOM-1844 Start Add by b.jiang 2015-08-14
		if (audioThread == null) {
			audioThread = new AudioThread();
			audioThread.start();
		}
		// JIRA ICOM-1844 End Add by b.jiang 2015-08-14
	}

	public boolean stop() {
		// 杀死图像更新线程
		WriteLogToDevice.writeLog("[Normal] -- LocalVideoPbH264: ", "stop preview");
		if (mySurfaceViewThread != null) {
			mySurfaceViewThread.requestExitAndWait();
			mySurfaceViewThread = null;
		}
		// 杀死音频线程
		if (audioThread != null) {
			audioThread.requestExitAndWait();
			audioThread = null;
		}
		hasInit = false;
		return true;
	}

	private class AudioThread extends Thread {
		private boolean done = false;

		public void run() {
			WriteLogToDevice.writeLog("[normal] -- LocalVideoPbH264: ", "start running AudioThread");
			ICatchFrameBuffer temp = null;

			ICatchAudioFormat audioFormat = videoPlayback.getAudioFormat();
			int bufferSize = AudioTrack.getMinBufferSize(audioFormat.getFrequency(), audioFormat.getNChannels() == 2 ? AudioFormat.CHANNEL_IN_STEREO
					: AudioFormat.CHANNEL_IN_LEFT, audioFormat.getSampleBits() == 16 ? AudioFormat.ENCODING_PCM_16BIT : AudioFormat.ENCODING_PCM_8BIT);

			audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, audioFormat.getFrequency(), audioFormat.getNChannels() == 2 ? AudioFormat.CHANNEL_IN_STEREO
					: AudioFormat.CHANNEL_IN_LEFT, audioFormat.getSampleBits() == 16 ? AudioFormat.ENCODING_PCM_16BIT : AudioFormat.ENCODING_PCM_8BIT,
					bufferSize, AudioTrack.MODE_STREAM);

			audioTrack.play();

			boolean ret = false;
			ICatchFrameBuffer tempBuffer = new ICatchFrameBuffer(1024 * 50);
			byte[] testaudioBuffer = new byte[1024 * 50];
			tempBuffer.setBuffer(testaudioBuffer);
			while (!done) {
				ICatchFrameBuffer icatchBuffer = new ICatchFrameBuffer(1024 * 50);
				byte[] audioBuffer = new byte[1024 * 50];
				icatchBuffer.setBuffer(audioBuffer);
				ret = false;
				try {
					ret = videoPb.getNextAudioFrame(icatchBuffer);
				} catch (IchSocketException e) {
					WriteLogToDevice.writeLog("[Error] -- LocalVideoPbH264: ", " getNextAudioFrame IchSocketException");
					// TODO Auto-generated catch block
					e.printStackTrace();
					return;
				} catch (IchBufferTooSmallException e) {
					WriteLogToDevice.writeLog("[Error] -- LocalVideoPbH264: ", "getNextAudioFrame IchBufferTooSmallException");
					// TODO Auto-generated catch block
					e.printStackTrace();
					return;
				} catch (IchCameraModeException e) {
					WriteLogToDevice.writeLog("[Error] -- LocalVideoPbH264: ", "getNextAudioFrame IchCameraModeException");
					// TODO Auto-generated catch block
					e.printStackTrace();
					return;
				} catch (IchInvalidSessionException e) {
					WriteLogToDevice.writeLog("[Error] -- LocalVideoPbH264: ", "getNextAudioFrame IchInvalidSessionException");
					// TODO Auto-generated catch block
					e.printStackTrace();
					return;
				} catch (IchTryAgainException e) {
					// WriteLogToDevice.writeLog("[Error] -- LocalVideoPbH264: ",
					// "getNextAudioFrame IchTryAgainException");
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IchStreamNotRunningException e) {
					WriteLogToDevice.writeLog("[Error] -- LocalVideoPbH264: ", "getNextAudioFrame IchStreamNotRunningException");
					// TODO Auto-generated catch block
					e.printStackTrace();
					return;
				} catch (IchInvalidArgumentException e) {
					WriteLogToDevice.writeLog("[Error] -- LocalVideoPbH264: ", "getNextAudioFrame IchInvalidArgumentException");
					// TODO Auto-generated catch block
					e.printStackTrace();
					return;
				} catch (IchAudioStreamClosedException e) {
					WriteLogToDevice.writeLog("[Error] -- LocalVideoPbH264: ", "getNextAudioFrame IchAudioStreamClosedException");
					// TODO Auto-generated catch block
					e.printStackTrace();
					return;

				} catch (IchPbStreamPausedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (false == ret) {
					// WriteLogToDevice.writeLog("[Error] -- LocalVideoPbH264: ",
					// "failed to getNextAudioFrame!");
					continue;
				} else {
					if (audioQueue.size() > 100) {
						audioQueue.poll();
					}
					audioQueue.offer(icatchBuffer);
				}
				if (audioPlayFlag) {
					temp = audioQueue.poll();
					audioTrack.write(temp.getBuffer(), 0, temp.getFrameSize());
				}
			}
			audioTrack.stop();
			audioTrack.release();
			WriteLogToDevice.writeLog("[Normal] -- LocalVideoPbH264: ", "stop audio thread");
		}

		public void requestExitAndWait() {
			// 把这个线程标记为完成，并合并到主程序线程
			done = true;
			try {
				join();
			} catch (InterruptedException ex) {
			}
		}
	}

	private class H264DecodeThread extends Thread {
		private BufferInfo info;

		H264DecodeThread() {
			super();
			H264DecodeThreadDone = false;
		}

		@Override
		public void run() {
			adjustLayoutH264Complete = false;
			WriteLogToDevice.writeLog("[Normal] -- LocalVideoPbH264: ", "h264 run for gettting surface image");
			ByteBuffer[] inputBuffers = decoder.getInputBuffers();
			info = new BufferInfo();

			byte[] mPixel = new byte[BUFFER_LENGTH];
			ICatchFrameBuffer frameBuffer = new ICatchFrameBuffer();
			frameBuffer.setBuffer(mPixel);
			int inIndex = -1;
			int sampleSize = 0;
			long pts = 0;
			boolean retvalue = true;
			int count = 0;
			long starttime = 0;
			WriteLogToDevice.writeLog("[normal] -- LocalVideoPbH264: ", "create  MediaFormat ----2");
			while (!H264DecodeThreadDone) {
				// 添加变量adjustLayoutH264Complete,使设定宽高才能去显示Preview;
				if (myWidth != parent.getWidth() || myHeight != parent.getHeight()) {
					WriteLogToDevice.writeLog("[normal] -- LocalVideoPbH264: ", "getWidth=" + getWidth());
					WriteLogToDevice.writeLog("[normal] -- LocalVideoPbH264: ", "getHeight=" + getHeight());
					if (parent.getWidth() > 0 && parent.getHeight() > 0) {
						adjustLayoutH264Complete = false;
						myWidth = parent.getWidth();
						myHeight = parent.getHeight();
						handler.obtainMessage(ADJUST_LAYOUT_H264).sendToTarget();
					}

				}
				// JIRA ICOM-1723 Start add by b.jiang 20150821
				if (!adjustLayoutH264Complete) {
					continue;
				}
				// JIRA ICOM-1723 End add by b.jiang 20150821

				retvalue = false;
				try {
					retvalue = videoPb.getNextVideoFrame(frameBuffer);
					if (!retvalue) {
						WriteLogToDevice.writeLog("[Error] -- LocalVideoPbH264: ", "h264 could not get buffer");
						continue;
					}

				} catch (IchTryAgainException ex) {
					// WriteLogToDevice.writeLog("[Error] -- LocalVideoPbH264: ",
					// "getNextVideoFrame IchTryAgainException");
					ex.printStackTrace();

					retvalue = false;
					continue;
				} catch (IchSocketException e) {
					// TODO Auto-generated catch block
					WriteLogToDevice.writeLog("[Error] -- LocalVideoPbH264: ", "getNextVideoFrame IchSocketException :");
					e.printStackTrace();
					retvalue = false;
					break;
				} catch (IchBufferTooSmallException e) {
					// TODO Auto-generated catch block
					WriteLogToDevice.writeLog("[Error] -- LocalVideoPbH264: ", "getNextVideoFrame IchBufferTooSmallException :");
					e.printStackTrace();
					retvalue = false;
					break;
				} catch (IchCameraModeException e) {
					// TODO Auto-generated catch block
					WriteLogToDevice.writeLog("[Error] -- LocalVideoPbH264: ", "getNextVideoFrame IchCameraModeException :");
					e.printStackTrace();
					retvalue = false;
					break;
				} catch (IchInvalidSessionException e) {
					// TODO Auto-generated catch block
					WriteLogToDevice.writeLog("[Error] -- LocalVideoPbH264: ", "getNextVideoFrame IchInvalidSessionException :");
					e.printStackTrace();
					retvalue = false;
					break;
				} catch (IchStreamNotRunningException e) {
					// TODO Auto-generated catch block
					WriteLogToDevice.writeLog("[Error] -- LocalVideoPbH264: ", "getNextVideoFrame IchStreamNotRunningException :");
					e.printStackTrace();
					retvalue = false;
					break;
				} catch (IchInvalidArgumentException e) {
					// TODO Auto-generated catch block
					WriteLogToDevice.writeLog("[Error] -- LocalVideoPbH264: ", "getNextVideoFrame IchInvalidArgumentException :");
					e.printStackTrace();
					retvalue = false;
					break;
				} catch (IchVideoStreamClosedException e) {
					// TODO Auto-generated catch block
					WriteLogToDevice.writeLog("[Error] -- LocalVideoPbH264: ", "getNextVideoFrame IchVideoStreamClosedException :");
					e.printStackTrace();
					retvalue = false;
					break;
				} catch (IchPbStreamPausedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (frameBuffer.getFrameSize() <= 0 || frameBuffer == null) {
					WriteLogToDevice.writeLog("tigertiger", "frameBuffer.getFrameSize() <=0");
					retvalue = false;
					continue;
				}

				inIndex = -1;
				inIndex = decoder.dequeueInputBuffer(timeout);
				if (inIndex >= 0) {

					sampleSize = frameBuffer.getFrameSize();
					pts = (long) (frameBuffer.getPresentationTime() * 1000 * 1000); // (seconds
					// us)
					ByteBuffer buffer = inputBuffers[inIndex];
					buffer.clear();
					buffer.rewind();

					buffer.put(frameBuffer.getBuffer(), 0, sampleSize);
					decoder.queueInputBuffer(inIndex, 0, sampleSize, pts, 0);
					dequeueAndRenderOutputBuffer(timeout);
				} else {
					if (!dequeueAndRenderOutputBuffer(timeout) && !dequeueAndRenderOutputBuffer(30 * 1000)) {
					}
				}
				if ((info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0) {
					break;
				}
				videoPbUpdateBarLitener.updateBar(frameBuffer.getPresentationTime());
			}
			decoder.stop();
			decoder.release();
			WriteLogToDevice.writeLog("[Normal] -- PreviewH264: ", "stop video thread");
		}

		public boolean dequeueAndRenderOutputBuffer(int outtime) {
			int outIndex = decoder.dequeueOutputBuffer(info, outtime);

			switch (outIndex) {
			case MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED:
				return false;
			case MediaCodec.INFO_OUTPUT_FORMAT_CHANGED:
				return false;
			case MediaCodec.INFO_TRY_AGAIN_LATER:
				return false;
			case MediaCodec.BUFFER_FLAG_SYNC_FRAME:
				return false;

			default:
				decoder.releaseOutputBuffer(outIndex, true);
				if (!audioPlayFlag) {
					audioPlayFlag = true;
					WriteLogToDevice.writeLog("[Error] -- PreviewH264: ", "ok show image!.....................");
				}
				return true;
			}
		}

		public void requestExitAndWait() {
			// 把这个线程标记为完成，并合并到主程序线程
			H264DecodeThreadDone = true;
			try {
				join();
			} catch (InterruptedException ex) {
			}
		}
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		hasSurface = true;
		if (mySurfaceViewThread != null) {
			if (mySurfaceViewThread.isAlive() == false) {
				setFormat();
				mySurfaceViewThread.start();
			}
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		WriteLogToDevice.writeLog("[Normal] -- LocalVideoPbH264: ", " surfaceDestroyed");
		hasSurface = false;
		stop();
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
	}

	public void setSurfaceViewArea(int mWidth, int mHeight) {
		// JIRA ICOM-1723 begin add by b.jiang 2015-09-07
		WriteLogToDevice.writeLog("[Normal] -- LocalVideoPbH264: ", "setSurfaceViewArea frmW=" + frmW);
		WriteLogToDevice.writeLog("[Normal] -- LocalVideoPbH264: ", "setSurfaceViewArea frmH=" + frmH);
		if (frmH <= 0 || frmW <= 0) {
			return;
		}
		Rect drawFrameRect = ScaleTool.getScaledPosition(frmW, frmH, mWidth, mHeight);
		// JIRA ICOM-1723 end add by b.jiang 2015-09-07
		RelativeLayout.LayoutParams surfaceViewLayoutParams = (RelativeLayout.LayoutParams) this.getLayoutParams();
		surfaceViewLayoutParams.setMargins(drawFrameRect.left, drawFrameRect.top, drawFrameRect.left, drawFrameRect.top);
		this.setLayoutParams(surfaceViewLayoutParams);
	}

	public void destorySurface() {
		hasSurface = false;
	}

	private void setFormat() {
		/* create & config android.media.MediaFormat */
		ICatchVideoFormat videoFormat = null;
		try {
			WriteLogToDevice.writeLog("[normal] -- LocalVideoPbH264: ", "start videoFormat");
			videoFormat = videoPb.getVideoFormat();
			WriteLogToDevice.writeLog("[normal] -- LocalVideoPbH264: ", "end videoFormat");
		} catch (IchSocketException e1) {
			// TODO Auto-generated catch block
			WriteLogToDevice.writeLog("[Error] -- LocalVideoPbH264: ", "h264 IchSocketException");
			e1.printStackTrace();
		} catch (IchCameraModeException e1) {
			// TODO Auto-generated catch block
			WriteLogToDevice.writeLog("[Error] -- LocalVideoPbH264: ", "h264 IchCameraModeException");
			e1.printStackTrace();
		} catch (IchInvalidSessionException e1) {
			// TODO Auto-generated catch block
			WriteLogToDevice.writeLog("[Error] -- LocalVideoPbH264: ", "h264 IchInvalidSessionException");
			e1.printStackTrace();
		} catch (IchStreamNotRunningException e1) {
			// TODO Auto-generated catch block
			WriteLogToDevice.writeLog("[Error] -- LocalVideoPbH264: ", "h264 IchStreamNotRunningException");
			e1.printStackTrace();
		}
		WriteLogToDevice.writeLog("[normal] -- PreviewH264: ", "create  MediaFormat");
		frmW = videoFormat.getVideoW();
		frmH = videoFormat.getVideoH();
		String type = videoFormat.getMineType();
		WriteLogToDevice.writeLog("[normal] -- PreviewH264: ", "create  MediaFormat w=" + frmW + " h=" + frmH + " type =" + type);
		MediaFormat format = MediaFormat.createVideoFormat(type, frmW, frmH);
		WriteLogToDevice.writeLog("[normal] -- PreviewH264: ", "create  MediaFormat ----a format=" + format);

		/* create & config android.media.MediaCodec */
		String ret = videoFormat.getMineType();

		decoder = null;
		try {
			decoder = MediaCodec.createDecoderByType(ret);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		decoder.configure(format, holder.getSurface(), null, 0);
		decoder.start();
	}

	public interface LocalH264VideoPbUpdateBarLitener {
		void updateBar(double pts);
	}

	public void addVideoPbUpdateBarLitener(LocalH264VideoPbUpdateBarLitener videoPbUpdateBarLitener) {
		// h264ImageLitener.isShown();
		this.videoPbUpdateBarLitener = videoPbUpdateBarLitener;
	}
}