package com.vxfly.helioscamera.baseItems;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.util.Log;

import com.vxfly.helioscamera.SDKAPI.CameraProperties;
import com.vxfly.helioscamera.global.App.PropertyId;
import com.vxfly.helioscamera.hash.PropertyHashMapDynamic;
import com.vxfly.helioscamera.log.WriteLogToDevice;

public class PropertyTypeString {

	private int propertyId;
	private List<String> valueListString;
	private List<String> valueListStringUI;
	private HashMap<String, ItemInfo> hashMap;
	private String[] valueArrayString;

	public PropertyTypeString(int propertyId, Context context) {
		this.propertyId = propertyId;
		initItem();
	}
	//JIRA ICOM-2246 Begin Add by b.jiang 2015-12-04
	private Boolean checkWithTimeLapseMask(int TimeLapseMaskValue, int index) {
		int intShiftValue = 0x1;
		int intMatchValue = 0 ;  
		WriteLogToDevice.writeLog("[Normal] -- PropertyTypeString: ", "start ceckWithTimeLapseMask");
		WriteLogToDevice.writeLog("[Normal] -- PropertyTypeString: ", "TimeLapseMaskValue = " + TimeLapseMaskValue);
		WriteLogToDevice.writeLog("[Normal] -- PropertyTypeString: ", "index = " + index);
		if ( index > 0) {
			intShiftValue = intShiftValue << index;			
		}  
		WriteLogToDevice.writeLog("[Normal] -- PropertyTypeString: ", "intShiftValue = " + intShiftValue);
		intMatchValue = TimeLapseMaskValue & intShiftValue; 
		WriteLogToDevice.writeLog("[Normal] -- PropertyTypeString: ", "intMatchValue = " + intMatchValue);
		if (intMatchValue > 0) {			
			WriteLogToDevice.writeLog("[Normal] -- PropertyTypeString: ", "end ceckWithTimeLapseMask = true ");
			return true;
		} else {
			WriteLogToDevice.writeLog("[Normal] -- PropertyTypeString: ", "end ceckWithTimeLapseMask = faluse ");
			return false;
		}		
	}
	//JIRA ICOM-2246 End Add by b.jiang 2015-12-04
	public void initItem() {
		// TODO Auto-generated method stub
		if (hashMap == null) {
			//JIRA ICOM-2246 Begin Add by b.jiang 2015-12-04
			if (propertyId == PropertyId.TIMELAPSE_VIDEO_SIZE_LIST_MASK) {
				WriteLogToDevice.writeLog("[Normal] -- PropertyTypeString: ", "start initItem in PropertyId.TIMELAPSE_VIDEO_SIZE_LIST_MASK");
				hashMap = PropertyHashMapDynamic.getInstance().getDynamicHashString(PropertyId.VIDEO_SIZE);
			} else {
				WriteLogToDevice.writeLog("[Normal] -- PropertyTypeString: ", "start initItem in " + propertyId);				
				hashMap = PropertyHashMapDynamic.getInstance().getDynamicHashString(propertyId);	
			} 
			
			//JIRA ICOM-2246 End Add by b.jiang 2015-12-04
			
			
			//hashMap = PropertyHashMapDynamic.getInstance().getDynamicHashString(propertyId);
		}
		if (propertyId == PropertyId.IMAGE_SIZE) {
			valueListString = CameraProperties.getInstance().getSupportedImageSizes();
		}
		if (propertyId == PropertyId.VIDEO_SIZE) {
			valueListString = CameraProperties.getInstance().getSupportedVideoSizes();
		}
		//JIRA ICOM-2246 Begin Add by b.jiang 2015-12-04
		if (propertyId == PropertyId.TIMELAPSE_VIDEO_SIZE_LIST_MASK) {
			valueListString = CameraProperties.getInstance().getSupportedVideoSizes();
			WriteLogToDevice.writeLog("[Normal] -- PropertyTypeString - initItem : ", " before valueListString.size = " + valueListString.size());
			Log.d("TigerTiger" ,"before valueListString.size = " + valueListString.size());
			// Check support property : TIMELAPSE_VIDEO_SIZE_LIST_MASK
			int intTimeLapseVideoSizeListMask = 0;
			if (CameraProperties.getInstance().hasFuction(PropertyId.TIMELAPSE_VIDEO_SIZE_LIST_MASK) == true) {
				intTimeLapseVideoSizeListMask = CameraProperties.getInstance().getCameraTimeLapseVideoSizeListMask();
				WriteLogToDevice.writeLog("[Normal] -- PropertyTypeString - initItem : ", " getCameraTimeLapseVideoSizeListMask = " + intTimeLapseVideoSizeListMask);
				for (int ii = 0; ii < valueListString.size(); ii++) {
					if (checkWithTimeLapseMask(intTimeLapseVideoSizeListMask, ii) == false) {
						valueListString.remove(ii);
						ii--;
						intTimeLapseVideoSizeListMask = intTimeLapseVideoSizeListMask >> 1 ;
					} 
				}		
			}			
		}
		
		//JIRA ICOM-2246 End Add by b.jiang 2015-12-04	
		WriteLogToDevice.writeLog("[Normal] -- PropertyTypeString - initItem : ", " after valueListString.size = " + valueListString.size());
		Log.d("TigerTiger" ,"after valueListString.size = " + valueListString.size());
		
		for (int ii = 0; ii < valueListString.size(); ii++) {
			if (hashMap.containsKey(valueListString.get(ii)) == false) {
				valueListString.remove(ii);
				ii--;
			}
		}
		valueListStringUI = new LinkedList<String>();
		valueArrayString = new String[valueListString.size()];
		if (valueListString != null) {
			for (int ii = 0; ii < valueListString.size(); ii++) {
				valueListStringUI.add(ii, hashMap.get(valueListString.get(ii)).uiStringInSettingString);
				valueArrayString[ii] = hashMap.get(valueListString.get(ii)).uiStringInSettingString;
			}
		}

	}

	public String getCurrentValue() {
		// TODO Auto-generated method stub
		//JIRA ICOM-2246 Begin Add by b.jiang 2015-12-04
		if (propertyId == PropertyId.TIMELAPSE_VIDEO_SIZE_LIST_MASK) {
			return CameraProperties.getInstance().getCurrentStringPropertyValue( PropertyId.VIDEO_SIZE);
		} else { 
			return CameraProperties.getInstance().getCurrentStringPropertyValue( propertyId);
		}
		//return CameraProperties.getInstance().getCurrentStringPropertyValue(propertyId);
		//JIRA ICOM-2246 Begin Add by b.jiang 2015-12-04
		
	}

	public String getCurrentUiStringInSetting() {
		ItemInfo itemInfo = hashMap.get(getCurrentValue());
		String ret = null;
		if (itemInfo == null) {
			ret = "Unknown";
		} else {
			ret = itemInfo.uiStringInSettingString;
		}
		return ret;
	}

	public String getCurrentUiStringInPreview() {
		// TODO Auto-generated method stub
		ItemInfo itemInfo = hashMap.get(getCurrentValue());
		String ret = null;
		if (itemInfo == null) {
			ret = "Unknown";
		} else {
			ret = itemInfo.uiStringInPreview;
		}
		return ret;
	}

	public String getCurrentUiStringInSetting(int position) {
		// TODO Auto-generated method stub
		return valueListString.get(position);
	}

	public List<String> getValueList() {
		// TODO Auto-generated method stub
		return valueListString;
	}

	public List<String> getValueListUI() {
		// TODO Auto-generated method stub
		return valueListString;
	}

	public Boolean setValue(String value) {
		// TODO Auto-generated method stub
		//JIRA ICOM-2246 Begin Add by b.jiang 2015-12-04
		if (propertyId == PropertyId.TIMELAPSE_VIDEO_SIZE_LIST_MASK) {
			return CameraProperties.getInstance().setStringPropertyValue( PropertyId.VIDEO_SIZE, value);
		} else { 
			return CameraProperties.getInstance().setStringPropertyValue( propertyId, value);	
		}
		//return CameraProperties.getInstance().setStringPropertyValue( propertyId, value);
		//JIRA ICOM-2246 End Add by b.jiang 2015-12-04
		
	}

	public boolean setValueByPosition(int position) {
		//JIRA ICOM-2246 Begin Add by b.jiang 2015-12-04
		if (propertyId == PropertyId.TIMELAPSE_VIDEO_SIZE_LIST_MASK) {
			return CameraProperties.getInstance().setStringPropertyValue( PropertyId.VIDEO_SIZE,
					valueListString.get(position));
		} else {
			return CameraProperties.getInstance().setStringPropertyValue( propertyId,
					valueListString.get(position));	
		}
		//return CameraProperties.getInstance().setStringPropertyValue( propertyId,valueListString.get(position));
		//JIRA ICOM-2246 Begin Add by b.jiang 2015-12-04

		
	}

	public String[] getValueArrayString() {
		return valueArrayString;
	}
}
