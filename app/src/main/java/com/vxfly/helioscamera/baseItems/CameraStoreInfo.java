package com.vxfly.helioscamera.baseItems;

public class CameraStoreInfo {
	public String cameraName;
	public String cameraUid;
	public String cameraPassword;
	public String camreaIp;

	public CameraStoreInfo(String cameraName,String cameraUid,String cameraPassword) {
		this.cameraName = cameraName;
		this.cameraUid = cameraUid;
		this.cameraPassword = cameraPassword;
	}
}
