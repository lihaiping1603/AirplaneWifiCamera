package com.vxfly.helioscamera.baseItems;

public class CameraSelectedInfo {
	public String cameraName;
	public String cameraIp;
	public int cameraMode;
	public String uid;

	public CameraSelectedInfo(String cameraName,String cameraIp, int cameraMode,String uid) {
		this.cameraName = cameraName;
		this.cameraIp = cameraIp;
		this.cameraMode = cameraMode;
		this.uid = uid;
	}
}
